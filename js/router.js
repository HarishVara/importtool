/**
 * Defines routing configuration for cnaApp route from one state to another state
 * It allows only for valid states which is need to be define here.
 * @ngInject
 *
 */
angular
    .module('cnaApp')
    .config(function($stateProvider, $httpProvider, $urlRouterProvider, IdleProvider, KeepaliveProvider) {
        $httpProvider.defaults.useXDomain = true;
        delete $httpProvider.defaults.headers.common['X-Requested-With'];
        KeepaliveProvider.interval(10);
        IdleProvider.windowInterrupt('focus');
        // For any unmatched url, redirect to /login
        $urlRouterProvider.otherwise("/login");
        // Now set up the states

          /* ########## Import Router ########## */
        $stateProvider
            .state('login', {
                url: "/login",
                views: {

                    'content': {
                        templateUrl: "views/import/login.html",
                        controller: "loginController"
                    }
                },
                cache: false
            })
            .state('logout', {
                url: "/logout",
                views: {

                    'content': {
                        templateUrl: "views/import/login.html",
                        controller: "logoutController"
                    }
                },
                cache: false
            })
            .state('importDashboard', {
                url: "/importDashboard",
                views: {
                    'nav': {
                        templateUrl: "views/import/nav.html",
                        controller: "mainNavCtrl",
                        resolve: {
                            UserDetails: GetUserInfo
                        }
                    },
                    'content': {
                        templateUrl: "views/import/dashboard.html",
                        controller: "importDashboardController"
                    }
                },
                cache: false
            })
            .state('userDashboard', {
                url: "/userDashboard",
                views: {
                    'nav': {
                        templateUrl: "views/import/adminNav.html",
                        controller: "mainNavCtrl",
                        resolve: {
                            UserDetails: GetUserInfo
                        }
                    },
                    'content': {
                        templateUrl: "views/import/userDashboard.html",
                        controller: "userDashboardController"
                    }
                },
                cache: false
            })
            .state('adminDashboard', {
                url: "/adminDashboard",
                views: {
                    'nav': {
                        templateUrl: "views/import/userNav.html",
                        controller: "mainNavCtrl",
                        resolve: {
                            UserDetails: GetUserInfo
                        }
                    },
                    'content': {
                        templateUrl: "views/import/adminDashboard.html",
                        controller: "adminDashboardController"
                    }
                },
                cache: false
            })
            .state('reportDetail', {
                url: "/reportDetail/:reportId",

                views: {
                    'nav': {
                        templateUrl: "views/import/adminNav.html",
                        controller: "mainNavCtrl",
                        resolve: {
                            UserDetails: GetUserInfo
                        }
                    },
                    'content': {
                        templateUrl: "views/import/reportDetail.html",
                        controller: "reportDetailController",

                    }
                },
                cache: false
            })
            .state('cbaRule', {
                url: "/cbaRule",
                views: {
                    'nav': {
                        templateUrl: "views/import/cbaNav.html",
                        controller: "mainNavCtrl",
                        resolve: {
                            UserDetails: GetUserInfo
                        }

                    },
                    'content': {
                        templateUrl: "views/import/cbaDashboard.html",
                        //controller: "cbaDashboardController"
                    }
                },
                cache: false
            })
    });


function GetUserInfo(AdminUser,Authentication) {

    return AdminUser.userDetail("AdminUsers/" + localStorage.id, function(response) {

        if (response.status == 200) {

            return response.data;


        }
    });


}


