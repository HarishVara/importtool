/* Start @Directive For Updateing CBA RUle*/
angular.module('cnaApp').directive('displayDiv', function(CBARulesService, $loading, toaster, $rootScope, DateConversionFilter,DateConversionService) {
    // Runs during compile
    return {

        scope: {
            eachdivdata: '=eachdivdata',

            // forEditingSurveyData: '=forEditingSurveyData'
        },

        restrict: 'E',
        templateUrl: 'views/import/cbaRule.html',
        replace: true,

        link: function($scope, iElm, iAttrs) {


            // $scope.eachdivdata={};
            // $scope.eachDivInput=$scope.eachdivdata.Id;
            // watching for fee fields mandatory
            $scope.eachdivdata.validFrom = DateConversionFilter.Convert($scope.eachdivdata.validFrom);
            $scope.eachdivdata.validTo = DateConversionFilter.Convert($scope.eachdivdata.validTo);
            $scope.ufeeReq = false;
            $scope.umandatoryForFee = false;
            $scope.$watch('eachdivdata.flatRecurringFees', function(newValue, oldValue) {
                if (newValue) {

                    $scope.umandatoryForFee = false;

                } else {
                    $scope.umandatoryForFee = true;

                }

            });

            // watching for fee fields mandatory ends here



            $scope.SaveRule = function() {
                $scope.$broadcast('show-errors-check-validity');

                if ($scope.updateRuleForm.$valid) {
                    if(DateConversionService.Convert($scope.eachdivdata.validFrom) > DateConversionService.Convert($scope.eachdivdata.validTo)){
                        toaster.pop("error", "Valid From Date should be less than Valid To Date");
                        return false;
                    };
                    
                    $scope.UpdateRuleObj = {};
                    $scope.UpdateRuleObj.payPeriod = $scope.eachdivdata.payPeriod;
                    $scope.UpdateRuleObj.individualType = $scope.eachdivdata.individualType;
                    $scope.UpdateRuleObj.individualSubType = $scope.eachdivdata.individualSubType;
                    $scope.UpdateRuleObj.memberType = $scope.eachdivdata.memberType;
                    $scope.UpdateRuleObj.memberSubType = $scope.eachdivdata.memberSubType;
                    $scope.UpdateRuleObj.reducedRate = $scope.eachdivdata.reducedRate;
                    $scope.UpdateRuleObj.capRate = $scope.eachdivdata.capRate;
                    $scope.UpdateRuleObj.bhrFactor = $scope.eachdivdata.bhrFactor;
                    $scope.UpdateRuleObj.minHours = $scope.eachdivdata.minHours;
                    $scope.UpdateRuleObj.validFrom = DateConversionService.Convert($scope.eachdivdata.validFrom);
                    $scope.UpdateRuleObj.validTo = DateConversionService.Convert($scope.eachdivdata.validTo);
                    $scope.UpdateRuleObj.flatRecurringFees = $scope.eachdivdata.flatRecurringFees;
                    $scope.UpdateRuleObj.discount = $scope.eachdivdata.discount;
                    $scope.UpdateRuleObj.priority = $scope.eachdivdata.priority;
                    $scope.UpdateRuleObj.name = $scope.eachdivdata.name;
                    $scope.UpdateRuleObj.description = $scope.eachdivdata.description;

                    $loading.start('Ruleloader');
                    CBARulesService.UpdateRule("CBARules/" + $scope.eachdivdata.id, $scope.UpdateRuleObj, function(response) {
                        console.log(response);
                        if (response.status == 200) {
                            //console.log(response);
                            $loading.finish('Ruleloader');
                            toaster.pop("success", "CBA Rule has been updated Successfully");
                        } else {
                            $loading.finish('Ruleloader');
                            toaster.pop("error", response.data.statusText);
                        }

                    });
                }



            };


            $scope.DeleteRule = function() {
                $loading.start('Ruleloader');
                CBARulesService.DeleteRule("CBARules/" + $scope.eachdivdata.id, function(response) {
                    console.log(response);
                    if (response.status == 200) {
                        //console.log(response);
                        $loading.finish('Ruleloader');
                        toaster.pop("success", "CBA Rule has been deleted Successfully");
                        $loading.start('Ruleloader');
                        CBARulesService.RulesByFacility("CBARules?filter[where][facilityId]=" + $rootScope.SelectedFacilityId, function(response) {
                            if (response.status == 200) {
                                console.log(response.data);
                                $rootScope.itemsList.items = response.data;
                                $loading.finish('Ruleloader');
                            } else {
                                $loading.finish('Ruleloader');
                            }

                        });

                        $scope.CBATabAPI;

                    } else {
                        $loading.finish('Ruleloader');
                        toaster.pop("error", response.data.statusText);
                    }

                });


            };


            $scope.ValidFromDate = function() {
                $scope.eachdivdata.validFrom = new Date($scope.eachdivdata.validFrom);
            };
            $scope.ValidFromDate();

            $scope.ValidFromCalenderFromDate = function() {
                $scope.validFromCalenderpopup.opened = true;

            };
            $scope.validFromCalenderpopup = {
                opened: false
            };


            $scope.ValidToDate = function() {
                $scope.eachdivdata.validTo = new Date($scope.eachdivdata.validTo);
            };
            $scope.ValidToDate();

            $scope.ValidToCalenderToDate = function() {
                $scope.validToCalenderpopup.opened = true;

            };
            $scope.validToCalenderpopup = {
                opened: false
            };
            $scope.formats = ['MM-dd-yyyy', 'dd.MM.yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
            $scope.format = $scope.formats[0];
            $scope.altInputFormats = ['M!/d!/yyyy'];

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            $scope.events = [{
                date: tomorrow,
                status: 'full'
            }, {
                date: afterTomorrow,
                status: 'partially'
            }];

            $scope.getDayClass = function(date, mode) {
                if (mode === 'day') {
                    var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

                    for (var i = 0; i < $scope.events.length; i++) {
                        var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                        if (dayToCheck === currentDay) {
                            return $scope.events[i].status;
                        }
                    }
                }

                return '';
            };
            /*date picker ends here*/

        }
    }
});



angular.module('cnaApp').directive('showMore', function($compile) {
        return {
            transclude: true,
            restrict: 'AE',
            scope: {
                text: '@',
                todate: '@',
                limit: '@',
            },

            replace: true,
            template: '<div ><span ng-class="(date > todate)?\'expired\':\'\'" ><p ng-show="largeText"> {{ text | subString :0 :end }}.... <a href="javascript:;"  ng-click="showMore()" ng-show="isShowMore">More</a><a href="javascript:;" ng-click="showLess()" ng-hide="isShowMore">Less </a></p><p ng-hide="largeText">{{ text }}</p> </span></div> ',

            link: function(scope, ele, attr, $rootScope, $broadcast) {

                function Dateconvert(str) {

                    var date = new Date(str),
                        mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                        day = ("0" + date.getDate()).slice(-2);
                    return [date.getFullYear(),mnth,day].join("-");
                }
               
                scope.date = Dateconvert(new Date());
                console.log(scope.date);
                scope.end = scope.limit;
                scope.isShowMore = true;
                scope.largeText = true;

                if (scope.text.length <= scope.limit) {
                    scope.largeText = false;
                };
                var moreTextLenth;
                scope.showMore = function() {

                    scope.end = scope.text.length;
                    scope.isShowMore = false;
                };

                scope.showLess = function() {

                    scope.end = scope.limit;
                    scope.isShowMore = true;
                }

            }
        }
    })
    /**
     * Retrun the string from specified range.
     * e.g<p><% course.description | subString : 0 :75 %> </p>
     */
angular.module('cnaApp').filter('subString', function() {
    return function(str, start, end) {
        if (str != undefined) {
            return str.substr(start, end);
        }
    }
});

/*End @Directive for CBA rule .Displaying show more text and less Tex*/

/*Directive for Form Validation */
angular.module('cnaApp').directive('showErrors', function($timeout, showErrorsConfig) {
    var getShowSuccess, linkFn;
    getShowSuccess = function(options) {
        var showSuccess;
        showSuccess = showErrorsConfig.showSuccess;
        if (options && options.showSuccess != null) {
            showSuccess = options.showSuccess;
        }
        return showSuccess;
    };
    linkFn = function(scope, el, attrs, formCtrl) {
        var blurred, inputEl, inputName, inputNgEl, options, showSuccess, toggleClasses;
        blurred = false;
        options = scope.$eval(attrs.showErrors);
        showSuccess = getShowSuccess(options);
        inputEl = el[0].querySelector('[name]');
        inputNgEl = angular.element(inputEl);
        inputName = inputNgEl.attr('name');
        if (!inputName) {
            throw 'show-errors element has no child input elements with a \'name\' attribute';
        }
        inputNgEl.bind('blur', function() {
            blurred = true;
            return toggleClasses(formCtrl[inputName].$invalid);
        });
        scope.$watch(function() {
            return formCtrl[inputName] && formCtrl[inputName].$invalid;
        }, function(invalid) {
            if (!blurred) {
                return;
            }
            return toggleClasses(invalid);
        });
        scope.$on('show-errors-check-validity', function() {
            return toggleClasses(formCtrl[inputName].$invalid);
        });
        scope.$on('show-errors-reset', function() {
            return $timeout(function() {
                el.removeClass('has-error');
                el.removeClass('has-success');
                return blurred = false;
            }, 0, false);
        });
        return toggleClasses = function(invalid) {
            el.toggleClass('has-error', invalid);
            if (showSuccess) {
                return el.toggleClass('has-success', !invalid);
            }
        };
    };
    return {
        restrict: 'A',
        require: '^form',
        compile: function(elem, attrs) {
            if (!elem.hasClass('form-group')) {
                throw 'show-errors element does not have the \'form-group\' class';
            }
            return linkFn;
        }
    };
});

angular.module('cnaApp').provider('showErrorsConfig', function() {
    var _showSuccess;
    _showSuccess = false;
    this.showSuccess = function(showSuccess) {
        return _showSuccess = showSuccess;
    };
    this.$get = function() {
        return {
            showSuccess: _showSuccess
        };
    };
});

angular.module('cnaApp').directive('fileModel', ['$parse', function($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;

            element.bind('change', function() {
                scope.$apply(function() {
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}])


/* start @Directive For loader */
//angular.module('cnaApp').directive('loading', ['$http' ,function ($http) {
//        return {
//            restrict: 'A',
//            link: function (scope, elm, attrs)
//            {
//                scope.isLoading = function () {
//                    return $http.pendingRequests.length > 0;
//                };
//
//                scope.$watch(scope.isLoading, function (v)
//                {
//                    if(v){
//                        elm.show();
//                    }else{
//                        elm.hide();
//                    }
//                });
//            }
//        };
//
//    }]);

/* end @Directive For loader */
