'use strict';

/**
 * @ngdoc service
 * @name cnaApp.rest
 * @description
 * # rest
 * Service to talk with backend api.
 */
var base = "http://dev.yogamihi.com/api/";

angular.module('cnaApp').service("userSession", function() {
    var isLoggedIn = false;
    this.checkUser = function() {
        if (localStorage.accessToken && localStorage.id) {
            return true;
        } else {
            return false;
        }
    }
    this.setLoginStat = function(state) {
        isLoggedIn = state;
    }
})

angular.module('cnaApp').service("Authentication", function($rootScope) {
    $rootScope.AuthRole=false;
    
    this.setAuthRole = function(role) {
        $rootScope.AuthRole = role;
    }
})


/*
This service belongs for only adminUser related.
*/
angular.module('cnaApp').service('AdminUser', function($http, $state) {

    this.login = function(url, data, callback) {
        $http({
            method: 'POST',
            url: base + url,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.logout = function(url, callback) {

        $http({
            method: 'POST',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.userDetail = function (url, callback) {
         return $http({
            method: 'GET',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function (data) {
            return data;
        }, function (err) {
            return err;
        });
    }

    /*this.userDetail = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        });
    }*/
    this.userList = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "AdminUsers?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.userListCount = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.AssignUserList = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.CreateUser = function(url, data, callback) {
        $http({
            method: 'POST',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.UpdateUser = function(url, data, callback) {
        $http({
            method: 'PUT',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.DeleteUser = function(url, callback) {
        $http({
            method: 'DELETE',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }




});


/*service belogs to Facilities*/
angular.module('cnaApp').service('FacilityService', function($http, $state) {

    this.FacilityList = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "Facilities?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.FacilityListCount = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.FacilityListWithCba = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "Facilities?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.FacilityDropdown = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.GetFacility = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "Facilities?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.AssignFacility = function(url, data, callback) {
        $http({
            method: 'PUT',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }


    this.GetMoreInfo=function(url, callback){
     $http({
            method: 'GET',
            url: base + "Reports?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
           callback(err);
          
        })

    }
    


});

angular.module('cnaApp').service('DataService', function($q, $http) {
      
      var serviceData= {
        data:{src:''},
        getData: function(url, callback) {
          var deffered = $q.defer();
         
            $http.get(base + "Reports?access_token=" + localStorage.accessToken + url).then(function(res) {
            
              deffered.resolve(res.data)
            })
          
          return deffered.promise.then(callback)
        }
      }
      
      return serviceData

    })

angular.module('cnaApp').service('CBARulesService', function($http, $state) {

    this.CBARuleList = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "CBARules?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.CBARuleObj = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.CreateRule = function(url, data, callback) {
        $http({
            method: 'POST',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.DeleteRule = function(url, callback) {
        $http({
            method: 'DELETE',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.UpdateRule = function(url, data, callback) {

        $http({
            method: 'PUT',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.RulesByFacility = function(url, callback) {

        $http({
            method: 'GET',
            url: base + url + "&access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }




});



/*service belogs to Reports*/
angular.module('cnaApp').service('ReportsService', function($http, $state) {

    this.ReportsList = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url + "&access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.totalReports = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.ListOfReports = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.ReportsTotalCount = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url + "&access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        });
    }


    this.UploadRecord = function(url, data, callback) {
        console.log(data);
        $http({
            method: 'POST',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }


    this.UploadFile = function(url, data, callback) {
        console.log(data);
        $http({
            method: 'POST',
            //url: base+url+"?access_token="+localStorage.access_token,
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': undefined
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.FilterByParam = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "Reports?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }


    this.DeleteAReport = function(url, callback) {
        $http({
            method: 'DELETE',
            url: base + url + "?access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.FacilityPayPeriod = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "Facilities/"+ url +"&access_token="+localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.downloadUploadedReport=function(id,filename){
        //http://dev.yogamihi.com/api/ReportUploads/56f49b672220aa8d72199135/download/Antelope-Abhi2.csv?access_token=yOjAc0zBZBZYmhEm3Jt0oRHWnzCjWlSlr6rgOZBBzzEORcKL8KfsuTDidRHefGMC/
        return base+"ReportUploads/"+id+"/download/"+filename+"?access_token="+localStorage.accessToken;

    }




});

angular.module('cnaApp').service('DateConversionService', function() {
    this.Convert = function(str) {
        console.log(str);
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }




});

angular.module('cnaApp').service('DateConversionFilter', function() {
    this.Convert = function(str) {
        if(str!=""|| str!=undefined){
            var numbers = str.match(/\d+/g);
          return [numbers[1], numbers[2],numbers[0]].join("-");

        }else{
            return "";
        }
    }
          
    
        




});

angular.module('cnaApp').service('ReportService', function($http, $state) {
    this.ReportInfos = function(url, callback) {

        $http({
            method: 'GET',
            url: base + url + "?access_token=" + localStorage.accessToken + "&filter[include]=facility",
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })


    }

    this.ReportStatusUpdate = function(url, data, callback) {
        $http({
            method: 'PUT',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })




    }
    this.RawReportCount = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "ReportRows/count?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })




    }
    this.GetRawReportRows = function(url, callback) {

        $http({
            method: 'GET',
            url: base + "ReportRows" + "?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })

    }
    
    this.GetReportRowsById = function(url, data, callback) {

        $http({
            method: 'PUT',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })

    }

    this.GetProcessedRows = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "ReportRows?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.GetImportedRows = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "ReportRows?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.GetProcessedCount = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "ReportRows/count?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.GetImportedCount = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "ReportRows/count?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.GetTemplateByReportType = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "Templates?access_token=" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.CreateTemplateByReportType = function(url, data, callback) {
        $http({
            method: 'POST',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.UpdateTemplateByReportType = function(url, data, callback) {
        $http({
            method: 'PUT',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.NeedUpdateTabCellEdit = function(url, data, callback) {

        $http({
            method: 'PUT',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
     this.NewTabCellEdit = function(url, data, callback) {

        $http({
            method: 'PUT',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }


    this.ExceptionTabCellEdit = function(url, data, callback) {

        $http({
            method: 'PUT',
            url: base + url + "?access_token=" + localStorage.accessToken,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.PostProcessedImportAction = function(url, data, callback) {
        $http({
            method: 'POST',
            url: base + "ReportRows/updateAll?access_token=" + localStorage.accessToken + url,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.PostProcessedClearAction = function(url, data, callback) {
        $http({
            method: 'POST',
            url: base + "ReportRows/updateAll?access_token=" + localStorage.accessToken + url,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.PostProcessedUpdateAction = function(url, data, callback) {
        $http({
            method: 'POST',
            url: base + "ReportRows/updateAll?access_token=" + localStorage.accessToken + url,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.PostImportedClearAction = function(url, data, callback) {
        $http({
            method: 'POST',
            url: base + "ReportRows/updateAll?access_token=" + localStorage.accessToken + url,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.EnrichNewRecord = function(url, data, callback) {
        $http({
            method: 'POST',
            url: base + "ReportRows/updateAll?access_token=" + localStorage.accessToken + url,
            data: data,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.ProcessedRowCount = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.ProcessedReportRowAllCount = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "ReportRows/count?" + localStorage.accessToken + url,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }
    this.DuplicateRecords = function(url, callback) {
        $http({
            method: 'GET',
            url: base + "ReportRows?" + url +"access_token="+localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }

    this.MatchedIndividual = function(url, callback) {
        $http({
            method: 'GET',
            url: base + url +"?access_token="+localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })
    }


});

angular.module('cnaApp').service('ReportDescriptionService', function($http, $state) {
    this.GetTargetColumn = function(url, callback) {

        $http({
            method: 'GET',
            url: base + url + "&access_token=" + localStorage.accessToken,
            headers: {
                'Content-Type': 'application/json'
            }
        }).then(function(data) {
            callback(data);
        }, function(err) {
            callback(err);
        })


    }



});

/*angular.module('cnaApp').filter('dateFilter', function () {
  return function (input, entity) {
    //console.log(entity);
    var date=Date(entity);
    return date;
    

  };
});*/





