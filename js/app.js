'use strict'; // this function is executed in strict mode
/**
 * Initialization of main App module.
 * Setting of cnaApp to access through out this application.
 * @ngInject
 *
 */
angular
    .module('cnaApp', [
        /*Angular Modules*/
        'ngAnimate',
        'ui.bootstrap',

        'ngTouch',
        'ngResource',
        'ui.grid',
        'ui.grid.pagination',
        'ui.grid.resizeColumns',
        'ui.grid.moveColumns',
        'ui.grid.edit',
        'ui.grid.rowEdit',
        'ui.grid.cellNav',
        'ui.grid.selection',
        'ui.grid.expandable',
        'toaster',
        'hm.readmore',
        'as.sortable',

        'nvd3ChartDirectives',

        /*3rd-party Modules*/
        'ui.router',

        'angucomplete-alt',
        'darthwade.loading',
        'ngIdle',
        'ls.LiveSet',
        'angularjs-dropdown-multiselect',
        'ls.ChangeStream',
        'ui.grid.exporter'



    ]);

angular.module('cnaApp').run(function ($rootScope, userSession, $window, $state, Idle, $log, Keepalive) {
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams) {
        $rootScope.pageTitle = toState.name.charAt(0).toUpperCase() + toState.name.slice(1);
        if (!userSession.checkUser() && toState.name != "login") {
            event.preventDefault();
            $state.go("login");
        }
    });

 
    //idle timeout call
    Idle.watch();
    //$log.debug('app started.');

});


