/**
 * Defines Login controller for only authorised users
 * @param {!angular.$scope} $scope
 * @constructor
 * @export cnaApp
 * @ngInject
 */
angular.module('cnaApp').controller("loginController", ['toaster', '$loading', '$scope', '$state', 'userSession', 'AdminUser', function loginController(toaster, $loading, $scope, $state, userSession, AdminUser) {
    /* Login function for user authentication*/

    if (userSession.checkUser()) { //checking user login.
        $state.go("importDashboard", {}, {
            reload: true
        });
    }

    $scope.submit = function () {
        $scope.$broadcast('show-errors-check-validity');

        if ($scope.loginForm.$valid) {
            //login API Call
            $loading.start('login');
            AdminUser.login("AdminUsers/login", $scope.user, function (response) {
                    //console.log(response);
                    if (response.status == 200) {
                        toaster.pop({
                            type: 'success',
                            body: 'Welcome! Login Successful',
                            timeout: 1500
                        });
                        //toaster.pop('success', "Welcome!", "Login Successful",timeout: 1500);
                        localStorage.setItem("accessToken", response.data.id);
                        localStorage.setItem("id", response.data.userId);
                        userSession.setLoginStat(true);
                        $loading.finish('login');
                        $state.go('importDashboard', {}, {
                            reload: true
                        });
                        $loading.finish('login');

                    } else {
                        $loading.finish('login');
                        toaster.pop("error", response.data.error.message);
                    }
                }) //login API ends here.

        }
    };

    $scope.reset = function () {
        $scope.$broadcast('show-errors-reset');
        $scope.user = {};
    }

}]);


angular.module('cnaApp').controller("mainNavCtrl", ['toaster', '$scope', '$rootScope', '$state', 'userSession', 'AdminUser', 'Idle', 'UserDetails', 'Authentication', function mainNavCtrl(toaster, $scope, $rootScope, $state, userSession, AdminUser, Idle, UserDetails, Authentication) {
	if($state.$current.path == "reportDetail"){
		$scope.reportsPage = true;
	}else{
		$scope.reportsPage = false;
	}
    $rootScope.userInfo = UserDetails.data;
    if($rootScope.userInfo.role === "admin"){
    	$scope.adminDiv = true;
    	$scope.userDiv  = false;
    	console.log($rootScope.userInfo);
    }else{
    	$scope.adminDiv = false;
    	$scope.userDiv  = true;
    }
    Authentication.setAuthRole(UserDetails.data.role);
    //console.log($rootScope.AuthRole);

    //$scope.userInfo=UserDetails.data;
    /*$scope.userInfoAPI = function() {
        AdminUser.userDetail("AdminUsers/" + localStorage.id, function(response) {

            if (response.status == 200) {
                $rootScope.userInfo = response.data;
                
            }
        });

    };

    $scope.userInfoAPI();*/
    //console.log("mainNav");
    $scope.logOut = function () {
        AdminUser.logout("AdminUsers/logout", function (response) {
            ////console.log(response);
            if (response.status == 204) {
                localStorage.removeItem('id');
                localStorage.removeItem('accessToken');

                toaster.pop('success', "Successfully Logout");
                $state.go('login', {}, {
                    reload: true
                });
                userSession.setLoginStat(false);
            }
        });
    };

    $scope.events = [];
    $scope.idle = 1800;

    $scope.timeout = 1;

    $scope.$on('IdleStart', function () {
        addEvent({
            event: 'IdleStart',
            date: new Date()
        });
        //console.log("IdleStart");
    });

    $scope.$on('IdleEnd', function () {
        addEvent({
            event: 'IdleEnd',
            date: new Date()
        });
        //console.log("IdleEnd");
    });

    $scope.$on('IdleWarn', function (e, countdown) {
        addEvent({
            event: 'IdleWarn',
            date: new Date(),
            countdown: countdown
        });
    });

    $scope.$on('IdleTimeout', function () {
        addEvent({
            event: 'IdleTimeout',
            date: new Date()
        });
        //console.log("IdleTimeout");
        localStorage.removeItem('id');
        localStorage.removeItem('accessToken');
        $state.go('login', {}, {
            reload: true
        });
        userSession.setLoginStat(false);
        toaster.pop('success', "Successfully Logout");
    });

    $scope.$on('Keepalive', function () {
        addEvent({
            event: 'Keepalive',
            date: new Date()
        });
    });

    function addEvent(evt) {
        $scope.$evalAsync(function () {
            $scope.events.push(evt);
        })
    }

    $scope.reset = function () {
        Idle.watch();
    }

    $scope.$watch('idle', function (value) {
        if (value !== null) Idle.setIdle(value);
    });

    $scope.$watch('timeout', function (value) {
        if (value !== null) Idle.setTimeout(value);
    });

}]);


/**
 * Defines admin  controller.
 * It specifies which dashboard user will able to access .
 * @param {!angular.$scope} $scope
 * @constructor
 * @export cnaApp
 * @ngInject
 */
angular.module('cnaApp').controller("importDashboardController", ['toaster', '$scope', '$state', 'AdminUser', 'FacilityService','ReportsService', function importDashboardController(toaster, $scope, $state, AdminUser, FacilityService,ReportsService) {

    $scope.toolTipContentFunction = function () {
        return function (key, x, y, e, graph) {
            return 'Reports' + '<h5>' + key + '</h5>' + '<p>' + y + ' at ' + x + '</p>'
        }
    }

    /*UserRole check API*/

    AdminUser.userDetail("AdminUsers/" + localStorage.id, function (response) {

        if (response.status == 200) {
            $scope.userInfo = response.data;

        }
    });

    /*UserRole check API ends here*/

    /* Count */
    AdminUser.userListCount("AdminUsers/count", function (response) {
        if (response.status == 200) {
            ////console.log(response);
            $scope.Usertotalcount = response.data.count;
        }
        ////console.log($scope.Usertotalcount);
    });

    FacilityService.FacilityListCount("Facilities/count", function (response) {
        if (response.status == 200) {
            ////console.log(response);
            $scope.Facilitytotalcount = response.data.count;
        }
        //console.log($scope.Facilitytotalcount);
    });
    ReportsService.ReportsTotalCount("Reports/count?", function (response) {
        if (response.status == 200) {
            ////console.log(response);
            $scope.Reporttotalcount = response.data.count;
        }
        //console.log($scope.Facilitytotalcount);
    });
    var condition = {
    		"status":"processed"
    }
   ReportsService.ReportsTotalCount("Reports/count?where="+angular.toJson(condition), function (response) {
	   if (response.status == 200) {
		   $scope.processedReportscount = response.data.count; 
        }
        //console.log($scope.Facilitytotalcount);
    });

    /* Count */

    $scope.exampleData = [{
        key: "One",
        y: 5
    }, {
        key: "Two",
        y: 2
    }, {
        key: "Three",
        y: 9
    }, {
        key: "Four",
        y: 7
    }, {
        key: "Five",
        y: 4
    }, {
        key: "Six",
        y: 3
    }, {
        key: "Seven",
        y: 9
    }];

    $scope.xFunction = function () {
        return function (d) {
            return d.key;
        };
    }
    $scope.yFunction = function () {
        return function (d) {
            return d.y;
        };
    }

    $scope.descriptionFunction = function () {
        return function (d) {
            return d.key;
        }
    }

}]);
/**
 * Defines user dashboard  controller.
 * @param {!angular.$scope} $scope
 * @constructor
 * @export cnaApp
 * @ngInject
 */

angular.module('cnaApp').controller("userDashboardController", ['$uibModal', '$rootScope', '$state', 'uiGridConstants', 'toaster', '$scope', '$filter', '$loading', 'ReportsService', 'FacilityService', 'DateConversionService', 'createChangeStream', 'LiveSet','$http', function ($uibModal, $rootScope, $state, uiGridConstants, toaster, $scope, $filter, $loading, ReportsService, FacilityService, DateConversionService, createChangeStream, LiveSet, $http) {
    /*live set example*/
    /*var data = [{"status": "processed","id": "56e24b3b4f8acd1f484dcdef"}];
    var src = new EventSource("http://dev.yogamihi.com/api/Reports/change-stream?access_token=jtWjs6gahrdrXSAkRF7C6Uwa0XKSX6OeWAGtVnDukXoRkD6feJBAUn9pVLjXOWvA",{ withCredentials: true });

    var changes = createChangeStream(src);
    var set = new LiveSet(data, changes);
    $scope.values = set.toLiveArray();
    //console.log($scope.values);
     //console.log(changes);*/

    //var changes = createChangeStream();

    /*live set example end here*/

    /*Object Declaration for UserDashboard Controller */
    $scope.enableDelete = false; // delete button.
    $scope.enableDownload = false;
   

    $scope.uploadObj = {}; //@Object for new report upload.
    /*Uploading New file*/
    console.log($rootScope.userInfo);
    $scope.gridOptionsReport = {};
    $scope.sortColumnNumber = 0;
    $scope.sortCoulmnName = {};
    $scope.sortOrder = {};
    $scope.sortPageNumber = 1;
    $scope.sortPageSize = 0;
    $scope.searchData = {};
    $scope.widthChanged = {};
    $scope.columnWidths = [];

    var init = function(){
    	$http({
			method : 'GET',
			url : "http://0.0.0.0:3000/api/settings?filter[where][username]="+$rootScope.userInfo.username,
			headers : {
				'Content-Type' : 'application/json'
			}
		}).then(function(response) {
			$scope.columnWidths = response.data;
			if($scope.columnWidths.length<=0){
				$http({
					method : 'GET',
					url : "http://0.0.0.0:3000/api/settings?filter[where][username]=default",
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function(response) {
					$scope.columnWidths = response.data;
					console.log($scope.columnWidths[0]);
					$scope.widthAssign();
					$scope.widthAssign();
				}, function(err) {
					alert(angular.toJson(err));
				})
			}else{
				$scope.widthAssign();
			}
			console.log($scope.columnWidths);
		}, function(err) {
			console.log(err);
			alert(angular.toJson(err));
			
		})
    }
    init();
    $scope.widthAssign = function(){
    	 $scope.filterReportGrid = {
   			  useExternalSorting: true,
   		        enableSelectAll: false,
   		        multiSelect: false,
   		        paginationPageSizes: [25, 50, 75],
   		        paginationPageSize: 10,
   		        onRegisterApi: function (gridApi) { //register grid data first within the gridOptions
   		            $scope.gridApi = gridApi;
   		        },

   		        columnDefs: [{
   		                field: 'facility.consituencyId',
   		                displayName: 'CONSTITUENCY ID',
   		                cellTooltip: '',
   		                headerTooltip: 'CONSTITUENCY ID',
   		                enableHiding: false,
   		                cellTooltip: true,
   		                headerTooltip: true,
   		                index: 1,
   		                width: $scope.columnWidths[0].constituencyId
   		            }, {
   		                field: 'facility.name',
   		                displayName: 'FACILITY NAME',
   		                enableHiding: false,
   		                cellTooltip: true,
   		                headerTooltip: true,
   		                index: 2,
   		                width: $scope.columnWidths[0].name
   		            }, {
   		                field: 'filename',
   		                displayName: 'FILE NAME',
   		                cellTemplate: '<div title="{{row.entity.filename}}">' + '<a href="#reportDetail/{{row.entity.id}}">{{row.entity.filename}}</a>' + '</div>',

   		                enableCellEdit: false,
   		                showFilter: true,
   		                enableColumnResize: true,
   		                enableHiding: false,
   		                index: 3, 
   		                width: $scope.columnWidths[0].fileName
   		            }, {
   		                field: 'reportType',
   		                displayName: 'REPORT TYPE',
   		                enableHiding: false,
   		                cellTooltip: true,
   		                headerTooltip: true,
   		                index: 4, 
   		                width: $scope.columnWidths[0].reportType
   		            }, {
   		                field: 'numberOfRecords',
   		                displayName: 'REPORT MEMBER COUNT',
   		                enableHiding: false,
   		                cellTooltip: true,
   		                headerTooltip: true,
   		                index: 5,
   		                width: $scope.columnWidths[0].numberOfRecords
   		            }, {
   		                field: 'facility.memberCount',
   		                displayName: 'FACILITY MEMBER COUNT',
   		                enableHiding: false,
   		                cellTooltip: true,
   		                headerTooltip: true,
   		                index: 6,
   		                width: $scope.columnWidths[0].memberCount
   		            }, {
   		                field: 'uploadDate',
   		                displayName: 'UPLOADED DATE',
   		                cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
   		                
   		                enableHiding: false,
   		                index: 7,
   		                width: $scope.columnWidths[0].uploadDate
   		            }, {
   		                field: 'payPeriod',
   		                displayName: 'PAYMENT SCHEDULE',
   		                enableHiding: false,
   		                cellTooltip: true,
   		                headerTooltip: true,
   		                index: 8,
   		                width: $scope.columnWidths[0].payPeriod
   		            }, {
   		                field: 'status',
   		                displayName: 'FILE STATUS',
   		                enableHiding: false,
   		                cellTooltip: true,
   		                headerTooltip: true,
   		                index: 9,
   		                width: $scope.columnWidths[0].status
   		            }, {
   		                field: 'exception',
   		                displayName: 'STATUS REASON',
   		                enableHiding: false,
   		                cellTooltip: true,
   		                headerTooltip: true,
   		                index: 10,
   		                width: $scope.columnWidths[0].exception
   		            }, {
   		                field: 'dbloadDate',
   		                displayName: 'DB LOAD DATE',
   		                cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
   		                enableHiding: false,
   		                index: 11,
   		                width: $scope.columnWidths[0].dbloadDate
   		            }, {
   		                field: 'processedDate',
   		                displayName: 'PROCESSED DATE',
   		                cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
   		                enableHiding: false,
   		                index: 12,
   		                width: $scope.columnWidths[0].processedDate
   		            }, {
   		                field: 'importDate',
   		                displayName: 'IMPORTED DATE',
   		                cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
   		                enableHiding: false,
   		                index: 13,
   		                width: $scope.columnWidths[0].importDate

   		            }, {
   		                field: 'createdBy',
   		                displayName: 'UPLOADED BY',
   		                enableHiding: false,
   		                index: 14,
   		                width: $scope.columnWidths[0].createdBy
   		            },
   		            {
   		                field: 'modifiedBy',
   		                displayName: 'Modified BY',
   		                enableHiding: false,
   		                index: 15,
   		                width: $scope.columnWidths[0].modifiedBy
   		             },
   		            //    { field: 'ProcessedDate',displayName:'PROCESSED DATE',cellTooltip: '', headerTooltip: 'PROCESSED DATE',enableColumnResize : true, enableHiding:false,width:150},
   		            //    { field: 'Matched',displayName:'MATCHED',cellTooltip: '', headerTooltip: 'MATCHED' ,enableHiding:false,width:150},
   		            //    { field: 'Match'+'Update',displayName:'MATCHED+ UPDATE',cellTooltip: '', headerTooltip: 'MATCHED+ UPDATE', enableHiding:false,width:150},
   		            //    { field: 'CreateDetails',displayName:'CREATE DETAILS',cellTooltip: '', headerTooltip: 'CREATE DETAILS', enableHiding:false,width:150},

   		        ]

   		    };

    }
    $scope.filterReportGrid = {
        enableGridMenu: true,
    };

    $scope.ReportPagination = {};

    $scope.limit = localStorage.getItem(localStorage.id + "ReportPage");
    if (!$scope.limit) {
        $scope.limit = 20;
    }
    $scope.ReportpageLimitValue = localStorage.getItem(localStorage.id + "ReportPage");

    $scope.pageNumber = 0;
    $scope.presentpage = 1;
    ReportsService.ReportsTotalCount("Reports/count?", function (response) {
        if (response.status == 200) {


            $scope.Reportstotalcount = response.data.count;

            if ($scope.Reportstotalcount < $scope.limit) {
                $scope.ReportPagination.toIndex = $scope.Reportstotalcount;
            } else {
                $scope.ReportPagination.toIndex = $scope.limit;
            }
            $scope.totalpages = Math.ceil($scope.Reportstotalcount / $scope.limit);

        }

    });



    $scope.ReportPagination = {
        //limit:10,
        limit: $scope.limit,
        skip: 0,
        //pageNumber: 0,
        fromIndex: 1,
        //toIndex: $scope.toIndex,
        pageNumber: 0,
        loadSelCountPerPage: function (pageLimitValue) {
            localStorage.setItem(localStorage.id + "ReportPage", pageLimitValue);
            this.pageNumber = 0;
            this.limit = pageLimitValue;
            this.skip = 0;
            $scope.presentpage = 1;
            $scope.totalpages = Math.ceil($scope.Reportstotalcount / pageLimitValue);
            $scope.ReportPagination.fromIndex = this.skip + 1;
            if ($scope.Reportstotalcount < pageLimitValue) {
                $scope.ReportPagination.toIndex = $scope.Reportstotalcount;
            } else {
                $scope.ReportPagination.toIndex = this.skip + Number(this.limit);
            }

            /*Start API*/
            $loading.start('AllReports');
            ReportsService.ReportsList("Reports?filter[order]=uploadDate DESC&filter[include]=facility&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                //console.log(response);
                if (response.status == 200) {
                    //$scope.ReportsList = response.data;
                    $scope.gridOptionsReport.data = response.data;
                    $scope.filterReportGrid.data = response.data
                        //                //console.log(response.data);

                    $loading.finish('AllReports');

                } else {
                    $loading.finish('AllReports');
                    toaster.pop("error", response.data.error.message);
                }
            });
            /*End of API*/
        },
        nextPage: function () {
        	
        	if(angular.isObject($scope.sortCoulmnName)){
        		$scope.sortCoulmnName = "uploadDate";
        		$scope.sortOrder = "DESC";
        		
        	}else{
        		$scope.sortCoulmnName = $scope.sortCoulmnName;
        		$scope.sortOrder = $scope.sortOrder;
        	}
            if ($scope.presentpage != $scope.totalpages) {
                this.pageNumber++;
                $scope.presentpage++;
                this.skip = this.limit * this.pageNumber;
                $scope.ReportPagination.fromIndex = this.skip + 1;
                $nextMaxtoindex = Number(this.skip) + Number(this.limit);
                //$scope.ReportPagination.toIndex = Number(this.skip) + Number(this.limit);
                if ($scope.Reportstotalcount < $nextMaxtoindex) {
                    $scope.ReportPagination.toIndex = $scope.Reportstotalcount;
                } else {
                    $scope.ReportPagination.toIndex = $nextMaxtoindex;
                }
                console.log(this.skip);
                $loading.start('AllReports');
                $scope.sortPageNumber = $scope.sortPageNumber + 1;
                console.log($scope.sortPageNumber);
                ReportsService.ReportsList("Reports?filter[order]="+$scope.sortCoulmnName+" "+$scope.sortOrder+"&filter[include]=facility&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    //console.log(response);
                    if (response.status == 200) {
                        //$scope.ReportsList = response.data;
                        $scope.gridOptionsReport.data = response.data;
                        $scope.filterReportGrid.data = response.data
                            //                //console.log(response.data);

                        $loading.finish('AllReports');


                    } else {
                        $loading.finish('AllReports');
                        toaster.pop("error", response.data.error.message);
                    }
                });
            }

        },

        previousPage: function () {
        	if(angular.isObject($scope.sortCoulmnName)){
        		$scope.sortCoulmnName = "uploadDate";
        		$scope.sortOrder = "DESC";
        		
        	}else{
        		$scope.sortCoulmnName = $scope.sortCoulmnName;
        		$scope.sortOrder = $scope.sortOrder;
        	}
        	if (this.pageNumber > 0) {
                this.pageNumber--;
                $scope.presentpage--;
                this.skip = this.limit * this.pageNumber;
                $NexttoIndex = $scope.ReportPagination.toIndex - this.limit;
                if ($scope.ReportPagination.toIndex == $scope.Reportstotalcount) {
                    $scope.ReportPagination.toIndex = $scope.ReportPagination.fromIndex - 1;
                } else {
                    $scope.ReportPagination.toIndex = $NexttoIndex;
                }
                $scope.ReportPagination.fromIndex = $scope.ReportPagination.fromIndex - this.limit;
                console.log(this.skip);
                $loading.start('AllReports');
                $scope.sortPageNumber = $scope.sortPageNumber - 1;
                console.log($scope.sortPageNumber);
                ReportsService.ReportsList("Reports?filter[order]="+$scope.sortCoulmnName+" "+$scope.sortOrder+"&filter[include]=facility&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    //console.log(response);
                    if (response.status == 200) {
                        //$scope.ReportsList = response.data;
                        $scope.gridOptionsReport.data = response.data;
                        $scope.filterReportGrid.data = response.data
                            //                //console.log(response.data);

                        $loading.finish('AllReports');

                    } else {
                        $loading.finish('AllReports');
                        toaster.pop("error", response.data.error.message);
                    }
                });
            }
        }
    };
    $scope.columnSizeChanged = function(colDef,deltaChange){
    	$scope.widthChanged = "changed";
    	var columnDetails = angular.toJson(colDef);
    	console.log(deltaChange);
    	console.log(colDef.index);
    	console.log(colDef.field);
    	var changedWidth;
    	if(colDef.field == "facility.consituencyId"){
    		changedWidth = $scope.columnWidths[0].constituencyId+deltaChange;
    		$scope.columnWidths[0].constituencyId = changedWidth; 
    		console.log($scope.columnWidths[0].constituencyId);
    	}else if(colDef.field == "facility.name"){
    		changedWidth = $scope.columnWidths[0].name+deltaChange;
    		$scope.columnWidths[0].name = changedWidth; 
    		console.log($scope.columnWidths[0].name);
    	}else if(colDef.field == "filename"){
    		changedWidth = $scope.columnWidths[0].fileName+deltaChange;
    		$scope.columnWidths[0].fileName = changedWidth; 
    		console.log($scope.columnWidths[0].fileName);
    	}else if(colDef.field == "reportType"){
    		changedWidth = $scope.columnWidths[0].reportType+deltaChange;
    		$scope.columnWidths[0].reportType = changedWidth; 
    		console.log($scope.columnWidths[0].reportType);
    	}else if(colDef.field == "numberOfRecords"){
    		changedWidth = $scope.columnWidths[0].numberOfRecords+deltaChange;
    		$scope.columnWidths[0].numberOfRecords = changedWidth; 
    		console.log($scope.columnWidths[0].numberOfRecords);
    	}else if(colDef.field == "facility.memberCount"){
    		changedWidth = $scope.columnWidths[0].memberCount+deltaChange;
    		$scope.columnWidths[0].memberCount = changedWidth; 
    		console.log($scope.columnWidths[0].memberCount);
    	}else if(colDef.field == "uploadDate"){
    		changedWidth = $scope.columnWidths[0].uploadDate+deltaChange;
    		$scope.columnWidths[0].uploadDate = changedWidth; 
    		console.log($scope.columnWidths[0].uploadDate);
    	}else if(colDef.field == "payPeriod"){
    		changedWidth = $scope.columnWidths[0].payPeriod+deltaChange;
    		$scope.columnWidths[0].payPeriod = changedWidth; 
    		console.log($scope.columnWidths[0].payPeriod);
    	}else if(colDef.field == "status"){
    		changedWidth = $scope.columnWidths[0].status+deltaChange;
    		$scope.columnWidths[0].status = changedWidth; 
    		console.log($scope.columnWidths[0].status);
    	}else if(colDef.field == "exception"){
    		changedWidth = $scope.columnWidths[0].exception+deltaChange;
    		$scope.columnWidths[0].exception = changedWidth; 
    		console.log($scope.columnWidths[0].exception);
    	}else if(colDef.field == "dbloadDate"){
    		changedWidth = $scope.columnWidths[0].dbloadDate+deltaChange;
    		$scope.columnWidths[0].dbloadDate = changedWidth; 
    		console.log($scope.columnWidths[0].dbloadDate);
    	}else if(colDef.field == "processedDate"){
    		changedWidth = $scope.columnWidths[0].processedDate+deltaChange;
    		$scope.columnWidths[0].processedDate = changedWidth; 
    		console.log($scope.columnWidths[0].processedDate);
    	}else if(colDef.field == "importDate"){
    		changedWidth = $scope.columnWidths[0].importDate+deltaChange;
    		$scope.columnWidths[0].importDate = changedWidth; 
    		console.log($scope.columnWidths[0].importDate);
    	}else if(colDef.field == "createdBy"){
    		changedWidth = $scope.columnWidths[0].createdBy+deltaChange;
    		$scope.columnWidths[0].createdBy = changedWidth; 
    		console.log($scope.columnWidths[0].createdBy);
    	}else if(colDef.field == "modifiedBy"){
    		changedWidth = $scope.columnWidths[0].modifiedBy+deltaChange;
    		$scope.columnWidths[0].modifiedBy = changedWidth; 
    		console.log($scope.columnWidths[0].modifiedBy);
    	}
    	
    };
    $scope.saveChangedWidth = function(){
    	console.log($scope.columnWidths[0]);
    	if($scope.columnWidths[0].username == $rootScope.userInfo.username){
    		$http({
				method : 'PUT',
				url : "http://0.0.0.0:3000/api/settings",
				data : $scope.columnWidths[0],
				headers : {
					'Content-Type' : 'application/json'
				}
			}).then(function(response) {
				console.log("Data saved as existing user");
			}, function(err) {
				alert(angular.toJson(err));
			})
    	}else{
    		$scope.newColumnWidth = {
    					  constituencyId: $scope.columnWidths[0].constituencyId,
    					  name: $scope.columnWidths[0].name,
    					  fileName: $scope.columnWidths[0].fileName,
    					  reportType: $scope.columnWidths[0].reportType,
    					  numberOfRecords: $scope.columnWidths[0].numberOfRecords,
    					  memberCount: $scope.columnWidths[0].memberCount,
    					  uploadDate: $scope.columnWidths[0].uploadDate,
    					  payPeriod: $scope.columnWidths[0].payPeriod,
    					  status: $scope.columnWidths[0].status,
    					  exception: $scope.columnWidths[0].exception,
    					  dbloadDate: $scope.columnWidths[0].dbloadDate,
    					  processedDate: $scope.columnWidths[0].processedDate,
    					  importDate: $scope.columnWidths[0].importDate,
    					  createdBy: $scope.columnWidths[0].createdBy,
    					  modifiedBy: $scope.columnWidths[0].modifiedBy,
    					  username: $rootScope.userInfo.username
    		}
    		console.log($scope.newColumnWidth);
    		$http({
				method : 'POST',
				url : "http://0.0.0.0:3000/api/settings",
				data : $scope.newColumnWidth,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).then(function(response) {
				console.log("Data saved as new user");
			}, function(err) {
				alert(angular.toJson(err));
			})
    	}
    }

    $scope.sortChanged = function ( grid, sortColumns ) {
    	 var limit;
         limit = localStorage.getItem(localStorage.id + "ReportPage");
         if (!limit) {
             limit = 20;
         }
		 var name = sortColumns[0].name;
		 $scope.sortOrder = sortColumns[0].sort.direction;
        console.log(name);
        if(name == "facility.consituencyId"){
            $scope.sortColumnNumber = 1;
            $scope.sortCoulmnName = "facility.consituencyId";
        }else if(name == "facility.name"){
       	     $scope.sortColumnNumber = 2;
       	  $scope.sortCoulmnName = "facility.name";
        }else if(name == "filename"){
       	     $scope.sortColumnNumber = 3;
       	  $scope.sortCoulmnName = "filename";
        }else if(name == "reportType"){
       	     $scope.sortColumnNumber = 4;
       	  $scope.sortCoulmnName = "reportType";
        }else if(name == "numberOfRecords"){
       	     $scope.sortColumnNumber = 5;
       	  $scope.sortCoulmnName = "numberOfRecords";
        }else if(name == "facility.memberCount"){
       	     $scope.sortColumnNumber = 6;
       	  $scope.sortCoulmnName = "facility.memberCount";
        }else if(name == "uploadDate"){
       	     $scope.sortColumnNumber = 7;
       	  $scope.sortCoulmnName = "uploadDate"; 
        }else if(name == "payPeriod"){
       	     $scope.sortColumnNumber = 8;
       	  $scope.sortCoulmnName = "payPeriod";
        }else if(name == "status"){
       	     $scope.sortColumnNumber = 9;
       	  $scope.sortCoulmnName = "status";
        }else if(name == "exception"){
       	     $scope.sortColumnNumber = 10;
       	  $scope.sortCoulmnName = "exception";
        }else if(name == "dbloadDate"){
       	     $scope.sortColumnNumber = 11;
       	  $scope.sortCoulmnName = "dbloadDate";
        }else if(name == "processedDate"){
       	     $scope.sortColumnNumber = 12;
       	  $scope.sortCoulmnName = "processedDate";
        }else if(name == "importDate"){
       	     $scope.sortColumnNumber = 13;
       	  $scope.sortCoulmnName = "importDate";
        }else if(name == "createdBy"){
       	     $scope.sortColumnNumber = 14;
       	  $scope.sortCoulmnName = "createdBy";
        }else if(name == "modifiedBy"){
       	     $scope.sortColumnNumber = 15;
       	  $scope.sortCoulmnName = "modifiedBy";
        }
        console.log(sortColumns[0].colDef.index);
        console.log(sortColumns[0].name);
        console.log($scope.sortColumnNumber);
        console.log($scope.sortOrder);
	    if( sortColumns.length == 0 || sortColumns[0].name != $scope.filterReportGrid.columnDefs[sortColumns[0].colDef.index-1].name ){
	    	console.log("if");
	        ReportsService.ReportsList("Reports?filter[order]=uploadDate DESC&filter[include]=facility&filter[limit]=" + limit + "&filter[skip]=&filter[skip]=" + this.skip, function (response) {
	            //console.log(response);
	        	if (response.status == 200) {
                    //$scope.ReportsList = response.data;
                    $scope.gridOptionsReport.data = response.data;
                    $scope.filterReportGrid.data = response.data
                        //                //console.log(response.data);

                    $loading.finish('AllReports');


                } else {
                    $loading.finish('AllReports');
                    toaster.pop("error", response.data.error.message);
                }
	        });
	    } else {
	     var skipRecords = 0;
	     skipRecords = ($scope.sortPageNumber-1) * $scope.sortPageSize;
	     console.log(skipRecords);
	     switch( sortColumns[0].sort.direction ) {
	        case uiGridConstants.ASC:
	            ReportsService.ReportsList("Reports?filter[order]="+sortColumns[0].name+" ASC&filter[include]=facility&filter[limit]=" + limit + "&filter[skip]=" + skipRecords, function (response) {
	                //console.log(response);
	            	if (response.status == 200) {
                        //$scope.ReportsList = response.data;
                        $scope.gridOptionsReport.data = response.data;
                        $scope.filterReportGrid.data = response.data;
                            //                //console.log(response.data);

                        $loading.finish('AllReports');


                    } else {
                        $loading.finish('AllReports');
                        toaster.pop("error", response.data.error.message);
                    }
	            });
	          break;
	        case uiGridConstants.DESC:
	        	ReportsService.ReportsList("Reports?filter[order]="+sortColumns[0].name+" DESC&filter[include]=facility&filter[limit]=" + limit + "&filter[skip]=" + skipRecords, function (response) {
	                //console.log(response);
	        		if (response.status == 200) {
                        //$scope.ReportsList = response.data;
                        $scope.gridOptionsReport.data = response.data;
                        $scope.filterReportGrid.data = response.data
                            //                //console.log(response.data);

                        $loading.finish('AllReports');


                    } else {
                        $loading.finish('AllReports');
                        toaster.pop("error", response.data.error.message);
                    }
	            });
	          break;
	        case undefined:
	        	ReportsService.ReportsList("Reports?filter[include]=facility&filter[limit]=" + limit + "&filter[skip]=" + skipRecords, function (response) {
	                //console.log(response);
	                if (response.status == 200) {
	                    //$scope.ReportsList = response.data;
	                    $scope.gridOptionsReport.data = response.data;
	                    $scope.filterReportGrid.data = response.data;
	                    if ($scope.filterReportGrid.data.length != 0) {
	                        $scope.ReportPagination.toIndex = $scope.filterReportGrid.data.length;

	                    } else {
	                        $scope.ReportPagination.toIndex = 0;
	                    }

	                    //                //console.log(response.data);

	                    $loading.finish('AllReports');


	                } else {
	                    $loading.finish('AllReports');
	                    toaster.pop("error", response.data.error.message);
	                }
	            });
	      }
	    }
	  };

   
    


    //Report List API Call Function 
    $scope.ReportListAPI = function () {
        $loading.start('AllReports');
        var limit;
        limit = localStorage.getItem(localStorage.id + "ReportPage");
        if (!limit) {
            limit = 20;
        }

        ReportsService.ReportsList("Reports?filter[order]=uploadDate DESC&filter[include]=facility&filter[skip]=0", function (response) {
        	if (response.status == 200) {
        		$scope.searchData = response.data;
        		console.log($scope.searchData);
        	}
        });

        ReportsService.ReportsList("Reports?filter[order]=uploadDate DESC&filter[include]=facility&filter[limit]=" + limit + "&filter[skip]=0", function (response) {
            //console.log(response);
            if (response.status == 200) {
                //$scope.ReportsList = response.data;
                $scope.gridOptionsReport.data = response.data;
                $scope.filterReportGrid.data = response.data;
                
                if ($scope.filterReportGrid.data.length != 0) {
                    $scope.ReportPagination.toIndex = $scope.filterReportGrid.data.length;

                } else {
                    $scope.ReportPagination.toIndex = 0;
                }

                //                //console.log(response.data);
                console.log($scope.filterReportGrid.data.length);
                $scope.sortPageSize = $scope.filterReportGrid.data.length;
                $loading.finish('AllReports');


            } else {
                $loading.finish('AllReports');
                toaster.pop("error", response.data.error.message);
            }
        });


    };

    //Report List API Call Function End Here 

    $scope.ReportListAPI();
   



    $scope.filterReportGrid.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;
        console.log($scope.gridApi.colResizable);
        $scope.gridApi.core.on.sortChanged( $scope, $scope.sortChanged );
        $scope.gridApi.colResizable.on.columnSizeChanged($scope,$scope.columnSizeChanged);
        var val = gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            //console.log(row.entity);
            $scope.enableDelete = false;
            $scope.enableDownload = false;
            if (row.isSelected == true) {
                $scope.enableDownload = true;
                $scope.downloadUrl = ReportsService.downloadUploadedReport(row.entity.id, row.entity.filename);
                //console.log($scope.downloadUrl);
            }


            if (row.entity.status != "imported" && row.isSelected == true) {
                $scope.enableDelete = true;
                $scope.deleteingId = row.entity.id;
                ////console.log($scope.deleteingId);
            } else {
                $scope.enableDelete = false;
            }
        });

    };


    /* Delete Records starts here */
    $scope.deleteReport = function () {
        $loading.start('AllReports');
        ReportsService.DeleteAReport("Reports/" + $scope.deleteingId, function (response) {

            if (response.status == 200) {
                ////console.log(response.data);
                $loading.start('AllReports');

                $scope.ReportListAPI();
                $scope.enableDelete = false;
                $scope.enableDownload = false;
                toaster.pop("success", "Successfully Deleted The Record");

            } else {
                $loading.finish('AllReports');
                toaster.pop("error", response.data.error.message);
            }
        });

    };

    /*$scope.downloadReport = function() {
      $scope.downloadUrl=ReportsService.downloadUploadedReport(row.entity.id,row.entity.filename); 

    };*/

    /* Delete Records stops here */


    $scope.filterReportGrid.enableHorizontalScrollbar = 2;
    /*Filter grid start*/
    $scope.filterReportGrid.data = $scope.gridOptionsReport.data;
    
    /*filtering data on input text*/
    $scope.refreshData = function (termObj) {
        $scope.filterReportGrid.data = $scope.searchData;
        if(termObj){
        	while (termObj) {
                var oSearchArray = termObj.split(' ');
                console.log($scope.filterReportGrid.data);
                $scope.filterReportGrid.data = $filter('filter')($scope.filterReportGrid.data, oSearchArray[0], undefined);
                console.log($scope.filterReportGrid.data);
                oSearchArray.shift();
                termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
            }
            $scope.ReportPagination.toIndex = $scope.filterReportGrid.data.length;
        }else{
        	$scope.ReportListAPI();
        }
        //
    };
   

    /* end filtering data on input text*/


    /* Filter Query starts here */

    $scope.FacilityFilter = function (selected) {

        $scope.fromDate = "";
        $scope.toDate = "";
        $scope.reportTypeVal = "";
        $scope.fileStage = "";
        if (selected) {

            $scope.filterByfacility = selected.originalObject.facilityAccountId;
            //console.log($scope.filterByfacility);
            $loading.start('AllReports');
            ReportsService.FilterByParam("&filter[order]=uploadDate DESC&filter[include]=facility&filter[where][facilityAccountId]=" + $scope.filterByfacility, function (response) {

                //console.log(response.data);
                if (response.status == 200) {

                    $scope.gridOptionsReport.data = response.data;
                    $scope.filterReportGrid.data = response.data;
                    $scope.ReportPagination.toIndex = $scope.filterReportGrid.data.length;
                    //                //console.log(response.data);

                    $loading.finish('AllReports');

                } else {
                    $loading.finish('AllReports');
                    toaster.pop("error", response.data.error.message);
                }
            });

        } else {
            $scope.ReportListAPI();
        }

    };

    $scope.FilterByDate = function (formDate, ToDate) {

        $scope.reportTypeVal = "";
        $scope.fileStage = "";


        $rootScope.$broadcast('angucomplete-alt:clearSearch');
        if ((formDate) && (ToDate)) {
            $scope.formDate = DateConversionService.Convert(formDate);
            $scope.ToDate = DateConversionService.Convert(ToDate);


            $loading.start('AllReports');
            ReportsService.FilterByParam("&filter[include]=facility&filter[order]=uploadDate DESC&filter[where][uploadDate][between][0]=" + $scope.formDate + "&filter[where][uploadDate][between][1]=" + $scope.ToDate, function (response) {

                //console.log(response.data);
                if (response.status == 200) {

                    $scope.gridOptionsReport.data = response.data;
                    $scope.filterReportGrid.data = response.data
                    $scope.ReportPagination.toIndex = $scope.filterReportGrid.data.length;
                    //                //console.log(response.data);

                    $loading.finish('AllReports');

                } else {
                    $loading.finish('AllReports');
                    toaster.pop("error", response.data.error.message);
                }
            });
        } else {
            $scope.ReportListAPI();
        }

    };

    $scope.FilterByReportType = function (selectedVal) {
        $scope.fromDate = "";
        $scope.toDate = "";
        $scope.fileStage = "";
        $rootScope.$broadcast('angucomplete-alt:clearSearch');

        if (selectedVal) {
            if (selectedVal == "All") {

                $loading.start('AllReports');
                ReportsService.FilterByParam("&filter[include]=facility&filter[order]=uploadDate DESC", function (response) {

                    //console.log(response.data);
                    if (response.status == 200) {

                        $scope.gridOptionsReport.data = response.data;
                        $scope.filterReportGrid.data = response.data;
                        $scope.ReportPagination.toIndex = $scope.filterReportGrid.data.length;
                        ////console.log(response.data);

                        $loading.finish('AllReports');

                    } else {
                        $loading.finish('AllReports');
                        toaster.pop("error", response.data.error.message);
                    }
                });

            } else {

                $loading.start('AllReports');
                ReportsService.FilterByParam("&filter[include]=facility&filter[order]=uploadDate DESC&filter[where][reportType]=" + selectedVal, function (response) {

                    //console.log(response.data);
                    if (response.status == 200) {

                        $scope.gridOptionsReport.data = response.data;
                        $scope.filterReportGrid.data = response.data;
                        $scope.ReportPagination.toIndex = $scope.filterReportGrid.data.length;

                        ////console.log(response.data);

                        $loading.finish('AllReports');

                    } else {
                        $loading.finish('AllReports');
                        toaster.pop("error", response.data.error.message);
                    }
                });
            }
        } else {
            $scope.ReportListAPI();
        }

    };


    $scope.FilterByFileStage = function (selectedVal) {
        $scope.fromDate = "";
        $scope.toDate = "";
        $scope.reportTypeVal = "";
        $rootScope.$broadcast('angucomplete-alt:clearSearch');
        if (selectedVal) {

            $loading.start('AllReports');
            ReportsService.FilterByParam("&filter[include]=facility&filter[order]=uploadDate DESC&filter[where][status]=" + selectedVal, function (response) {

                //console.log(response.data);
                if (response.status == 200) {

                    $scope.gridOptionsReport.data = response.data;
                    $scope.filterReportGrid.data = response.data;
                    $scope.ReportPagination.toIndex = $scope.filterReportGrid.data.length;
                    ////console.log(response.data);

                    $loading.finish('AllReports');

                } else {
                    $loading.finish('AllReports');
                    toaster.pop("error", response.data.error.message);
                }
            });
        } else {
            $scope.ReportListAPI();
        }

    };

    /* Filter Query ends here */

    /* date picker starts here */
    $scope.today = function () {
        $scope.dt = new Date();
    };
    //  $scope.today();

    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.dateOptions1 = {
        startingDay: 0
    };

    // Disable weekend selection
    /*$scope.disabled = function(date, mode) {
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 7);
    };*/

    $scope.open1 = function () {
        $scope.popup1.opened = true;
    };

    $scope.open2 = function () {
        $scope.popup2.opened = true;
    };

    /*$scope.setDate = function(year, month, day) {
      $scope.dt = new Date(year, month, day);
    };*/

    /*selecting Pay Period Calender*/


    $scope.PayPeriodCalenderFromDate = function () {
        $scope.payperiodpopupFromCalender.opened = true;

    };
    $scope.PayPeriodCalenderToDate = function () {
        $scope.payperiodpopupToCalender.opened = true;
    };
    $scope.payperiodpopupFromCalender = {
        opened: false
    };

    $scope.payperiodpopupToCalender = {
        opened: false
    };

    /* date picker start here */
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['MM-dd-yyyy', 'dd.MM.yyyy', 'dd-MMMM-yyyy', 'yyyy/MM/dd', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup1 = {
        opened: false
    };

    $scope.popup2 = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [{
        date: tomorrow,
        status: 'full'
    }, {
        date: afterTomorrow,
        status: 'partially'
    }];

    $scope.getDayClass = function (date, mode) {
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    };

    /* date picker ends here */

    $scope.facilityObj = {};
    //FacilityList API call
    FacilityService.FacilityDropdown("Facilities", function (response) {

        if (response.status == 200) {
            //console.log(response.data);

            $scope.facilityObj = response.data;

            $loading.finish('facilityTab');
        }

    });


    $scope.reset = function () {
        $scope.$broadcast('show-errors-reset');
        $scope.uploadObj = {};

    }

    $scope.openUploadModal = function () {

        $scope.modalInstance = $uibModal.open({

            templateUrl: 'FileUploadModalContent.html',
            controller: 'fileUploadController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                facilityAutocompleteList: function () {
                    return $scope.facilityObj;
                }
            }

        });
    };

    /* prevent dot for input */

    /* prevent dot for input */

}]);

angular.module('cnaApp').controller('fileUploadController', ['$scope', '$timeout', '$loading', 'toaster', '$uibModalInstance', 'facilityAutocompleteList', 'FacilityService', 'ReportsService','DateConversionFilter', function fileUploadController($scope, $timeout, $loading, toaster, $uibModalInstance, facilityAutocompleteList, FacilityService, ReportsService,DateConversionFilter) {

    $scope.facilityAutocompleteObj = facilityAutocompleteList;


    $scope.fileObj = {};
    $scope.uploadedData = {};
    $scope.uploadObj = {};

    $scope.checkFileExt = function (val) {
        if (val) {

            var uploadFileExt = val.substr(val.lastIndexOf('.') + 1);
            if ((uploadFileExt.toLowerCase() == 'txt') || (uploadFileExt.toLowerCase() == 'csv')) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    };
    $scope.payperiodSelection = function (val) {

        if (val) {
            //console.log(val);


            if (val == 'calender') {

                $scope.payperiodDateSelection = true;
                $scope.payperiodlistSelection = false;

            } else {
                $scope.payperiodlistSelection = true;
                $scope.payperiodDateSelection = false;

            }
        } else {
            return false;
        }

    };

    function Convert(str) {
        var date = new Date(str),
            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
            day = ("0" + date.getDate()).slice(-2);
        return [date.getFullYear(), mnth, day].join("-");
    }


    // SetDate for making auto input field of ToDate.
    /* $scope.uploadObj.reportPeriodTo = undefined;
     $scope.uploadObj.reportPeriodFrom = undefined;
     $scope.ResetDate = function() {
         $scope.uploadObj.reportPeriodTo = undefined;
         $scope.uploadObj.reportPeriodFrom = undefined;

     };*/
    /*date calculation */

    Date.isLeapYear = function (year) {
        return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
    };

    Date.getDaysInMonth = function (year, month, type) {
        //return [31, (Date.isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
        if (type == 1) {
            //console.log("month");
            return [30, (Date.isLeapYear(year) ? 28 : 27), 30, 29, 30, 29, 30, 30, 29, 30, 29, 30][month];

        } else if (type == 2) { //type  2 for calculating semi month date


            return [15, (Date.isLeapYear(year) ? 13 : 12), 15, 14, 15, 14, 15, 15, 14, 15, 14, 15][month];

        }

    };

    Date.prototype.isLeapYear = function () {
        return Date.isLeapYear(this.getFullYear());
    };

    /*Date.prototype.getDaysInMonth = function() {
        return Date.getDaysInMonth(this.getFullYear(), this.getMonth());
    };*/

    /* Date.prototype.addMonths = function(value) {
         var n = this.getDate();
        
         this.setDate(1);
         this.setMonth(this.getMonth() + value);
         this.setDate(Math.min(n, this.getDaysInMonth()));
       
         return this.getDaysInMonth();

     };
     Date.prototype.substractMonths = function(value) {
         var n = this.getDate();
         this.setDate(1);
         this.setMonth(this.getMonth() - value);
         this.setDate(Math.min(n, this.getDaysInMonth()));
         return this;
     };*/

    /*date calculation end here */

    $scope.SetToDate = function () {
        var flexibleValue = undefined;

        if ($scope.uploadObj.payPeriod == "Weekly") { //weekly selectable calender(add and substract 7 days)
            var flexibleValue = 6;
            var temp = new Date($scope.uploadObj.reportPeriodFrom);
            temp.setDate(temp.getDate() + flexibleValue);
            $scope.uploadObj.reportPeriodTo = temp;
            //$scope.reportPeriodTo=$scope.reportPeriodFrom.getDate()+7;

        } else if ($scope.uploadObj.payPeriod == "Bi-Weekly") { //Bi-weekly selectable calender(add and substract 14 days)
            var flexibleValue = 13;
            var temp = new Date($scope.uploadObj.reportPeriodFrom);
            temp.setDate(temp.getDate() + flexibleValue);
            $scope.uploadObj.reportPeriodTo = temp;

        } else if ($scope.uploadObj.payPeriod == "Semi-Monthly") { //Semi-Monthly selectable calender(add and substract 60 days)

            var temp = new Date($scope.uploadObj.reportPeriodFrom);
            //console.log(temp.getDate());
            if (temp.getDate() == 1) {
                temp.setDate(temp.getDate() + 14);
                $scope.uploadObj.reportPeriodTo = temp;
            } else if (temp.getDate() == 16) {
                temp.setDate(temp.getDate() + Date.getDaysInMonth(temp.getFullYear(), temp.getMonth(), 2));
                $scope.uploadObj.reportPeriodTo = temp;

            } else {
                $scope.uploadObj.reportPeriodTo = "";
            }
            //temp.setDate(temp.getDate() + Date.getDaysInMonth(temp.getFullYear(), temp.getMonth(), 2));
            //$scope.uploadObj.reportPeriodTo = temp;

        } else if ($scope.uploadObj.payPeriod == "Monthly") { //Monthly selectable calender(add and substract 30 days)

            var temp = new Date($scope.uploadObj.reportPeriodFrom);
            temp.setDate(temp.getDate() + Date.getDaysInMonth(temp.getFullYear(), temp.getMonth(), 1));
            $scope.uploadObj.reportPeriodTo = temp;
            /*var temp = new Date($scope.uploadObj.reportPeriodFrom);
            //console.log(Date.getDaysInMonth(temp.getFullYear(), temp.getMonth()));*/
            //var monthlyFromDate = new Date($scope.uploadObj.reportPeriodFrom);

            // $scope.uploadObj.reportPeriodTo = monthlyFromDate.addMonths(1);

        }
    }; //End SetDate for making auto input field of ToDate.
    //SetDate for making auto input field of FromDate.
    $scope.SetFromDate = function () {
        var flexibleValue = undefined;

        if ($scope.uploadObj.payPeriod == "Weekly") { //weekly selectable calender(add and substract 7 days)
            var flexibleValue = 6;
            var temp = new Date($scope.uploadObj.reportPeriodTo);
            temp.setDate(temp.getDate() - flexibleValue);
            $scope.uploadObj.reportPeriodFrom = temp;
            //$scope.reportPeriodTo=$scope.reportPeriodFrom.getDate()+7;

        } else if ($scope.uploadObj.payPeriod == "Bi-Weekly") { //Bi-weekly selectable calender(add and substract 14 days)
            var flexibleValue = 13;
            var temp = new Date($scope.uploadObj.reportPeriodTo);
            temp.setDate(temp.getDate() - flexibleValue);
            $scope.uploadObj.reportPeriodFrom = temp;

        } else if ($scope.uploadObj.payPeriod == "Semi-Monthly") { //Semi-Monthly selectable calender(add and substract 60 days)

            var temp = new Date($scope.uploadObj.reportPeriodTo);
            ////console.log(temp.getMonth());
            /* if (temp.getDate() == 15) {
                 temp.setDate(temp.getDate() - 14);
                 $scope.uploadObj.reportPeriodFrom = temp;
             }*/

            if (temp.getMonth() == 1) { //temp.getMonth() == 1 (it checks for  the 2nd month)
                if (temp.getDate() == 28 || temp.getDate() == 29 || temp.getDate() == 15) {
                    temp.setDate(temp.getDate() - Date.getDaysInMonth(temp.getFullYear(), temp.getMonth(), 2));
                    $scope.uploadObj.reportPeriodFrom = temp;
                } else {
                    $scope.uploadObj.reportPeriodFrom = "";
                }

            } else {
                if (temp.getDate() == 30 || temp.getDate() == 31 || temp.getDate() == 15) {
                    temp.setDate(temp.getDate() - Date.getDaysInMonth(temp.getFullYear(), temp.getMonth(), 2));
                    $scope.uploadObj.reportPeriodFrom = temp;
                } else {
                    $scope.uploadObj.reportPeriodFrom = "";
                }

            }

        } else if ($scope.uploadObj.payPeriod == "Monthly") { //Monthly selectable calender(add and substract 30 days)
            var flexibleValue = 30;
            var temp = new Date($scope.uploadObj.reportPeriodTo);
            temp.setDate(temp.getDate() - Date.getDaysInMonth(temp.getFullYear(), temp.getMonth(), 1));
            $scope.uploadObj.reportPeriodFrom = temp;

        }
    }; //End SetDate for making auto input field of FromDate.


    /* End selecting Pay Period Calender*/

    $scope.FacilityUpload = function (selected) {
        if (selected) {
            $scope.uploadObj.facilityAccountId = selected.originalObject.facilityAccountId;
            FacilityService.GetFacility("&filter[where][facilityAccountId]=" + selected.originalObject.facilityAccountId, function (response) {
                if (response.status == 200) {
                    $scope.uploadObj.facilityId = response.data[0].id;

                }

            });

        }

    };


    $scope.GetPayPeriodDate = function () {

        ReportsService.FacilityPayPeriod($scope.uploadObj.facilityId + "/payPeriods?reportType=" + $scope.uploadObj.reportType + "&payPeriod=" + $scope.uploadObj.payPeriod, function (response) {
            if (response.status == 200) {
                $scope.payPeriodDateRange = response.data;
                /*$scope.PayPeriod=response.data[response.data.length - 1];

                $scope.uploadObj.reportPeriodFrom=Convert($scope.PayPeriod.reportPeriodFrom);
                $scope.uploadObj.reportPeriodTo=Convert($scope.PayPeriod.reportPeriodTo);*/

            }

        });


    };

    $scope.CustomDateConversionFilter=function(str){ alert(str);
        var val=DateConversionFilter.Convert(str);
        return val;

    };


    $scope.UploadFileData = function () {
        $scope.$broadcast('show-errors-check-validity');
        ////console.log($scope.uploadObj);


        if (!$scope.fileObj.file) {

            toaster.pop("error", "Please a file");
            return false;

        }
        if ($scope.uploadObj.reportType != "PD Application") {
            if (!$scope.uploadObj.facilityAccountId) {

                toaster.pop("error", "Please Select facility ");
                return false;

            }

        }
        if ($scope.uploadObj.PayPeriodType) {

            if ($scope.uploadObj.PayPeriodType == 'calender') {
                if ((!$scope.uploadObj.reportPeriodFrom) || (!$scope.uploadObj.reportPeriodTo)) {
                    toaster.pop("error", "Please Select PayPeriod dates");
                    return false;

                }

            } else {
                if (!$scope.uploadObj.reportPeriodDate) {
                    toaster.pop("error", "Please Select List of PayPeriods");
                    return false;

                }

            }


        } else {
            toaster.pop("error", "Please Select Pay Period Type ");
            return false;

        }


        if ($scope.FileuploadForm.$valid) {

            //$scope.uploadObj.reportPeriodFrom = Convert($scope.uploadObj.reportPeriodFrom);
            // $scope.uploadObj.reportPeriodTo = Convert($scope.uploadObj.reportPeriodTo);

            //$scope.uploadObj.filename = $scope.fileObj.file.name;
            //$scope.uploadedData = $scope.uploadObj;


            if ($scope.uploadObj.reportType == 'PD Application') {
                $scope.uploadedData.filename = $scope.fileObj.file.name;
                if ($scope.uploadObj.PayPeriodType == 'calender') {
                    $scope.uploadedData.reportPeriodFrom = Convert($scope.uploadObj.reportPeriodFrom);
                    $scope.uploadedData.reportPeriodTo = Convert($scope.uploadObj.reportPeriodTo);

                } else {
                    var tmpPayPeriodDate = $scope.uploadObj.reportPeriodDate.split(",");

                    $scope.uploadedData.reportPeriodFrom = tmpPayPeriodDate[0];
                    $scope.uploadedData.reportPeriodTo = tmpPayPeriodDate[1];
                }

                $scope.uploadedData.reportType = $scope.uploadObj.reportType;
                $scope.uploadedData.delemeter = $scope.uploadObj.delemeter;
                $scope.uploadedData.headerRow = $scope.uploadObj.headerRow;
                $scope.uploadedData.payPeriod = $scope.uploadObj.payPeriod;
                //console.log($scope.uploadedData);

            } else {
                //console.log($scope.uploadObj.reportPeriodDate);
                $scope.uploadedData.filename = $scope.fileObj.file.name;
                $scope.uploadedData.facilityId = $scope.uploadObj.facilityId;
                $scope.uploadedData.facilityAccountId = $scope.uploadObj.facilityAccountId;
                if ($scope.uploadObj.PayPeriodType == 'calender') {
                    $scope.uploadedData.reportPeriodFrom = Convert($scope.uploadObj.reportPeriodFrom);
                    $scope.uploadedData.reportPeriodTo = Convert($scope.uploadObj.reportPeriodTo);


                } else {
                    var tmpPayPeriodDate = $scope.uploadObj.reportPeriodDate.split(",");

                    $scope.uploadedData.reportPeriodFrom = tmpPayPeriodDate[0];
                    $scope.uploadedData.reportPeriodTo = tmpPayPeriodDate[1];
                }
                /* $scope.uploadedData.reportPeriodFrom = Convert($scope.uploadObj.reportPeriodFrom);
                 $scope.uploadedData.reportPeriodTo = Convert($scope.uploadObj.reportPeriodTo);*/
                $scope.uploadedData.reportType = $scope.uploadObj.reportType;
                $scope.uploadedData.delemeter = $scope.uploadObj.delemeter;
                $scope.uploadedData.headerRow = $scope.uploadObj.headerRow;
                $scope.uploadedData.payPeriod = $scope.uploadObj.payPeriod;
                //console.log($scope.uploadedData);

            }

            //upload data API Call
            $loading.start('uploadLoad');

            ReportsService.UploadRecord("Reports", $scope.uploadedData, function (response) {
                    //console.log(response);
                    if (response.status == 200) {

                        var fd = new FormData();
                        angular.forEach($scope.fileObj, function (val, key) {
                            fd.append(key, $scope.fileObj[key]);
                        });

                        //console.log(fd);
                        //console.log(response.data.id);
                        ReportsService.UploadFile("Reports/" + response.data.id + "/file", fd, function (response) {
                            if (response.status == 200) {
                                $loading.finish('uploadLoad');
                                $scope.ReportListAPI();

                                toaster.pop("success", "Uploaded Successfully");
                                ReportTimer = $timeout(function () {
                                    // 0 ms delay to reload the page.
                                    $scope.ReportListAPI();
                                    //console.log("times");
                                }, 30000);


                                //$timeout.cancel(ReportTimer);
                                $uibModalInstance.close();

                            } else {

                                $loading.finish('uploadLoad');
                                toaster.pop("error", response.data.error.message);
                                $uibModalInstance.close();


                            }
                        })
                    } else if (response.status == 500) {

                        $loading.finish('uploadLoad');
                        toaster.pop("error", "File already exists !");

                        //$uibModalInstance.close();
                    } else {
                        $loading.finish('uploadLoad');
                        toaster.pop("error", response.data.error.name, response.data.error.message);
                        $uibModalInstance.close();

                    }
                })
                //upload data API ends here

        }
    };


    $scope.CloselUploadModal = function () {
        $uibModalInstance.close();
    };


}]);



/**
 * Defines adminUser dashboard controller.
 * @param {!angular.$scope} $scope 
 * @constructor
 * @export cnaApp
 * @ngInject
 */

angular.module('cnaApp').controller("adminDashboardController", ['$scope', '$rootScope', '$filter', '$state', 'toaster', '$loading', '$uibModal', 'AdminUser', 'FacilityService', 'CBARulesService', 'DateConversionService', 'DataService', '$http', 'uiGridConstants', function adminDashboardController($scope, $rootScope, $filter, $state, toaster, $loading, $uibModal, AdminUser, FacilityService, CBARulesService, DateConversionService, DataService, $http, uiGridConstants) {
    AdminUser.userDetail("AdminUsers/" + localStorage.id, function (response) {

        if (response.status == 200) {
            $scope.userInfo = response.data;
            if (response.data.role == 'user') {
                $state.go("importDashboard", {
                    reload: true
                });

            }


        }
    });
    $scope.isSelected = 1;

    $rootScope.UserActionValue = "Create User";
    $rootScope.enableUserDelete = false;
    $rootScope.CreateUserObj = {};
    $scope.listOfUsers = {};
    $scope.sortPageNumber = 1;
    $scope.sortPageSize = 0;
    $scope.sortCoulmnName = "username";
	$scope.sortOrder = "ASC";
    $scope.filterGridRowHight = undefined;
    $scope.gridOptionsUser = {};
    $scope.cbaTableWidths = [];
    $scope.filterUserGrid = {

    };
    var cbaInit = function(){
    	$http({
			method : 'GET',
			url : "http://0.0.0.0:3000/api/CBATables?filter[where][username]="+$rootScope.userInfo.username,
			headers : {
				'Content-Type' : 'application/json'
			}
		}).then(function(response) {
			$scope.cbaTableWidths = response.data;
			if($scope.cbaTableWidths.length<=0){
				$http({
					method : 'GET',
					url : "http://0.0.0.0:3000/api/CBATables?filter[where][username]=default",
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function(response) {
					$scope.cbaTableWidths = response.data;
					console.log($scope.cbaTableWidths[0]);
				}, function(err) {
					alert(angular.toJson(err));
				})
			}else{
			}
			console.log($scope.cbaTableWidths[0]);
		}, function(err) {
			alert(angular.toJson(err));
			
		})
    }
    cbaInit();
    
    $scope.userTableWidths = [];

    var userInit = function(){
    	$http({
			method : 'GET',
			url : "http://0.0.0.0:3000/api/UserTableWidths?filter[where][username]="+$rootScope.userInfo.username,
			headers : {
				'Content-Type' : 'application/json'
			}
		}).then(function(response) {
			$scope.userTableWidths = response.data;
			if($scope.userTableWidths.length<=0){
				$http({
					method : 'GET',
					url : "http://0.0.0.0:3000/api/UserTableWidths?filter[where][username]=default",
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function(response) {
					$scope.userTableWidths = response.data;
					console.log($scope.userTableWidths[0]);
					$scope.UserWidthAssign();
				}, function(err) {
					alert(angular.toJson(err));
				})
			}else{
				$scope.UserWidthAssign();
			}
			console.log($scope.userTableWidths[0]);
		}, function(err) {
			alert(angular.toJson(err));
			
		})
    }
    userInit();
    
    $scope.UserWidthAssign = function(){
        $scope.filterUserGrid = {
   			  useExternalSorting: true,
   		        enableSelectAll: false,
   		        multiSelect: false,
   		        onRegisterApi: function (gridApi) { //register grid data first within the gridOptions
   		            $scope.gridApi = gridApi;
   		        },

   	            columnDefs: [{
                    field: 'username',
                    displayName: 'USER NAME',
                    enableHiding: false,
                    cellTooltip: true,
                    headerTooltip: true,
                    width: $scope.userTableWidths[0].userName
                }, {
                    field: 'firstName',
                    displayName: 'FIRST NAME',
                    enableHiding: false,
                    cellTooltip: true,
                    headerTooltip: true,
                    width: $scope.userTableWidths[0].firstName
                }, {
                    field: 'lastName',
                    displayName: 'LAST NAME',
                    enableHiding: false,
                    cellTooltip: true,
                    headerTooltip: true,
                    width: $scope.userTableWidths[0].lastName
                }, {
                    field: 'memberCount',
                    displayName: 'TOTAL INDIVIDUALS',
                    enableHiding: false,
                    cellTooltip: true,
                    headerTooltip: true,
                    width: $scope.userTableWidths[0].numberOfIndividuals
                }, {
                    field: 'role',
                    displayName: 'ROLE',
                    enableHiding: false,
                    cellTooltip: true,
                    headerTooltip: true,
                    width: $scope.userTableWidths[0].role
                },

            ],
   		    };

    }

    $scope.userList = {};
    $scope.CBAfacilityAccountID = undefined;
    $scope.selectedFacilityObj = [];

    //console.log("one time")
    /*Modal PopUp for creating new user*/

    $scope.OpenUserModal = function () {
        //console.log("usermodal");
        $scope.modalInstance = $uibModal.open({
            templateUrl: 'UserModalContent.html',
            controller: 'UserModalController',
            scope: $scope,
        });
    };


    /* pagination for user */
    $scope.limit = 20;
    $scope.pageNumber = 0;
    $scope.Userpresentpage = 1;

    $scope.UserPagination = {};

    AdminUser.userListCount("AdminUsers/count", function (response) {
        if (response.status == 200) {
            ////console.log(response);
            $scope.Usertotalcount = response.data.count;
            if ($scope.Usertotalcount < $scope.limit) {
                $scope.UserPagination.toIndex = $scope.Usertotalcount;
            } else {
                $scope.UserPagination.toIndex = $scope.limit;
            }

            $scope.Usertotalpages = Math.ceil($scope.Usertotalcount / $scope.limit);
        }
    });

    $scope.UserPagination = {
        //limit:10,
        limit: $scope.limit,
        skip: 0,
        //pageNumber: 0,
        fromIndex: 1,
        toIndex: $scope.limit,
        pageNumber: 0,
        loadSelCountPerPage: function (pageLimitValue) {
            this.pageNumber = 0;
            this.limit = pageLimitValue;
            this.skip = 0;
            $scope.Userpresentpage = 1;
            $scope.Usertotalpages = Math.ceil($scope.Usertotalcount / pageLimitValue);
            $scope.UserPagination.fromIndex = this.skip + 1;

            if ($scope.Usertotalcount < pageLimitValue) {
                $scope.UserPagination.toIndex = $scope.Usertotalcount;
            } else {
                $scope.UserPagination.toIndex = this.skip + Number(this.limit);
            }


            /*Start API*/
            $scope.userTabAPI(this.limit, this.skip);
            /*End of API*/
        },
        nextPage: function () {
            if ($scope.Userpresentpage != $scope.Usertotalpages) {
            	if(angular.isObject($scope.sortCoulmnName)){
            		$scope.sortCoulmnName = "username";
            		$scope.sortOrder = "DESC";
            		
            	}else{
            		$scope.sortCoulmnName = $scope.sortCoulmnName;
            		$scope.sortOrder = $scope.sortOrder;
            	}
                this.pageNumber++;
                $scope.Userpresentpage++;
                $scope.sortPageNumber = $scope.sortPageNumber + 1;
                this.skip = this.limit * this.pageNumber;
                $scope.UserPagination.fromIndex = this.skip + 1;
                //$scope.UserPagination.toIndex = Number(this.skip) + Number(this.limit);
                $nextMaxtoindex = Number(this.skip) + Number(this.limit);
                if ($scope.Usertotalcount < $nextMaxtoindex) {
                    $scope.UserPagination.toIndex = $scope.Usertotalcount;
                } else {
                    $scope.UserPagination.toIndex = $nextMaxtoindex;
                }
                /*Start API*/
                $scope.userTabAPI(this.limit, this.skip);
                /*End of API*/
            }

        },

        previousPage: function () {
            if (this.pageNumber > 0) {
            	if(angular.isObject($scope.sortCoulmnName)){
            		$scope.sortCoulmnName = "username";
            		$scope.sortOrder = "DESC";
            		
            	}else{
            		$scope.sortCoulmnName = $scope.sortCoulmnName;
            		$scope.sortOrder = $scope.sortOrder;
            	}
                this.pageNumber--;
                $scope.Userpresentpage--;
                this.skip = this.limit * this.pageNumber;
                $scope.sortPageNumber = $scope.sortPageNumber - 1;
                $NexttoIndex = $scope.UserPagination.toIndex - this.limit;

                if ($scope.UserPagination.toIndex == $scope.Usertotalcount) {
                    $scope.UserPagination.toIndex = $scope.UserPagination.fromIndex - 1;
                } else {
                    $scope.UserPagination.toIndex = $NexttoIndex;
                }
                $scope.UserPagination.fromIndex = $scope.UserPagination.fromIndex - this.limit;

                /*Start API*/
                $scope.userTabAPI(this.limit, this.skip);
                /*End of API*/
            }
        }
    };
    /* pagination for user */


    // API call for Userist only for Admin.
    if(angular.isObject($scope.sortCoulmnName)){
		$scope.sortCoulmnName = "username";
		$scope.sortOrder = "DESC";
		
	}else{
		$scope.sortCoulmnName = $scope.sortCoulmnName;
		$scope.sortOrder = $scope.sortOrder;
	}
    $scope.userTabAPI = function (limit, skip) {
        $loading.start('userTab');
        //alert($scope.sortCoulmnName+"..."+$scope.sortOrder);
        AdminUser.userList("&filter[order]="+$scope.sortCoulmnName+" "+$scope.sortOrder+"&filter[limit]=" + limit + "&filter[skip]=" + skip, function (response) {

            if (response.status == 200) {
                //$scope.userList = response.data;
                $scope.gridOptionsUser.data = response.data;
                $scope.filterUserGrid.data = response.data
                $scope.sortPageSize = $scope.filterUserGrid.data.length;
                    ////console.log(response.data);

                $loading.finish('userTab');

            } else {
                $loading.finish('userTab');

                toaster.pop("error", "User " + response.data.error.message);

            }
        });

    };
    $scope.userTabAPI(20, 0);

    // API call for Userist API for table end here. 

    /*API Call for getting all userlist for assign*/
    AdminUser.AssignUserList("AdminUsers", function (response) {

        if (response.status == 200) {
            $scope.userList = response.data;

            $loading.finish('userTab');

        }
    });

    /*API Call for getting all userlist for assign ends here*/


    /*Delete user API Call*/
    $scope.deleteUser = function () {
        $loading.start("userTab");
        AdminUser.DeleteUser("AdminUsers/" + $rootScope.CreateUserObj.id, function (response) {
            if (response.status == 200) {
                $loading.finish("userTab");
                toaster.pop("success", "Selected User has been deleteed successfully.");
                $rootScope.UserActionValue = "Create User";
                $rootScope.enableUserDelete = false;
                $rootScope.CreateUserObj = {};
                $scope.userTabAPI(20, 0);
            } else {
                $loading.finish("userTab");
                $rootScope.UserActionValue = "Create User";
                $rootScope.enableUserDelete = false;
                $rootScope.CreateUserObj = {};
                toaster.pop("error", response.data.error.message);
            }


        });
    };

    /*Delete user API Call end here*/


    //to change the prority
    $scope.ChageOrder = function () {

        /*//console.log($scope.itemsList);

        var hello = {
          hello: 'world',
          foo: 'bar'
        };
        var qaz = {
            hello: 'stevie',
            foo: 'baz'
        };
        var searchTerm = "stevie";
        function arrayObjectIndexOf(myArray, searchTerm, property) {
            for(var i = 0;len = myArray.length; i < len; i++) {
                if (myArray[i][property] === searchTerm) return i;
            }
            return -1;
        }
        arrayObjectIndexOf(arr, "stevie", "hello");*/

        //to change the prority ends here


        /* for (i = 0; i <= 5; i += 1) {
           $scope.itemsList.items1.push({'Id': i, 'Label': 'Biweekliy ' + i});
         }*/

        /*for (i = 0; i <= 5; i += 1) {
          $scope.itemsList.items2.push({'Id': i, 'Label': 'Item 2_' + i});
        }*/

    };
    $scope.sortableOptions = {
        containment: '#sortable-container',
        //restrict move across columns. move only within column.
        accept: function (sourceItemHandleScope, destSortableScope) {
            return sourceItemHandleScope.itemScope.sortableScope.$id === destSortableScope.$id;
        }
    };

    $scope.hideAll = function (ad) {
        if (ad == $scope.currentIndex) {
            $scope.currentIndex = undefined;
            return;
        }
        $scope.currentIndex = ad;
    };
    /* end drag and sortable */


    // API call For Facility TAB
    $scope.columnSizeChanged = function(colDef,deltaChange){
    	$scope.userWidthChanged = "changed";
    	var columnDetails = angular.toJson(colDef);
    	console.log(deltaChange);
    	console.log(colDef);
    	console.log(colDef.field);
    	var changedWidth;
    	if(colDef.field == "username"){
    		changedWidth = $scope.userTableWidths[0].userName+deltaChange;
    		$scope.userTableWidths[0].userName = changedWidth; 
    		console.log($scope.userTableWidths[0].userName);
    	}else if(colDef.field == "firstName"){
    		changedWidth = $scope.userTableWidths[0].firstName+deltaChange;
    		$scope.userTableWidths[0].firstName = changedWidth; 
    		console.log($scope.userTableWidths[0].firstName);
    	}else if(colDef.field == "lastName"){
    		changedWidth = $scope.userTableWidths[0].lastName+deltaChange;
    		$scope.userTableWidths[0].lastName = changedWidth; 
    		console.log($scope.userTableWidths[0].lastName);
    	}else if(colDef.field == "memberCount"){
    		changedWidth = $scope.userTableWidths[0].numberOfIndividuals+deltaChange;
    		$scope.userTableWidths[0].numberOfIndividuals = changedWidth; 
    		console.log($scope.userTableWidths[0].numberOfIndividuals);
    	}else if(colDef.field == "role"){
    		changedWidth = $scope.userTableWidths[0].role+deltaChange;
    		$scope.userTableWidths[0].role = changedWidth; 
    		console.log($scope.userTableWidths[0].role);
    	}
    	
    };
    $scope.saveChangedWidth = function(){
    	console.log($scope.userTableWidths[0]);
    	if($scope.userTableWidths[0].username == $rootScope.userInfo.username){
    		$http({
				method : 'PUT',
				url : "http://0.0.0.0:3000/api/UserTableWidths",
				data : $scope.userTableWidths[0],
				headers : {
					'Content-Type' : 'application/json'
				}
			}).then(function(response) {
				console.log("Data saved as existing user");
			}, function(err) {
				alert(angular.toJson(err));
			})
    	}else{
    		$scope.newColumnWidth = {
    					  userName: $scope.userTableWidths[0].userName,
    					  firstName: $scope.userTableWidths[0].firstName,
    					  lastName: $scope.userTableWidths[0].lastName,
    					  numberOfIndividuals: $scope.userTableWidths[0].numberOfIndividuals,
    					  role: $scope.userTableWidths[0].role,
    					  username: $rootScope.userInfo.username
    		}
    		console.log($scope.newColumnWidth);
    		$http({
				method : 'POST',
				url : "http://0.0.0.0:3000/api/UserTableWidths",
				data : $scope.newColumnWidth,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).then(function(response) {
				console.log("Data saved as new user");
			}, function(err) {
				alert(angular.toJson(err));
			})
    	}
    }
    
    $scope.sortChanged = function ( grid, sortColumns ) {
    	var limit = 20;;
		var name = sortColumns[0].name;
		$scope.sortOrder = sortColumns[0].sort.direction;
       console.log(name);
       if(name == "username"){
           $scope.sortColumnNumber = 1;
           $scope.sortCoulmnName = "username";
       }else if(name == "firstName"){
      	     $scope.sortColumnNumber = 2;
      	  $scope.sortCoulmnName = "firstName";
       }else if(name == "lastName"){
      	     $scope.sortColumnNumber = 3;
      	  $scope.sortCoulmnName = "lastName";
       }else if(name == "memberCount"){
      	     $scope.sortColumnNumber = 4;
      	  $scope.sortCoulmnName = "memberCount";
       }else if(name == "role"){
      	     $scope.sortColumnNumber = 5;
      	  $scope.sortCoulmnName = "role";
       }
       console.log(sortColumns[0].colDef.index);
       console.log(sortColumns[0].name);
       console.log($scope.sortColumnNumber);
       console.log($scope.sortOrder);
	    if( sortColumns.length == 0 || sortColumns[0].name != $scope.filterUserGrid.columnDefs[$scope.sortColumnNumber-1].name ){
	    	console.log("if");
	    	AdminUser.userList("&filter[order]=username ASC&filter[limit]=" + limit + "&filter[skip]=" + skip, function (response) {
	            //console.log(response);
	    		 if (response.status == 200) {
	                 //$scope.userList = response.data;
	                 $scope.gridOptionsUser.data = response.data;
	                 $scope.filterUserGrid.data = response.data
	                     ////console.log(response.data);

	                 $loading.finish('userTab');

	             } else {
	                 $loading.finish('userTab');

	                 toaster.pop("error", "User " + response.data.error.message);

	             }
	        });
	    } else {
	     var skipRecords = 0;
	     skipRecords = ($scope.sortPageNumber-1) * $scope.sortPageSize;
	     console.log(skipRecords);
	     switch( sortColumns[0].sort.direction ) {
	        case uiGridConstants.ASC:
	        	AdminUser.userList("&filter[order]="+sortColumns[0].name+"ASC&filter[limit]=" + limit + "&filter[skip]=" + skipRecords, function (response) {
	                //console.log(response);
	            	if (response.status == 200) {
	                    //$scope.userList = response.data;
	                    $scope.gridOptionsUser.data = response.data;
	                    $scope.filterUserGrid.data = response.data
	                        ////console.log(response.data);

	                    $loading.finish('userTab');

	                } else {
	                    $loading.finish('userTab');

	                    toaster.pop("error", "User " + response.data.error.message);

	                }
	            });
	          break;
	        case uiGridConstants.DESC:
	        	AdminUser.userList("&filter[order]="+sortColumns[0].name+"DESC&filter[limit]=" + limit + "&filter[skip]=" + skipRecords, function (response) {
	                //console.log(response);
	        		if (response.status == 200) {
	                    //$scope.userList = response.data;
	                    $scope.gridOptionsUser.data = response.data;
	                    $scope.filterUserGrid.data = response.data
	                        ////console.log(response.data);

	                    $loading.finish('userTab');

	                } else {
	                    $loading.finish('userTab');

	                    toaster.pop("error", "User " + response.data.error.message);

	                }
	            });
	          break;
	        case undefined:
	        	AdminUser.userList("&filter[limit]=" + limit + "&filter[skip]=" + skipRecords, function (response) {
	                //console.log(response);
	        		if (response.status == 200) {
	                    //$scope.userList = response.data;
	                    $scope.gridOptionsUser.data = response.data;
	                    $scope.filterUserGrid.data = response.data
	                        ////console.log(response.data);

	                    $loading.finish('userTab');

	                } else {
	                    $loading.finish('userTab');

	                    toaster.pop("error", "User " + response.data.error.message);

	                }

	            });
	      }
	    }
	  };

    

    
    $scope.FacilitiyTabAPI = function (limit, skip) {
        $loading.start('facilityTab');
        FacilityService.FacilityList("&filter[limit]=" + limit + "&filter[skip]=" + skip, function (response) {
            if (response.status == 200) {
                //console.log(response.data);

                $scope.gridOptionsFacility.data = response.data;
                $scope.filterFacilityGrid.data = response.data;
                $loading.finish('facilityTab');
            } else {
                $loading.finish('facilityTab');

                toaster.pop("error", response.data.error.message);
            }

        });

    };

    function max_date(all_dates) {
        var max_dt = all_dates[0],
            max_dtObj = new Date(all_dates[0]);
        all_dates.forEach(function (dt, index) {
            if (new Date(dt) > max_dtObj) {
                max_dt = dt;
                max_dtObj = new Date(dt);
            }
        });
        return max_dt;
    }

    /* $scope.GetAdditionInfoFun=function(accountID){
        var infodata;
        //return accountID;
          DataService.getData("&filter[where][facilityAccountId]="+accountID+"&filter[order]=reportPoid DESC&filter[limit]=1", function(result) {
           //console.log(result[0]);
           if(result.length!=0){
             infodata=result[0];
           }
           return infodata; 
          

          }); 
 
    };
*/

    /*CBA Rule API Call start */
    $scope.CBATabAPI = function (limit, skip) {

        $loading.start('cbaTab');
        $scope.CbaRowdata = [];

        FacilityService.FacilityListWithCba("&filter[include]=cBARules&filter[limit]=" + limit + "&filter[skip]=" + skip, function (response) {
            //console.log(response);
            if (response.status == 200) {

                var greaterLength = 0;
                var tempLength;
                angular.forEach(response.data, function (val, key) {
                    ////console.log(val);

                    $scope.payPeriodArr = [];
                    $scope.CbaRowObj = {};
                    $scope.CbaRowObj['id'] = val.id;
                    $scope.CbaRowObj['facilityAccountId'] = val.facilityAccountId;
                    $scope.CbaRowObj['name'] = val.name;
                    $scope.CbaRowObj['groupName'] = val.groupName;
                    $scope.CbaRowObj['consituencyId'] = val.consituencyId;
                    $scope.CbaRowObj['lastDemoDate'] = val.lastDemoDate;
                    $scope.CbaRowObj['lastDemoPayPeriod'] = val.lastDemoPayPeriod;
                    $scope.CbaRowObj['lastDuesDate'] = val.lastDuesDate;
                    $scope.CbaRowObj['lastDuesPayPeriod'] = val.lastDuesPayPeriod;

                    $scope.CbaRowObj['lastComboDate'] = val.lastComboDate;
                    $scope.CbaRowObj['lastComboPayPeriod'] = val.lastComboPayPeriod;
                    $scope.CbaRowObj['lastEventsDate'] = val.lastEventsDate;
                    $scope.CbaRowObj['lastEventsPayPeriod'] = val.lastEventsPayPeriod;
                    $scope.CbaRowObj['lastNewHireDate'] = val.lastNewHireDate;
                    $scope.CbaRowObj['lastNewHirePayPeriod'] = val.lastNewHirePayPeriod;
                    $scope.CbaRowObj['lastPDApplicationDate'] = val.lastPDApplicationDate;
                    $scope.CbaRowObj['lastPDApplicationPayPeriod'] = val.lastPDApplicationPayPeriod;
                    $scope.CbaRowObj['lastTerminationDate'] = val.lastTerminationDate;
                    $scope.CbaRowObj['lastTerminationPayPeriod'] = val.lastTerminationPayPeriod;
                    $scope.payPeriodArr.push(val.lastDemoDate);
                    $scope.payPeriodArr.push(val.lastDuesDate);
                    $scope.payPeriodArr.push(val.lastComboDate);
                    $scope.payPeriodArr.push(val.lastEventsDate);
                    $scope.payPeriodArr.push(val.lastNewHireDate);
                    $scope.payPeriodArr.push(val.lastPDApplicationDate);
                    $scope.payPeriodArr.push(val.lastTerminationDate);

                    $scope.CbaRowObj['lastReceivedPayperiod'] = max_date($scope.payPeriodArr);
                    $scope.cbaTempArr = [];
                    angular.forEach(val.cBARules, function (rule, key) {


                        $scope.cbaTempArr.push({
                            "rule": rule.name,
                            "fromDate": rule.validFrom,
                            "toDate": rule.validTo
                        });
                    });
                    $scope.CbaRowObj['billingRule'] = $scope.cbaTempArr;


                    if ($scope.cbaTempArr.length > 0) {
                        tempLength = $scope.cbaTempArr.length;

                    }

                    if (greaterLength < tempLength) {
                        greaterLength = tempLength;

                    }

                    $scope.CbaRowdata.push($scope.CbaRowObj);


                });

                if (greaterLength != 0) {
                    $scope.filterCbaGrid['expandableRowHeight'] = greaterLength * 80;
                } else {
                    $scope.filterCbaGrid['expandableRowHeight'] = 400;
                }


                $scope.filterCbaGrid.data = $scope.CbaRowdata;
                $scope.gridOptionsCba.data = $scope.CbaRowdata;

                $loading.finish('cbaTab');
            }
        });
    };
    /*CBA Rule API Call end here */
    //console.log("caalling admin");



    /* Initializayion of date picker start here */
    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1
    };

    $scope.formats = ['MM/dd/yyyy', 'dd.MM.yyyy', 'dd-MMMM-yyyy', 'yyyy-MM-dd', 'shortDate'];
    $scope.format = $scope.formats[3];
    //$scope.altInputFormats = ['M!/d!/yyyy'];

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [{
        date: tomorrow,
        status: 'full'
    }, {
        date: afterTomorrow,
        status: 'partially'
    }];

    $scope.getDayClass = function (date, mode) {
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }
        return '';
    };

    /* date picker ends here */

    $scope.validFromCalender = function () {
        $scope.validFromCalenderpopup.opened = true;
    };


    $scope.validFromCalenderpopup = {
        opened: false
    };

    $scope.validToCalender = function () {
        $scope.validToCalenderpopup.opened = true;
    };


    $scope.validToCalenderpopup = {
        opened: false
    };


    $scope.cbaRuleFromCalender = function () {
        $scope.cbaRuleFromCalenderpopup.opened = true;
    };

    $scope.cbaRuleFromCalenderpopup = {
        opened: false
    };

    $scope.cbaRuleToCalender = function () {
        $scope.cbaRuleToCalenderpopup.opened = true;
    };

    $scope.cbaRuleToCalenderpopup = {
        opened: false
    };



    $scope.filterUserGrid.onRegisterApi = function (gridApi) {
        //set gridApi on scope
        $scope.gridApi = gridApi;	 
        $scope.gridApi.core.on.sortChanged( $scope, $scope.sortChanged );
        $scope.gridApi.colResizable.on.columnSizeChanged($scope,$scope.columnSizeChanged);
        var val = gridApi.selection.on.rowSelectionChanged($scope, function (row) {

            $scope.selectedUserObj = $scope.gridApi.selection.getSelectedRows();

            if (row.isSelected) {
                $rootScope.UserActionValue = "Edit User";
                $rootScope.CreateUserObj = row.entity;
                if (row.entity.id == localStorage.id) {
                    $rootScope.enableUserDelete = false;
                } else {
                    $rootScope.enableUserDelete = true;
                }

                //console.log(row.entity);

            } else {

                $rootScope.UserActionValue = "Create User";
                $rootScope.CreateUserObj = {};
                $rootScope.enableUserDelete = false;

            }
        });

    };
    $scope.listUsers = function () {
        AdminUser.userList("", function (response) {
            if (response.status == 200) {
            	$scope.listOfUsers = response.data;
            	console.log($scope.listOfUsers);
            } else {
            	console.log("Eror"+response);
            }
        });

    };
    $scope.listUsers();

    $scope.refreshDataUser = function (termObj) {


        $scope.filterUserGrid.data = $scope.listOfUsers;

        if(termObj){
        while (termObj) {
            var oSearchArray = termObj.split(' ');
            $scope.filterUserGrid.data = $filter('filter')($scope.filterUserGrid.data, oSearchArray[0], undefined);
            oSearchArray.shift()
            termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
        }
        $scope.UserPagination.toIndex = $scope.filterUserGrid.data.length;
        }else{
        	 $scope.userTabAPI(20, 0);
        }
    };

    $scope.OpenCBAModal = function () {

        $scope.modalInstance = $uibModal.open({

            templateUrl: 'CBARuleListModalContent.html',
            controller: 'CBARuleListModalController',
            size: 'lg',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                selectedFacilityIdToCba: function () {
                    return $scope.CBAfacilityObjId;
                }
            }
        });
    };

    $scope.selectTabItem = function (setTab) {
        $scope.isSelected = setTab;
        if (setTab == 2) { //TAB two ie Facility
        	
            /* pagination for facility */
            $scope.limit = 20;
            $scope.FacilitypageNumber = 0;
            $scope.Facilitypresentpage = 1;

            $scope.FacilityPagination = {};
            

            $scope.gridOptionsFacility = {};
            $scope.filterFacilityGrid = {

            };
            $loading.start('facilityTab');
            
            $scope.facilityStaffTableWidths = [];

            var facilityStaffInit = function(){
            	$http({
        			method : 'GET',
        			url : "http://0.0.0.0:3000/api/FacilityStaffTables?filter[where][username]="+$rootScope.userInfo.username,
        			headers : {
        				'Content-Type' : 'application/json'
        			}
        		}).then(function(response) {
        			$scope.facilityStaffTableWidths = response.data;
        			if($scope.facilityStaffTableWidths.length<=0){
        				$http({
        					method : 'GET',
        					url : "http://0.0.0.0:3000/api/FacilityStaffTables?filter[where][username]=default",
        					headers : {
        						'Content-Type' : 'application/json'
        					}
        				}).then(function(response) {
        					$scope.facilityStaffTableWidths = response.data;
        					console.log($scope.facilityStaffTableWidths[0]);
        					$scope.facilityStaffWidthAssign();
        				}, function(err) {
        					alert(angular.toJson(err));
        				})
        			}else{
        				$scope.facilityStaffWidthAssign();
        			}
        			console.log($scope.facilityStaffTableWidths[0]);
        		}, function(err) {
        			alert(angular.toJson(err));
        			
        		})
            }
            facilityStaffInit();
            
            $scope.facilityStaffWidthAssign = function(){
                $scope.filterFacilityGrid = {
                		 useExternalSorting: true,
                        columnDefs: [{
                            field: 'name',
                            displayName: 'FACILITY NAME',
                            enableHiding: false,
                            width: $scope.facilityStaffTableWidths[0].facilityName,
                            cellTooltip: true,
                            headerTooltip: true
                        }, {
                            field: 'groupName',
                            displayName: 'GROUP NAME',
                            enableHiding: false,
                            width: $scope.facilityStaffTableWidths[0].groupName,
                            cellTooltip: true,
                            headerTooltip: true
                        }, {
                            field: 'consituencyId',
                            displayName: 'CONSTITUENCY ID',
                            enableHiding: false,
                            width: $scope.facilityStaffTableWidths[0].constituencyId,
                            cellTooltip: true,
                            headerTooltip: true
                        }, {
                            field: 'memberCount',
                            displayName: 'NUMBER OF INDIVIDUALS ',
                            enableHiding: false,
                            width: $scope.facilityStaffTableWidths[0].memberCount,
                            cellTooltip: true,
                            headerTooltip: true
                        }, {
                            field: 'adminUserName',
                            displayName: 'ASSIGNED USER',
                            enableHiding: false,
                            width: $scope.facilityStaffTableWidths[0].assignedUser,
                            cellTooltip: true,
                            headerTooltip: true
                        }, ],

                        enableSelectAll: true,
                        multiSelect: true,

                        onRegisterApi: function (gridApi) { //register grid data first within the gridOptions
                            $scope.gridApi = gridApi;


                        }

                    };

            }
            


            

            FacilityService.FacilityListCount("Facilities/count", function (response) {
                if (response.status == 200) {
                    ////console.log(response);
                    $scope.Facilitytotalcount = response.data.count;
                    if ($scope.Facilitytotalcount < $scope.limit) {
                        $scope.FacilityPagination.toIndex = $scope.Facilitytotalcount;
                    } else {
                        $scope.FacilityPagination.toIndex = $scope.limit;
                    }

                    $scope.Facilitytotalpages = Math.ceil($scope.Facilitytotalcount / $scope.limit);

                }

            });

            $scope.FacilityPagination = {
                //limit:10,
                limit: $scope.limit,
                skip: 0,
                //pageNumber: 0,
                fromIndex: 1,
                toIndex: $scope.limit,
                pageNumber: 0,
                loadSelCountPerPage: function (pageLimitValue) {
                    this.pageNumber = 0;
                    this.limit = pageLimitValue;
                    this.skip = 0;
                    $scope.Facilitypresentpage = 1;
                    $scope.Facilitytotalpages = Math.ceil($scope.Facilitytotalcount / pageLimitValue);
                    $scope.FacilityPagination.fromIndex = this.skip + 1;
                    //$scope.FacilityPagination.toIndex = this.skip + Number(this.limit);

                    if ($scope.Facilitytotalcount < pageLimitValue) {
                        $scope.FacilityPagination.toIndex = $scope.Facilitytotalcount;
                    } else {
                        $scope.FacilityPagination.toIndex = this.skip + Number(this.limit);
                    }

                    /*Start API*/
                    $scope.FacilitiyTabAPI(this.limit, this.skip);
                    /*End of API*/
                },
                nextPage: function () {
                    if ($scope.Facilitypresentpage != $scope.Facilitytotalpages) {
                        this.pageNumber++;
                        $scope.Facilitypresentpage++;
                        this.skip = this.limit * this.pageNumber;
                        $scope.FacilityPagination.fromIndex = this.skip + 1;
                        $nextMaxtoindex = Number(this.skip) + Number(this.limit);
                        if ($scope.Facilitytotalcount < $nextMaxtoindex) {
                            $scope.FacilityPagination.toIndex = $scope.Facilitytotalcount;
                        } else {
                            $scope.FacilityPagination.toIndex = $nextMaxtoindex;
                        }
                        /*Start API*/
                        $scope.FacilitiyTabAPI(this.limit, this.skip);
                        /*End of API*/
                    }

                },

                previousPage: function () {
                    if (this.pageNumber > 0) {
                        this.pageNumber--;
                        $scope.Facilitypresentpage--;
                        this.skip = this.limit * this.pageNumber;

                        $NexttoIndex = $scope.FacilityPagination.toIndex - this.limit;

                        if ($scope.FacilityPagination.toIndex == $scope.Facilitytotalcount) {
                            $scope.FacilityPagination.toIndex = $scope.FacilityPagination.fromIndex - 1;
                        } else {
                            $scope.FacilityPagination.toIndex = $NexttoIndex;
                        }
                        $scope.FacilityPagination.fromIndex = $scope.FacilityPagination.fromIndex - this.limit;
                        /*Start API*/
                        $scope.FacilitiyTabAPI(this.limit, this.skip);
                        /*End of API*/
                    }
                }
            };
            /* pagination for facility */

            
            $scope.columnSizeChanged = function(colDef,deltaChange){
            	$scope.facilityStaffWidthChanged = "changed";
            	var columnDetails = angular.toJson(colDef);
            	console.log(deltaChange);
            	console.log(colDef);
            	console.log(colDef.field);
            	var changedWidth;
            	if(colDef.field == "name"){
            		changedWidth = $scope.facilityStaffTableWidths[0].facilityName+deltaChange;
            		$scope.facilityStaffTableWidths[0].facilityName = changedWidth; 
            		console.log($scope.facilityStaffTableWidths[0].facilityName);
            	}else if(colDef.field == "groupName"){
            		changedWidth = $scope.facilityStaffTableWidths[0].groupName+deltaChange;
            		$scope.facilityStaffTableWidths[0].groupName = changedWidth; 
            		console.log($scope.facilityStaffTableWidths[0].groupName);
            	}else if(colDef.field == "constituencyId"){
            		changedWidth = $scope.facilityStaffTableWidths[0].constituencyId+deltaChange;
            		$scope.facilityStaffTableWidths[0].constituencyId = changedWidth; 
            		console.log($scope.facilityStaffTableWidths[0].constituencyId);
            	}else if(colDef.field == "memberCount"){
            		changedWidth = $scope.facilityStaffTableWidths[0].memberCount+deltaChange;
            		$scope.facilityStaffTableWidths[0].memberCount = changedWidth; 
            		console.log($scope.facilityStaffTableWidths[0].memberCount);
            	}else if(colDef.field == "adminUserName"){
            		changedWidth = $scope.facilityStaffTableWidths[0].assignedUser+deltaChange;
            		$scope.facilityStaffTableWidths[0].assignedUser = changedWidth; 
            		console.log($scope.facilityStaffTableWidths[0].assignedUser);
            	}
            	
            };
            $scope.saveChangedFacilityWidth = function(){
            	console.log($scope.facilityStaffTableWidths[0]);
            	if($scope.facilityStaffTableWidths[0].username == $rootScope.userInfo.username){
            		$http({
        				method : 'PUT',
        				url : "http://0.0.0.0:3000/api/FacilityStaffTables",
        				data : $scope.facilityStaffTableWidths[0],
        				headers : {
        					'Content-Type' : 'application/json'
        				}
        			}).then(function(response) {
        				console.log("Data saved as existing user");
        			}, function(err) {
        				alert(angular.toJson(err));
        			})
            	}else{
            		$scope.newColumnWidth = {
            					  facilityName: $scope.facilityStaffTableWidths[0].facilityName,
            					  groupName: $scope.facilityStaffTableWidths[0].groupName,
            					  constituencyId: $scope.facilityStaffTableWidths[0].constituencyId,
            					  memberCount: $scope.facilityStaffTableWidths[0].memberCount,
            					  assignedUser: $scope.facilityStaffTableWidths[0].assignedUser,
            					  username: $rootScope.userInfo.username
            		}
            		console.log($scope.newColumnWidth);
            		$http({
        				method : 'POST',
        				url : "http://0.0.0.0:3000/api/FacilityStaffTables",
        				data : $scope.newColumnWidth,
        				headers : {
        					'Content-Type' : 'application/json'
        				}
        			}).then(function(response) {
        				console.log("Data saved as new user");
        			}, function(err) {
        				alert(angular.toJson(err));
        			})
            	}
            }
            




            //FacilityList API call
            $scope.FacilitiyTabAPI(20, 0);


            //Facility API call end here

            //row selection for assign facility to users.

            $rootScope.enableAssignBtn = false;
            $scope.assignUserObj = {};
            $scope.filterFacilityGrid.onRegisterApi = function (gridApi) {
                //set gridApi on scope
                $scope.gridApi = gridApi;
                $scope.gridApi.colResizable.on.columnSizeChanged($scope,$scope.columnSizeChanged);
                var val = gridApi.selection.on.rowSelectionChanged($scope, function (row) {
                    $scope.selectedFacilityObj = $scope.gridApi.selection.getSelectedRows();

                    if ($scope.selectedFacilityObj.length != 0) {
                        //console.log($scope.selectedFacilityObj);
                        $rootScope.enableAssignBtn = true;


                    } else {
                        $rootScope.enableAssignBtn = false;

                    }

                });

                gridApi.selection.on.rowSelectionChangedBatch($scope, function () {
                    if ($scope.gridApi.selection.getSelectedRows().length != 0) {
                        $scope.selectedFacilityObj = $scope.gridApi.selection.getSelectedRows();
                        $rootScope.enableAssignBtn = true;

                    } else {
                        $rootScope.enableAssignBtn = false;
                    }


                });

            };
            //row selection for assign facility to users end here.

            $scope.refreshDataFacility = function (termObj) {

                $scope.filterFacilityGrid.data = $scope.gridOptionsFacility.data;

                while (termObj) {
                    var oSearchArray = termObj.split(' ');
                    $scope.filterFacilityGrid.data = $filter('filter')($scope.filterFacilityGrid.data, oSearchArray[0], undefined);
                    oSearchArray.shift();
                    termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';

                }
                $scope.FacilityPagination.toIndex = $scope.filterFacilityGrid.data.length;
            };

        } else if (setTab == 3) { //TAB three ir CBA 

            /* pagination for facility */
            $scope.limit = 20;
            $scope.pageNumber = 0;
            $scope.CBApresentpage = 1;

            $scope.CBAPagination = {};
           
            console.log($scope.cbaTableWidths);
            FacilityService.FacilityListCount("Facilities/count", function (response) {
                if (response.status == 200) {
                    ////console.log(response);
                    $scope.CBAtotalcount = response.data.count;

                    if ($scope.CBAtotalcount < $scope.limit) {
                        $scope.CBAPagination.toIndex = $scope.CBAtotalcount;
                    } else {
                        $scope.CBAPagination.toIndex = $scope.limit;
                    }
                    $scope.CBAtotalpages = Math.ceil($scope.CBAtotalcount / $scope.limit);

                }

            });

            $scope.CBAPagination = {
                //limit:10,
                limit: $scope.limit,
                skip: 0,
                //pageNumber: 0,
                fromIndex: 1,
                toIndex: $scope.limit,
                pageNumber: 0,
                loadSelCountPerPage: function (pageLimitValue) {
                    this.pageNumber = 0;
                    this.limit = pageLimitValue;
                    this.skip = 0;
                    $scope.CBApresentpage = 1;
                    $scope.CBAtotalpages = Math.ceil($scope.CBAtotalcount / pageLimitValue);
                    $scope.CBAPagination.fromIndex = this.skip + 1;
                    if ($scope.CBAtotalcount < pageLimitValue) {
                        $scope.CBAPagination.toIndex = $scope.CBAtotalcount;
                    } else {
                        $scope.CBAPagination.toIndex = this.skip + Number(this.limit);
                    }

                    /*Start API*/
                    $scope.CBATabAPI(this.limit, this.skip);

                    /*End of API*/
                },
                nextPage: function () {
                    if ($scope.CBApresentpage != $scope.CBAtotalpages) {
                        this.pageNumber++;
                        $scope.CBApresentpage++;
                        this.skip = this.limit * this.pageNumber;
                        $scope.CBAPagination.fromIndex = this.skip + 1;
                        //$scope.CBAPagination.toIndex = Number(this.skip) + Number(this.limit);

                        $nextMaxtoindex = Number(this.skip) + Number(this.limit);

                        if ($scope.CBAtotalcount < $nextMaxtoindex) {
                            $scope.CBAPagination.toIndex = $scope.CBAtotalcount;
                        } else {
                            $scope.CBAPagination.toIndex = $nextMaxtoindex;
                        }

                        /*Start API*/
                        $scope.CBATabAPI(this.limit, this.skip);

                        /*End of API*/

                    }

                },

                previousPage: function () {
                    if (this.pageNumber > 0) {
                        this.pageNumber--;
                        $scope.CBApresentpage--;
                        this.skip = this.limit * this.pageNumber;

                        $NexttoIndex = $scope.CBAPagination.toIndex - this.limit;

                        if ($scope.CBAPagination.toIndex == $scope.CBAtotalcount) {
                            $scope.CBAPagination.toIndex = $scope.CBAPagination.fromIndex - 1;
                        } else {
                            $scope.CBAPagination.toIndex = $NexttoIndex;
                        }
                        $scope.CBAPagination.fromIndex = $scope.CBAPagination.fromIndex - this.limit;

                        /*Start API*/
                        $scope.CBATabAPI(this.limit, this.skip);

                        /*End of API*/
                    }
                }
            };
            /* pagination for facility */

    
            $scope.CBATabAPI(20, 0);

            ////console.log($scope.filterGridRowHight);

            $scope.gridOptionsCba = {};
            $scope.filterCbaGrid = {
            };

            	 $scope.filterCbaGrid = {
                         columnDefs: [{
                                 field: 'consituencyId',
                                 displayName: 'CONSTITUENCY ID',
                                 enableHiding: false,
                                 width: $scope.cbaTableWidths[0].constituencyId,
                                 headerTooltip: true
                             }, {
                                 field: 'name',
                                 displayName: 'FACILITY NAME',
                                 enableHiding: false,
                                 width: $scope.cbaTableWidths[0].facilityName,
                                 cellTooltip: true,
                                 headerTooltip: true
                             }, {
                                 field: 'groupName',
                                 displayName: 'GROUP NAME',
                                 enableHiding: false,
                                 width: $scope.cbaTableWidths[0].groupName,
                                 cellTooltip: true,
                                 headerTooltip: true
                             }, {
                                 field: 'lastReceivedPayperiod',
                                 displayName: 'LAST RECEIVED PAY PERIOD DATE',
                                 cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                 enableHiding: false,
                                 width: $scope.cbaTableWidths[0].receivedPayPeriod,
                                 headerTooltip: true
                             }, {
                                 field: 'lastImportedPayperiod',
                                 displayName: 'LAST IMPORTED PAY PERIOD DATE',
                                 enableHiding: false,
                                 width: $scope.cbaTableWidths[0].importedPayPeriod,
                                 cellTooltip: true,
                                 headerTooltip: true
                             }, {
                                 field: 'lastYetImportedPayPeriod',
                                 displayName: 'NOT YET IMPORTED PAY PERIOD DATE',
                                 enableHiding: false,
                                 width: $scope.cbaTableWidths[0].yetImportedPayPeriod,
                                 cellTooltip: true,
                                 headerTooltip: true
                             },
                             /*{
                                                     field: 'lastDemoDate',
                                                     displayName: 'LAST DEMO DATE',
                                                     cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                                     enableHiding: false,
                                                     width: 200,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastDemoPayPeriod',
                                                     displayName: 'LAST DEMO PAY PERIOD',
                                                     enableHiding: false,
                                                     width: 200,
                                                     cellTooltip: true,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastDuesDate',
                                                     displayName: 'LAST DUE DATE',
                                                     cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                                     enableHiding: false,
                                                     width: 200,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastDuesPayPeriod',
                                                     displayName: 'LAST DUE PAY PERIOD',
                                                     enableHiding: false,
                                                     width: 200,
                                                     cellTooltip: true,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastDemoDate',
                                                     displayName: 'LAST DEMO DATE',
                                                     cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                                     enableHiding: false,
                                                     width: 200,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastDemoPayPeriod',
                                                     displayName: 'LAST DEMO PAY PERIOD',
                                                     enableHiding: false,
                                                     width: 200,
                                                     cellTooltip: true,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastComboDate',
                                                     displayName: 'LAST Combo DATE',
                                                     cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                                     enableHiding: false,
                                                     width: 200,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastComboPayPeriod',
                                                     displayName: 'LAST Combo PAY PERIOD',
                                                     enableHiding: false,
                                                     width: 200,
                                                     cellTooltip: true,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastEventsDate',
                                                     displayName: 'LAST Events DATE',
                                                     cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                                     enableHiding: false,
                                                     width: 200,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastEventsPayPeriod',
                                                     displayName: 'LAST Events PAY PERIOD',
                                                     enableHiding: false,
                                                     width: 200,
                                                     cellTooltip: true,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastNewHireDate',
                                                     displayName: 'LAST New Hire DATE',
                                                     cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                                     enableHiding: false,
                                                     width: 200,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastNewHirePayPeriod',
                                                     displayName: 'LAST New Hire PAY PERIOD',
                                                     enableHiding: false,
                                                     width: 200,
                                                     cellTooltip: true,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastPDApplicationDate',
                                                     displayName: 'LAST PD Application DATE',
                                                     cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                                     enableHiding: false,
                                                     width: 200,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastPDApplicationPayPeriod',
                                                     displayName: 'LAST PD Application PAY PERIOD',
                                                     enableHiding: false,
                                                     width: 200,
                                                     cellTooltip: true,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastTerminationDate',
                                                     displayName: 'LAST Termination DATE',
                                                     cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                                     enableHiding: false,
                                                     width: 200,
                                                     headerTooltip: true
                                                 }, {
                                                     field: 'lastTerminationPayPeriod',
                                                     displayName: 'LAST Termination PAY PERIOD',
                                                     enableHiding: false,
                                                     width: 200,
                                                     cellTooltip: true,
                                                     headerTooltip: true
                                                 }*/
                             /*, {
                                                     field: 'billingRule',
                                                     displayName: 'BILLING RULE',
                                                     enableHiding: false,
                                                     cellTooltip: true,
                                                     headerTooltip: true,
                                                     width: 400,

                                                     cellTemplate: '<div ng-repeat="rules in row.entity.billingRule "><show-more  text="{{rules.rule}}" todate={{rules.toDate}} limit="50"> </show-more></div>'
                                                 },*/


                         ],
                         enableSelectAll: false,
                         multiSelect: false,
                         //enableGridMenu: true,
                         /* rowHeight:$scope.filterGridRowHight,*/
                         //rowHeight:90,


                         onRegisterApi: function (gridApi) { //register grid data first within the gridOptions
                             $scope.gridApi = gridApi;

                         },
                         expandableRowTemplate: 'views/import/expandableRowTemplate.html',

                         //enableExpandableRowHeader:false

                         //subGridVariable will be available in subGrid scope
                     };
            


            $rootScope.enableRuleUpdateBtn = false;
            $rootScope.enableRuleCreateBtn = false;
            
            $scope.columnSizeChanged = function(colDef,deltaChange){
            	$scope.cbaWidthChanged = "changed";
            	var columnDetails = angular.toJson(colDef);
            	console.log(deltaChange);
            	console.log(colDef);
            	console.log(colDef.field);
            	var changedWidth;
            	if(colDef.field == "consituencyId"){
            		changedWidth = $scope.cbaTableWidths[0].constituencyId+deltaChange;
            		$scope.cbaTableWidths[0].constituencyId = changedWidth; 
            		console.log($scope.cbaTableWidths[0].constituencyId);
            	}else if(colDef.field == "name"){
            		changedWidth = $scope.cbaTableWidths[0].facilityName+deltaChange;
            		$scope.cbaTableWidths[0].facilityName = changedWidth; 
            		console.log($scope.cbaTableWidths[0].facilityName);
            	}else if(colDef.field == "groupName"){
            		changedWidth = $scope.cbaTableWidths[0].groupName+deltaChange;
            		$scope.cbaTableWidths[0].groupName = changedWidth; 
            		console.log($scope.cbaTableWidths[0].groupName);
            	}else if(colDef.field == "lastReceivedPayperiod"){
            		changedWidth = $scope.cbaTableWidths[0].receivedPayPeriod+deltaChange;
            		$scope.cbaTableWidths[0].receivedPayPeriod = changedWidth; 
            		console.log($scope.cbaTableWidths[0].receivedPayPeriod);
            	}else if(colDef.field == "lastImportedPayperiod"){
            		changedWidth = $scope.cbaTableWidths[0].importedPayPeriod+deltaChange;
            		$scope.cbaTableWidths[0].importedPayPeriod = changedWidth; 
            		console.log($scope.cbaTableWidths[0].importedPayPeriod);
            	}
            	else if(colDef.field == "lastYetImportedPayPeriod"){
            		changedWidth = $scope.cbaTableWidths[0].yetImportedPayPeriod+deltaChange;
            		$scope.cbaTableWidths[0].yetImportedPayPeriod = changedWidth; 
            		console.log($scope.cbaTableWidths[0].yetImportedPayPeriod);
            	}
            	
            	
            };
            $scope.saveChangedCbaWidth = function(){
            	console.log($scope.cbaTableWidths[0]);
            	if($scope.cbaTableWidths[0].username == $rootScope.userInfo.username){
            		$http({
        				method : 'PUT',
        				url : "http://0.0.0.0:3000/api/CBATables",
        				data : $scope.cbaTableWidths[0],
        				headers : {
        					'Content-Type' : 'application/json'
        				}
        			}).then(function(response) {
        				console.log("Data saved as existing user");
        			}, function(err) {
        				alert(angular.toJson(err));
        			})
            	}else{
            		$scope.newColumnWidth = {
            				constituencyId: $scope.cbaTableWidths[0].constituencyId,
            				facilityName: $scope.cbaTableWidths[0].facilityName,
            				groupName: $scope.cbaTableWidths[0].groupName,
            				receivedPayPeriod: $scope.cbaTableWidths[0].receivedPayPeriod,
            				importedPayPeriod: $scope.cbaTableWidths[0].importedPayPeriod,
            				yetImportedPayPeriod: $scope.cbaTableWidths[0].yetImportedPayPeriod,
            				username: $rootScope.userInfo.username
            		}
            		console.log($scope.newColumnWidth);
            		$http({
        				method : 'POST',
        				url : "http://0.0.0.0:3000/api/CBATables",
        				data : $scope.newColumnWidth,
        				headers : {
        					'Content-Type' : 'application/json'
        				}
        			}).then(function(response) {
        				console.log("Data saved as new user");
        			}, function(err) {
        				alert(angular.toJson(err));
        			})
            	}
            }

            $scope.filterCbaGrid.onRegisterApi = function (gridApi) {
                //set gridApi on scope
                $scope.gridApi = gridApi;
                $scope.gridApi.colResizable.on.columnSizeChanged($scope,$scope.columnSizeChanged);
                var val = gridApi.selection.on.rowSelectionChanged($scope, function (row) {

                    $scope.selectedCBAObj = $scope.gridApi.selection.getSelectedRows();

                    if (row.isSelected) {
                        //console.log(row);
                        $scope.CBAfacilityAccountID = row.entity.facilityAccountId;
                        $scope.CBAfacilitySelectedName = row.entity.name;
                        $scope.CBAfacilityObjId = row.entity.id;
                        $rootScope.CBASelectedFacilityAccountId = row.entity.facilityAccountId;


                        if (row.entity.billingRule) {
                            //$scope.AllRuleByFacilityAPI($scope.CBAfacilityAccountID);
                            $rootScope.enableRuleUpdateBtn = true;
                            $rootScope.enableRuleCreateBtn = false;

                        } else {

                            $rootScope.enableRuleCreateBtn = true;
                            $rootScope.enableRuleUpdateBtn = false;
                        }

                    } else {

                        $rootScope.enableRuleUpdateBtn = false;
                        $rootScope.enableRuleCreateBtn = false;

                    }

                });

                gridApi.expandable.on.rowExpandedStateChanged($scope, function (row) {

                    if (row.isExpanded) {

                        FacilityService.GetMoreInfo("&filter[where][facilityAccountId]=" + row.entity.facilityAccountId + "&filter[where][reportType]=Dues&filter[order]=reportPoid DESC&filter[limit]=1", function (response) {

                            if (response.status == 200) {
                                if (response.data.length != 0) {
                                    row.entity['DeuesBillPay'] = response.data[0].payPeriod;
                                    row.entity['DeuesAdditionalInfo'] = response.data[0];
                                    //console.log(response);

                                }
                            }
                        });
                        FacilityService.GetMoreInfo("&filter[where][facilityAccountId]=" + row.entity.facilityAccountId + "&filter[where][reportType]=Combo&filter[order]=reportPoid DESC&filter[limit]=1", function (response) {

                            if (response.status == 200) {
                                if (response.data.length != 0) {
                                    row.entity['ComboBillPay'] = response.data[0].payPeriod;
                                    row.entity['ComboAdditionalInfo'] = response.data[0];
                                    //console.log(response);

                                }
                            }
                        });
                        FacilityService.GetMoreInfo("&filter[where][facilityAccountId]=" + row.entity.facilityAccountId + "&filter[where][reportType]=Demographic&filter[order]=reportPoid DESC&filter[limit]=1", function (response) {

                            if (response.status == 200) {
                                if (response.data.length != 0) {
                                    row.entity['DemographicBillPay'] = response.data[0].payPeriod;
                                    row.entity['DemographicAdditionalInfo'] = response.data[0];
                                    //console.log(response);

                                }
                            }
                        });
                        FacilityService.GetMoreInfo("&filter[where][facilityAccountId]=" + row.entity.facilityAccountId + "&filter[where][reportType]=Dues&filter[order]=reportPoid DESC&filter[limit]=1", function (response) {

                            if (response.status == 200) {
                                if (response.data.length != 0) {
                                    row.entity['DuesBillPay'] = response.data[0].payPeriod;
                                    row.entity['DuesAdditionalInfo'] = response.data[0];

                                    //console.log(response);

                                }
                            }
                        });
                        FacilityService.GetMoreInfo("&filter[where][facilityAccountId]=" + row.entity.facilityAccountId + "&filter[where][reportType]=Events&filter[order]=reportPoid DESC&filter[limit]=1", function (response) {

                            if (response.status == 200) {
                                if (response.data.length != 0) {
                                    row.entity['EventsBillPay'] = response.data[0].payPeriod;
                                    row.entity['EventsAdditionalInfo'] = response.data[0];
                                    //console.log(response);

                                }
                            }
                        });
                        FacilityService.GetMoreInfo("&filter[where][facilityAccountId]=" + row.entity.facilityAccountId + "&filter[where][reportType]=New Hire&filter[order]=reportPoid DESC&filter[limit]=1", function (response) {

                            if (response.status == 200) {
                                if (response.data.length != 0) {
                                    row.entity['NewHireBillPay'] = response.data[0].payPeriod;
                                    row.entity['NewHireAdditionalInfo'] = response.data[0];
                                    //console.log(response);

                                }
                            }
                        });
                        FacilityService.GetMoreInfo("&filter[where][facilityAccountId]=" + row.entity.facilityAccountId + "&filter[where][reportType]=Termination&filter[order]=reportPoid DESC&filter[limit]=1", function (response) {

                            if (response.status == 200) {
                                if (response.data.length != 0) {
                                    row.entity['TerminationBillPay'] = response.data[0].payPeriod;
                                    row.entity['TerminationAdditionalInfo'] = response.data[0];
                                    //console.log(response);

                                }
                            }
                        });

                        /*FacilityService.GetMoreInfo("&filter[where][facilityAccountId]="+accountID+"&filter[order]=reportPoid DESC&filter[limit]=1",function(response){
                        //console.log();
                       })
*/
                    }
                });

            };

            $scope.filterCbaGrid.data = $scope.gridOptionsCba.data;
            /*Global Search calling function from template*/
            $scope.refreshDataCba = function (termObj) {

                $scope.filterCbaGrid.data = $scope.gridOptionsCba.data;

                while (termObj) {
                    var oSearchArray = termObj.split(' ');
                    $scope.filterCbaGrid.data = $filter('filter')($scope.filterCbaGrid.data, oSearchArray[0], undefined);
                    oSearchArray.shift();
                    termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
                }
                $scope.CBAPagination.toIndex = $scope.filterCbaGrid.data.length;

            };
            /*Global Search calling function from template end here*/


        }

    };




    $scope.OpenAssignModal = function () {

        $scope.modalInstance = $uibModal.open({

            templateUrl: 'AssignUserModalContent.html',
            controller: 'AssignUserModalInstanceController',
            scope: $scope,
            backdrop: 'static',
            keyboard: false,
            resolve: {
                selectedFacilities: function () {
                    return $scope.selectedFacilityObj;
                },
                userDropdownList: function () {
                    return $scope.userList;
                }

            }
        });
    };


}]);


angular.module('cnaApp').controller('subGridController', ['$scope', 'DateConversionService', function subGridController($scope, DateConversionService) {

    $scope.GetStatus = function (toDate) {
        function Dateconvert(str) {

            var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
            return [date.getFullYear(), mnth, day].join("-");
        }

        var date = Dateconvert(new Date());
        toDate = Dateconvert(toDate);

        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        var firstDate = new Date(toDate);
        var secondDate = new Date(date);

        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));

        if (date > toDate) {
            return "Expired";

        } else {
            if (diffDays < 30) {
                return "Expiring";

            } else {
                return "Active";
            }

        }


    }


    $scope.GetStatusClass = function (toDate) {
        function Dateconvert(str) {

            var date = new Date(str),
                mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                day = ("0" + date.getDate()).slice(-2);
            return [date.getFullYear(), mnth, day].join("-");
        }

        var date = Dateconvert(new Date());
        toDate = Dateconvert(toDate);

        var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
        var firstDate = new Date(toDate);
        var secondDate = new Date(date);

        var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime()) / (oneDay)));
        ////console.log(diffDays);
        if (date > toDate) {
            return "rule-red";

        } else {
            if (diffDays < 30) {
                return "rule-orange";

            } else {
                return "rule-green";
            }

        }


    }

    /* $scope.GetAdditionInfo = function(id) {
//console.log(id);
    }*/




}]);








angular.module('cnaApp').controller('CBARuleListModalController', ['$scope', '$rootScope', '$uibModalInstance', '$loading', 'toaster', 'CBARulesService', 'DateConversionService', 'selectedFacilityIdToCba', function EnrichModalController($scope, $rootScope, $uibModalInstance, $loading, toaster, CBARulesService, DateConversionService, selectedFacilityIdToCba) {
    //console.log(selectedFacilityIdToCba);
    $rootScope.SelectedFacilityId = selectedFacilityIdToCba;
    $rootScope.itemsList = {};

    $scope.AddRule = function () {
        $scope.showRuleForm = true;

        //$scope.itemsList.items1.push({'Id': i, 'Label': 'Biweekly '+i});
    };
    $scope.CancelRule = function () {
        $scope.showRuleForm = false;
    };
    $scope.CreateRuleObj = {};


    //API Call For All Rule of Selected Facility
    $scope.CBARuleAPIList = function () {
        $loading.start('Ruleloader');
        CBARulesService.RulesByFacility("CBARules?filter[where][facilityId]=" + $rootScope.SelectedFacilityId, function (response) {
            if (response.status == 200) {
                //console.log(response.data);
                $rootScope.itemsList.items = response.data;
                $rootScope.rulePriority = response.data.length;
                $loading.finish('Ruleloader');
            } else {
                $loading.finish('Ruleloader');
            }

        });

    };


    // watching for fee fields mandatory
    $scope.feeReq = false;
    $scope.mandatoryForFee = false;
    $scope.$watch('CreateRuleObj.flatRecurringFees', function (newValue, oldValue) {
        if (newValue) {

            $scope.mandatoryForFee = false;

        } else {
            $scope.mandatoryForFee = true;

        }

    });

    // watching for fee fields mandatory ends here



    //API Call For All Rule of Selected Facility end here
    $scope.CBARuleAPIList();

    $scope.CreateRule = function () {

        $scope.$broadcast('show-errors-check-validity');

        if ($scope.AddRuleForm.$valid) {


            if (DateConversionService.Convert($scope.CreateRuleObj.validFrom) > DateConversionService.Convert($scope.CreateRuleObj.validTo)) {
                toaster.pop("error", "Valid From Date should be less than Valid To Date");
                return false;
            };
            $loading.start('Ruleloader');
            $scope.CreateRuleObj.validFrom = DateConversionService.Convert($scope.CreateRuleObj.validFrom);
            $scope.CreateRuleObj.validTo = DateConversionService.Convert($scope.CreateRuleObj.validTo);


            //$scope.CreateRuleObj.facilityAccountId = $scope.CBAfacilityAccountID;
            $scope.CreateRuleObj.facilityId = $rootScope.SelectedFacilityId;
            $scope.CreateRuleObj.priority = $rootScope.rulePriority + 1;

            //console.log($scope.CreateRuleObj);
            CBARulesService.CreateRule("CBARules", $scope.CreateRuleObj, function (response) {
                ////console.log(response);
                if (response.status == 200) {
                    $loading.finish('Ruleloader');
                    $scope.CBARuleAPIList();
                    $scope.CBATabAPI(20, 0);
                    $scope.$broadcast('show-errors-reset');

                    $scope.CreateRuleObj = {};

                    $scope.showRuleForm = false;
                    $rootScope.enableRuleUpdateBtn = false;
                    $rootScope.enableRuleCreateBtn = false;


                    ////console.log(response);

                    toaster.pop("success", "New CBA Rule has been Added Successfully");
                    //$uibModalInstance.close();
                } else {
                    $loading.finish('Ruleloader');
                    toaster.pop("error", response.data.error.message);
                }

            });


        }

    };



    $scope.CloseCBAModal = function () {
        //console.log("closed");
        $scope.CBATabAPI(20, 0);
        $uibModalInstance.close();
    };


}]);





/**
 * Defines adminUser dashboard controller.
 * @param {!angular.$scope} $scope 
 * @constructor
 * @export cnaApp
 * @ngInject
 * @ui grid ,$http, $q, $interval,$timeout 
 */

angular.module('cnaApp').controller("reportDetailController", ['$scope', '$rootScope', '$uibModal', 'uiGridConstants', '$http', '$q', '$interval', 'toaster', '$filter', '$stateParams', '$loading', 'ReportService', 'ReportDescriptionService','DateConversionFilter','$state', function ($scope, $rootScope, $uibModal, $http, $q, $interval, toaster, uiGridConstants, $filter, $stateParams, $loading, ReportService, ReportDescriptionService,DateConversionFilter,$state) {
	
	$scope.reportsPage = false;
    $scope.createButton = false;
    $scope.editButton = false;
    $rootScope.reportId = $stateParams.reportId;
    console.log($rootScope.reportId);
    //$scope.reportInfoObj = {};
    $scope.TargetColObj = {};

    $scope.enableEditMapBtn = false;
    $scope.enableCreateMapBtn = false;
    $rootScope.disableEditMap = false;
    $rootScope.seletedArray = [];

    $scope.TransformationRules = [{
            Id: 1,
            Name: 'substring-before-space'
    }, {
            Id: 2,
            Name: 'substring-after-space'
    }, {
            Id: 3,
            Name: 'substring-before-comma'
    }, {
            Id: 4,
            Name: 'substring-after-comma'
    }, {
            Id: 5,
            Name: 'substring-after-comma-last-space'
    }, {
            Id: 6,
            Name: 'last-string-after-space'
    }
        /*, {
                Id: 7,
                Name: 'CleanSSN'
            }, {
                Id: 8,
                Name: 'FTEFromHours(1Week)'
            }, {
                Id: 9,
                Name: 'FTEFromHours(2Weeks)'
            }, {
                Id: 10,
                Name: 'DateFormat(MM-DD-YYYY)'
            }, {
                Id: 11,
                Name: 'GetPhonePart(AreaCode)'
            }, {
                Id: 12,
                Name: 'GetPhonePart(MainNumber)'
            }, {
                Id: 13,
                Name: 'GetPhonePart(Extension)'
            }*/
        ];



    /*API Call for getting  all records for matching duplicate records */
    $scope.ProcessedAllWithoutPaginationRowsAPI = function () {
        $scope.ProcessedAllDuplicateRows = [];
        ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed", function (response) {
            if (response.status == 200) {
            	$scope.processedRows = response.data;
                angular.forEach(response.data, function (val, key) {
                    $scope.ProcessedAllDuplicateRowsObj = {};

                    angular.forEach(val.mappedReportFields, function (val, key) {

                        var fieldNames;
                        var filedValues;
                        fieldNames = val.fieldName;
                        filedValues = val.value;
                        $scope.ProcessedAllDuplicateRowsObj[fieldNames] = filedValues;

                    });
                    $scope.ProcessedAllDuplicateRows.push($scope.ProcessedAllDuplicateRowsObj);
                });

            }

        });

    };
    /*API Call for getting  all records for matching duplicate records end here */
    $scope.ProcessedAllWithoutPaginationRowsAPI();

    $scope.rawRecordGridOptions = {
        enableGridMenu: true,
    };


    $scope.limit = 20;
    if (localStorage.id + "rawReportPage") {
        $scope.limit = localStorage.getItem(localStorage.id + "rawReportPage");
        if (!$scope.limit) {
            $scope.limit = 20;
        }
        $scope.RawReportpageLimitValue = localStorage.getItem(localStorage.id + "rawReportPage");
    }
    $scope.pageNumber = 0;
    $scope.Rawreportpresentpage = 1;
    ReportService.RawReportCount("&where[reportId]=" + $rootScope.reportId, function (response) {
        //console.log(response);
        if (response.status == 200) {
            $scope.Rawreporttotalcount = response.data.count;
            if ($scope.Rawreporttotalcount < $scope.limit) {
                $scope.rawRecordPagination.toIndex = $scope.Rawreporttotalcount;
            } else {
                $scope.rawRecordPagination.toIndex = $scope.limit;
            }
            $scope.Rawreporttotalpages = Math.ceil($scope.Rawreporttotalcount / $scope.limit);
        }
    });

    $scope.rawRecordPagination = {
        //limit:10,
        limit: $scope.limit,
        skip: 0,
        //pageNumber: 0,
        fromIndex: 1,
        //toIndex: $scope.limit,
        pageNumber: 0,
        loadSelCountPerPage: function (pageLimitValue) {
            localStorage.setItem(localStorage.id + "rawReportPage", pageLimitValue);
            this.pageNumber = 0;
            this.limit = pageLimitValue;
            this.skip = 0;
            $scope.Rawreportpresentpage = 1;
            $scope.Rawreporttotalpages = Math.ceil($scope.Rawreporttotalcount / pageLimitValue);
            $scope.rawRecordPagination.fromIndex = this.skip + 1;
            if ($scope.Rawreporttotalcount < pageLimitValue) {
                $scope.rawRecordPagination.toIndex = $scope.Rawreporttotalcount;
            } else {
                $scope.rawRecordPagination.toIndex = this.skip + Number(this.limit);
            }

            /*API Call For Raw Report Rows Detals  */
            $loading.start('RawReport');
            $scope.RawrowData = [];
            $scope.RawrowFieldData = [];

            ReportService.GetRawReportRows("&filter[where][reportId]=" + $rootScope.reportId + "&filter[include]=rawReportFields&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                if (response.status == 200) {
                    //console.log(response);
                    //Making Json Objects for UI grid for Raw Report Rows
                    ////console.log(response.data);
                    //xml= json2xml(response.data);
                    ////console.log(xml);
                    angular.forEach(response.data, function (val, key) {
                        $scope.RawrowDataObj = {};
                        $scope.RawrowFieldDataObj = {};


                        $scope.RawrowDataObj.facilityAccountId = val.facilityAccountId;
                        $scope.RawrowDataObj.id = val.id;
                        $scope.RawrowDataObj.reportId = val.reportId;
                        $scope.RawrowDataObj.row = val.row;


                        angular.forEach(val.rawReportFields, function (val, key) {
                            ////console.log(val.fieldName,val.value);

                            var fieldNames;
                            var filedValues;
                            fieldNames = val.fieldName;
                            filedValues = val.value;
                            $scope.RawrowDataObj[fieldNames] = filedValues;
                            $scope.RawrowFieldDataObj[fieldNames] = filedValues;

                        });

                        $scope.RawrowData.push($scope.RawrowDataObj);
                        $scope.RawrowFieldData.push($scope.RawrowFieldDataObj);
                    });
                    //Making Json Objects for UI grid for Raw Report Rows end Here

                    $scope.rawRecordGridOptions.data = $scope.RawrowData;
                    $scope.SourceColArr = $scope.rawRecordGridOptions.data[0];
                    $scope.rawRowHeaderArr = $scope.RawrowFieldData[0];
                    $scope.rawRecordGridOptions.columnDefs = [];

                    for (elem in $scope.rawRowHeaderArr) {
                        $scope.rawRowHeaderObj = {};

                        $scope.rawRowHeaderObj['field'] = elem;
                        $scope.rawRowHeaderObj['width'] = 150;
                        $scope.rawRecordGridOptions.columnDefs.push($scope.rawRowHeaderObj);

                    }

                    //Creating Souce Columns
                    $scope.SourceColField = [];
                    for (elem in $scope.rawRowHeaderArr) {
                        $scope.sourceDataObj = {};
                        $scope.sourceDataObj['colfields'] = elem;
                        $scope.SourceColField.push($scope.sourceDataObj);

                    }
                    //Creating Souce Columns end Here

                    $loading.finish('RawReport');

                } else {
                    $loading.finish('RawReport');
                    toaster.pop("error", "Users " + response.data.error.message);

                }
            });
            /*API Call For Raw Report Rows Detals end here */


        },
        nextPage: function () {
            if ($scope.Rawreportpresentpage != $scope.Rawreporttotalpages) {
                $scope.Rawreportpresentpage++;
                this.pageNumber++;
                this.skip = Number(this.limit) * Number(this.pageNumber);
                $scope.rawRecordPagination.fromIndex = this.skip + 1;
                $nextMaxtoindex = this.skip + Number(this.limit);
                if ($scope.Rawreporttotalcount < $nextMaxtoindex) {
                    $scope.rawRecordPagination.toIndex = $scope.Rawreporttotalcount;
                } else {
                    $scope.rawRecordPagination.toIndex = $nextMaxtoindex;
                }
                /*API Call For Raw Report Rows Detals  */
                $loading.start('RawReport');
                $scope.RawrowData = [];
                $scope.RawrowFieldData = [];

                ReportService.GetRawReportRows("&filter[where][reportId]=" + $rootScope.reportId + "&filter[include]=rawReportFields&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {

                    if (response.status == 200) {
                        //console.log(response);
                        //Making Json Objects for UI grid for Raw Report Rows
                        ////console.log(response.data);
                        //xml= json2xml(response.data);
                        ////console.log(xml);
                        angular.forEach(response.data, function (val, key) {
                            $scope.RawrowDataObj = {};
                            $scope.RawrowFieldDataObj = {};


                            $scope.RawrowDataObj.facilityAccountId = val.facilityAccountId;
                            $scope.RawrowDataObj.id = val.id;
                            $scope.RawrowDataObj.reportId = val.reportId;
                            $scope.RawrowDataObj.row = val.row;


                            angular.forEach(val.rawReportFields, function (val, key) {
                                ////console.log(val.fieldName,val.value);

                                var fieldNames;
                                var filedValues;
                                fieldNames = val.fieldName;
                                filedValues = val.value;
                                $scope.RawrowDataObj[fieldNames] = filedValues;
                                $scope.RawrowFieldDataObj[fieldNames] = filedValues;

                            });

                            $scope.RawrowData.push($scope.RawrowDataObj);
                            $scope.RawrowFieldData.push($scope.RawrowFieldDataObj);
                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.rawRecordGridOptions.data = $scope.RawrowData;
                        $scope.SourceColArr = $scope.rawRecordGridOptions.data[0];
                        $scope.rawRowHeaderArr = $scope.RawrowFieldData[0];
                        $scope.rawRecordGridOptions.columnDefs = [];


                        for (elem in $scope.rawRowHeaderArr) {
                            $scope.rawRowHeaderObj = {};

                            $scope.rawRowHeaderObj['field'] = elem;
                            $scope.rawRowHeaderObj['width'] = 150;
                            $scope.rawRecordGridOptions.columnDefs.push($scope.rawRowHeaderObj);

                        }

                        //Creating Souce Columns
                        $scope.SourceColField = [];
                        for (elem in $scope.rawRowHeaderArr) {
                            $scope.sourceDataObj = {};
                            $scope.sourceDataObj['colfields'] = elem;
                            $scope.SourceColField.push($scope.sourceDataObj);

                        }
                        //Creating Souce Columns end Here

                        $loading.finish('RawReport');

                    } else {
                        $loading.finish('RawReport');
                        toaster.pop("error", response.data.error.message);

                    }
                });
                /*API Call For Raw Report Rows Detals end here */
            }

        },

        previousPage: function () {
            if (this.pageNumber > 0) {
                $scope.Rawreportpresentpage--;
                this.pageNumber--;
                this.skip = this.limit * this.pageNumber;

                $NexttoIndex = $scope.rawRecordPagination.toIndex - this.limit;
                if ($scope.rawRecordPagination.toIndex == $scope.Rawreporttotalcount) {
                    $scope.rawRecordPagination.toIndex = $scope.rawRecordPagination.fromIndex - 1;
                } else {
                    $scope.rawRecordPagination.toIndex = $NexttoIndex;
                }
                $scope.rawRecordPagination.fromIndex = $scope.rawRecordPagination.fromIndex - this.limit;
                /*API Call For Raw Report Rows Detals  */
                $loading.start('RawReport');
                $scope.RawrowData = [];
                $scope.RawrowFieldData = [];

                ReportService.GetRawReportRows("&filter[where][reportId]=" + $rootScope.reportId + "&filter[include]=rawReportFields&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {

                    if (response.status == 200) {
                        //console.log(response);
                        //Making Json Objects for UI grid for Raw Report Rows
                        ////console.log(response.data);
                        //xml= json2xml(response.data);
                        ////console.log(xml);
                        angular.forEach(response.data, function (val, key) {
                            $scope.RawrowDataObj = {};
                            $scope.RawrowFieldDataObj = {};


                            $scope.RawrowDataObj.facilityAccountId = val.facilityAccountId;
                            $scope.RawrowDataObj.id = val.id;
                            $scope.RawrowDataObj.reportId = val.reportId;
                            $scope.RawrowDataObj.row = val.row;


                            angular.forEach(val.rawReportFields, function (val, key) {
                                ////console.log(val.fieldName,val.value);

                                var fieldNames;
                                var filedValues;
                                fieldNames = val.fieldName;
                                filedValues = val.value;
                                $scope.RawrowDataObj[fieldNames] = filedValues;
                                $scope.RawrowFieldDataObj[fieldNames] = filedValues;

                            });

                            $scope.RawrowData.push($scope.RawrowDataObj);
                            $scope.RawrowFieldData.push($scope.RawrowFieldDataObj);
                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.rawRecordGridOptions.data = $scope.RawrowData;
                        $scope.SourceColArr = $scope.rawRecordGridOptions.data[0];
                        $scope.rawRowHeaderArr = $scope.RawrowFieldData[0];
                        $scope.rawRecordGridOptions.columnDefs = [];


                        for (elem in $scope.rawRowHeaderArr) {
                            $scope.rawRowHeaderObj = {};

                            $scope.rawRowHeaderObj['field'] = elem;
                            $scope.rawRowHeaderObj['width'] = 150;
                            $scope.rawRecordGridOptions.columnDefs.push($scope.rawRowHeaderObj);

                        }

                        //Creating Souce Columns
                        $scope.SourceColField = [];
                        for (elem in $scope.rawRowHeaderArr) {
                            $scope.sourceDataObj = {};
                            $scope.sourceDataObj['colfields'] = elem;
                            $scope.SourceColField.push($scope.sourceDataObj);

                        }
                        //Creating Souce Columns end Here

                        $loading.finish('RawReport');

                    } else {
                        $loading.finish('RawReport');
                        toaster.pop("error", response.data.error.message);

                    }
                });
                /*API Call For Raw Report Rows Detals end here */
            }
        }
    };
    //console.log("reportDetailController");
    /* showing action button for main tab*/
    $scope.CheckButton = function (tab) {
        if (tab == 0) {
            $scope.createButton = true;
            $scope.editButton = false;
        } else {
            $scope.createButton = false;
            $scope.editButton = true;
        }

    };
    $scope.ShowColsByUnderscore = function (input) {
        var result = input.replace(/([A-Z]+)/g, ",$1");
        var result = result.replace(/^,+/, "");
        return result.split(",").join("_");

    }
    $scope.getSelectedData = function (data) {
        //console.log(data)
    }


    $scope.GetReportInfoAPI = function () {
        //console.log("reportinfoAPIcall");

        $loading.start('ReportInfo');
        ReportService.ReportInfos("Reports/" + $rootScope.reportId, function (response) {
            //console.log(response);

            if (response.status == 200) {
                //console.log(response.data.status);
                $scope.reportInfoObj = response.data;
                $scope.reportInfoObj.reportPeriodTo=DateConversionFilter.Convert($scope.reportInfoObj.reportPeriodTo);
                if($scope.reportInfoObj.importDate ){
                     $scope.reportInfoObj.importDate=DateConversionFilter.Convert($scope.reportInfoObj.importDate);

                }else{
                     $scope.reportInfoObj.importDate="";

                }
                if($scope.reportInfoObj.processedDate){
                     $scope.reportInfoObj.processedDate=DateConversionFilter.Convert($scope.reportInfoObj.processedDate);
                }else{
                   $scope.reportInfoObj.processedDate=""; 
                }
               
               
                $rootScope.reportType = $scope.reportInfoObj.reportType;
                if ($scope.reportInfoObj.facility) {
                    $rootScope.facilityAccountId = $scope.reportInfoObj.facility.facilityAccountId;
                } else {
                    $rootScope.facilityAccountId = "";
                }

                //console.log($rootScope.facilityAccountId);
                if ($scope.reportInfoObj.status == 'processed') {
                    $rootScope.seletedArray = [true, true, false];

                    $rootScope.isSelectedActivity = 1;
                    $rootScope.activityTabStatus = [false, true, false];
                    $rootScope.disableEditMap = true;
                    $rootScope.ImportEditMap = false;

                } else if ($scope.reportInfoObj.status == 'imported') {
                    $rootScope.seletedArray = [true, true, true];

                    $rootScope.isSelectedActivity = 2;
                    $rootScope.activityTabStatus = [false, false, true];
                    $rootScope.ImportEditMap = true;
                    $rootScope.disableEditMap = false;
                } else {
                    $rootScope.seletedArray = [true, false, false];

                    $rootScope.isSelectedActivity = 0;
                    $rootScope.activityTabStatus = [true, false, false];
                    $rootScope.disableEditMap = true;
                    $rootScope.ImportEditMap = false;
                }

                //console.log($rootScope.reportType);
                //console.log($rootScope.facilityAccountId);
                $scope.GetTemplateAPI();



                $loading.finish('ReportInfo');

            } else {
                $loading.finish('ReportInfo');
                toaster.pop("error", response.data.error.message);

            }
        });

    };

    $scope.GetReportInfoAPI();


    $scope.GetTemplateAPI = function () {

        ////console.log($scope.reportInfoObj.facilityAccountId);
        //.log($scope.reportInfoObj.reportType);
        //console.log($rootScope.facilityAccountId);
        ReportService.GetTemplateByReportType("&filter[where][facilityAccountId]=" + $rootScope.facilityAccountId + "&filter[where][reportType]=" + $rootScope.reportType, function (response) {
            //console.log(response);
            if (response.status == 200) {
                $scope.templateObj = response.data[0];
               // alert($rootScope.reportType);
                //console.log($scope.templateObj);
                if ($scope.templateObj) {

                    $scope.enableEditMapBtn = true;
                    $scope.enableCreateMapBtn = false;

                } else {

                    $scope.enableEditMapBtn = false;
                    $scope.enableCreateMapBtn = true;

                }

            }
        });

    };


    /*API Call For Raw Report Rows Detals  */
    $loading.start('RawReport');
    $scope.RawrowData = [];
    $scope.RawrowFieldData = [];
    var reportlimit;
    reportlimit = localStorage.getItem(localStorage.id + "rawReportPage");
    if (!reportlimit) {
        reportlimit = 20;

    }

    ReportService.GetRawReportRows("&filter[where][reportId]=" + $rootScope.reportId + "&filter[include]=rawReportFields&filter[limit]=" + reportlimit + "&filter[skip]=0", function (response) {

        if (response.status == 200) {
            //console.log(response);
            //Making Json Objects for UI grid for Raw Report Rows
            ////console.log(response.data);
            //xml= json2xml(response.data);
            ////console.log(xml);
            angular.forEach(response.data, function (val, key) {
                $scope.RawrowDataObj = {};
                $scope.RawrowFieldDataObj = {};

                $scope.RawrowDataObj.facilityAccountId = val.facilityAccountId;
                $scope.RawrowDataObj.id = val.id;
                $scope.RawrowDataObj.reportId = val.reportId;
                $scope.RawrowDataObj.row = val.row;

                angular.forEach(val.rawReportFields, function (val, key) {
                    ////console.log(val.fieldName,val.value);

                    var fieldNames;
                    var filedValues;
                    var newDate;
                    fieldNames = val.fieldName;
                    filedValues = val.value;
                    if(fieldNames == "Hire Date"){
                    	var utc_days  = Math.floor(filedValues - 25569);
                    	   var utc_value = utc_days * 86400;                                        
                    	   var date_info = new Date(utc_value * 1000);

                    	   var fractional_day = filedValues - Math.floor(filedValues) + 0.0000001;

                    	   var total_seconds = Math.floor(86400 * fractional_day);

                    	   var seconds = total_seconds % 60;

                    	   total_seconds -= seconds;

                    	   var hours = Math.floor(total_seconds / (60 * 60));
                    	   var minutes = Math.floor(total_seconds / 60) % 60;

                    	   newDate = new Date(date_info.getFullYear(), date_info.getMonth(), date_info.getDate(), hours, minutes, seconds);
                    	   console.log(newDate);
                    	   var date = new Date(newDate);
                    	   filedValues = (date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear();
                    	   
                    }
                    $scope.RawrowDataObj[fieldNames] = filedValues;
                    $scope.RawrowFieldDataObj[fieldNames] = filedValues;

                });

                $scope.RawrowData.push($scope.RawrowDataObj);
                $scope.RawrowFieldData.push($scope.RawrowFieldDataObj);
            });
            //Making Json Objects for UI grid for Raw Report Rows end Here
            console.log("Data is"+angular.toJson($scope.RawrowData));
            $scope.rawRecordGridOptions.data = $scope.RawrowData;
            $scope.SourceColArr = $scope.rawRecordGridOptions.data[0];
            $scope.rawRowHeaderArr = $scope.RawrowFieldData[0];
            $scope.rawRecordGridOptions.columnDefs = [];

            for (elem in $scope.rawRowHeaderArr) {
                $scope.rawRowHeaderObj = {};

                $scope.rawRowHeaderObj['field'] = elem;
                $scope.rawRowHeaderObj['width'] = 150;
                $scope.rawRecordGridOptions.columnDefs.push($scope.rawRowHeaderObj);

            }

            //Creating Souce Columns
            $scope.SourceColField = [];
            for (elem in $scope.rawRowHeaderArr) {
                $scope.sourceDataObj = {};
                $scope.sourceDataObj['colfields'] = elem;
                $scope.SourceColField.push($scope.sourceDataObj);

            }
            //Creating Souce Columns end Here

            $loading.finish('RawReport');

        } else {
            $loading.finish('RawReport');
            toaster.pop("error", response.data.error.message);

        }
    });

    /*API Call For Raw Report Rows Detals end here */
    $scope.SampledataObj = {};
    $scope.fullName = [];
    $scope.ruleToken = undefined;

    $scope.CreateMap = function () {

        $loading.start('CreateMapping');
        $scope.SampledataObj = $scope.rawRecordGridOptions.data[0];
        //console.log($scope.SampledataObj);

        ReportDescriptionService.GetTargetColumn("ReportDescriptions/findOne?" + "filter[where][reportType]=" + $rootScope.reportType, function (response) {
            //console.log(response);
            if (response.status == 200) {
                $scope.TargetColObj = response.data.reportFields;
                $scope.SourceColValueObj = $scope.TargetColObj;

                $loading.finish('CreateMapping');
            } else {
                $loading.finish('CreateMapping');
                toaster.pop("error", response.data.error.message);

            }
        });

        $scope.modalInstance = $uibModal.open({

            templateUrl: 'CreateMapModalContent.html',
            controller: 'CreateMapModalInstanceController',
            scope: $scope,
            size: 'lg',
        });

    };

    $scope.EditMap = function () {

        $scope.SampledataObj = $scope.rawRecordGridOptions.data[0];
        //console.log($scope.SampledataObj);
        //console.log('hih');

        $loading.start('UpdateMapping');
        ReportDescriptionService.GetTargetColumn("ReportDescriptions/findOne?" + "filter[where][reportType]=" + $rootScope.reportType, function (response) {
            //console.log(response);
            if (response.status == 200) {
                $loading.finish('UpdateMapping');
                $scope.TargetColObj = response.data.reportFields;

                json = xmlToJSON.parseString($scope.templateObj.xslt);

                var tagetSorce = $scope.TargetColObj;
                $scope.targetSourceObj = tagetSorce;
                //console.log(json);
                ////console.log(json.stylesheet[0].template[0].report[0]+'.for-each[0].'+reportRows[0].mappedReportField);
                ////console.log(json.stylesheet[0].template[0].report[0]['for-each'][0]['reportRows'][0]['mappedReportField']);
                $scope.MappedSelectedFiels = json.stylesheet[0].template[3].report[0]['for-each'][0]['reportRows'][0]['mappedReportField'];
                //$scope.mappedXsltArr = [];
                //console.log($scope.MappedSelectedFiels);
                angular.forEach($scope.targetSourceObj, function (val1, key1) {

                    angular.forEach($scope.MappedSelectedFiels, function (val, key) {

                        var tempFieldValue;
                        var tempFieldRule;
                        if (typeof (val['value'][0]['value-of']) != "undefined") {
                            tempFieldValue = val['value'][0]['value-of'][0]['_attr']['select']['_value'].match("='(.*)']");
                            tempFieldRule = val['value'][0]['value-of'][0]['_attr']['select']['_value'].split('(');

                            //console.log("value-of");

                        } else if (typeof (val['value'][0]['call-template']) != "undefined") {

                            //console.log("call-template");
                            //tempFieldRule = val['value'][0]['call-template'][0]['_attr']['name']['_value'];
                            tempFieldValue = val['value'][0]['call-template'][0]['with-param'][0]['_attr']['select']['_value'].match("='(.*)']");
                            tempFieldRule = "substring-after-comma-last-space";

                        } else if (typeof (val['value'][0]['choose']) != "undefined") {
                            //console.log("choose");
                            tempFieldValue = val['value'][0]['choose'][0]['when'][0]['_attr']['test']['_value'].match("='(.*)']");
                            tempFieldRule = "last-string-after-space";
                        }


                        /* if(typeof(val['value'][0]['value-of']!="undefined")){

                           
                            //console.log(val['value'][0]['value-of'][0]['_attr']['select']['_value'].match("='(.*)']"));

                         }else{
                               //console.log(val['value'][0]);
                         }
                            
                        var tempFieldValue = val['value'][0]['value-of'][0]['_attr']['select']['_value'].match("='(.*)']");
                        var tempFieldRule = val['value'][0]['value-of'][0]['_attr']['select']['_value'].split('(');*/

                        if (val1['fieldName'] == val['fieldName'][0]['_text']) {
                            //console.log(tempFieldValue);
                            //console.log(tempFieldRule[1]);
                            val1['fieldValue'] = tempFieldValue[1];

                            var tempSourceVal;
                            var tempSplitToken;
                            var editSplitTempVal;
                            tempSourceVal = $scope.SampledataObj[tempFieldValue[1]];

                            var editSplitTempVal = tempSourceVal.split(',');

                            if (editSplitTempVal.length == 1) {
                                val1['splitToken'] = ' ';
                                tempSplitToken = ' ';
                                //console.log("split by comma");
                            } else {
                                val1['splitToken'] = ',';
                                tempSplitToken = ',';
                                //console.log("split by space");
                            }


                            ////console.log(tempSplitToken);
                            /* if (tempFieldRule[2] == "substring-after") {
                                 val1['fieldRule'] = "middle-name";
                                 var splitValue = $scope.SampledataObj[tempFieldValue[1]].split(tempSplitToken);
                                 splitValue = splitValue[1].split(" ");

                                 if ((splitValue[1].charAt(splitValue[1].length - 1) == ",") || (splitValue[1].charAt(0) == ",")) {

                                     val1['fieldResult'] = splitValue[1].replace(/(^,)|(,$)/g, "");

                                 } else {
                                     val1['fieldResult'] = splitValue[1];
                                 }

                             }*/
                            if (tempFieldRule[1] == "substring-after") {
                                if (tempSplitToken == ',') {
                                    val1['fieldRule'] = "substring-after-comma";
                                } else {
                                    val1['fieldRule'] = "substring-after-space";
                                }

                                var splitValue = $scope.SampledataObj[tempFieldValue[1]].split(tempSplitToken);
                                var tmpsplitValue = splitValue[1].split(" ");

                                if ((splitValue[1].charAt(splitValue[1].length - 1) == ",") || (splitValue[1].charAt(0) == ",")) {
                                    val1['fieldResult'] = splitValue[1].replace(/(^,)|(,$)/g, "");

                                } else {
                                    val1['fieldResult'] = splitValue[1];
                                }

                            } else if (tempFieldRule[1] == "substring-before") {
                                if (tempSplitToken == ',') {
                                    val1['fieldRule'] = "substring-before-comma";
                                } else {
                                    val1['fieldRule'] = "substring-before-space";
                                }


                                var splitValue = $scope.SampledataObj[tempFieldValue[1]].split(tempSplitToken);
                                if ((splitValue[0].charAt(splitValue[0].length - 1) == ",") || (splitValue[0].charAt(0) == ",")) {
                                    val1['fieldResult'] = splitValue[0].replace(/(^,)|(,$)/g, "");

                                } else {
                                    val1['fieldResult'] = splitValue[0];
                                }

                            } else if (tempFieldRule == "substring-after-comma-last-space") {
                                var splitValue = $scope.SampledataObj[tempFieldValue[1]].split(',');
                                val1['fieldRule'] = "substring-after-comma-last-space";
                                var editsubStingAfterTmp = "";
                                var editTempSplitVal = splitValue[1].trim();
                                editsubStrVal = editTempSplitVal.split(' ');
                                if (editsubStrVal.length == 1) {
                                    val1['fieldResult'] = splitValue[1];
                                } else {
                                    for (var i = 0; i < editsubStrVal.length - 1; i++) {
                                        editsubStingAfterTmp += editsubStrVal[i] + " ";

                                    }
                                    val1['fieldResult'] = editsubStingAfterTmp;
                                }


                            } else if (tempFieldRule == "last-string-after-space") {
                                var splitValue = $scope.SampledataObj[tempFieldValue[1]].split(',');
                                var editsubStingAfterTmp = "";
                                val1['fieldRule'] = "last-string-after-space";
                                var editTempSplitVal = splitValue[1].trim();
                                editTempSplitVal = editTempSplitVal.split(' ');
                                if (editTempSplitVal.length == 1) {
                                    val1['fieldResult'] == "";
                                } else {
                                    val1['fieldResult'] = editTempSplitVal[editTempSplitVal.length - 1];
                                }

                            } else {

                                val1['fieldResult'] = $scope.SampledataObj[tempFieldValue[1]];

                            }


                            /* if (tempFieldRule[1] == 'substring-before') {
                                 val1['fieldRule'] = tempFieldRule[1];
                                 var splitValue = $scope.SampledataObj[tempFieldValue[1]].split(tempSplitToken);
                                 if((splitValue[0].charAt(splitValue[0].length-1 ) == ",") || (splitValue[0].charAt(0)=="," )) {
                                    val1['fieldResult'] = splitValue[0].replace(/(^,)|(,$)/g, "");
                                    
                                 }else{
                                   val1['fieldResult'] = splitValue[0];
                                 }
                                 
                             } else if (tempFieldRule[1] == 'substring-after') {
                                 val1['fieldRule'] = tempFieldRule[1];
                                 var splitValue = $scope.SampledataObj[tempFieldValue[1]].split(tempSplitToken);
                                 //console.log(splitValue);
                                 //console.log(splitValue[0]);
                                
                                 if((splitValue[1].charAt(splitValue[1].length-1 ) == ",") || (splitValue[1].charAt(0)=="," )) {
                                   
                                      val1['fieldResult'] = splitValue[1].replace(/(^,)|(,$)/g, "");
                                     
                                 }else{
                                    val1['fieldResult'] = splitValue[1];
                                 }
                             } else {

                                 val1['fieldResult'] = $scope.SampledataObj[tempFieldValue[1]];
                             }*/

                        }
                    });

                });
                //console.log($scope.targetSourceObj);

                // $loading.finish('CreateMapping');
            } else {
                $loading.finish('UpdateMapping');
                toaster.pop("error", response.data.error.message);

            }
        });

        $scope.modalInstance = $uibModal.open({

            templateUrl: 'EditMapModalContent.html',
            controller: 'EditMapModalInstanceController',
            scope: $scope,
            size: 'lg',
        });

    };

    $scope.ImportedEditMap = function () {

        $scope.SampledataObj = $scope.rawRecordGridOptions.data[0];
        //console.log($scope.SampledataObj);
        //console.log('hih');

        $loading.start('UpdateMapping');
        ReportDescriptionService.GetTargetColumn("ReportDescriptions/findOne?" + "filter[where][reportType]=" + $rootScope.reportType, function (response) {
            //console.log(response);
            if (response.status == 200) {
                $loading.finish('UpdateMapping');
                $scope.TargetColObj = response.data.reportFields;

                json = xmlToJSON.parseString($scope.templateObj.xslt);

                var tagetSorce = $scope.TargetColObj;
                $scope.targetSourceObj = tagetSorce;
                //console.log(json);
                ////console.log(json.stylesheet[0].template[0].report[0]+'.for-each[0].'+reportRows[0].mappedReportField);
                ////console.log(json.stylesheet[0].template[0].report[0]['for-each'][0]['reportRows'][0]['mappedReportField']);
                $scope.MappedSelectedFiels = json.stylesheet[0].template[3].report[0]['for-each'][0]['reportRows'][0]['mappedReportField'];
                //$scope.mappedXsltArr = [];
                //console.log($scope.MappedSelectedFiels);
                angular.forEach($scope.targetSourceObj, function (val1, key1) {

                    angular.forEach($scope.MappedSelectedFiels, function (val, key) {

                        var tempFieldValue;
                        var tempFieldRule;
                        if (typeof (val['value'][0]['value-of']) != "undefined") {
                            tempFieldValue = val['value'][0]['value-of'][0]['_attr']['select']['_value'].match("='(.*)']");
                            tempFieldRule = val['value'][0]['value-of'][0]['_attr']['select']['_value'].split('(');

                            //console.log("value-of");

                        } else if (typeof (val['value'][0]['call-template']) != "undefined") {

                            //console.log("call-template");
                            //tempFieldRule = val['value'][0]['call-template'][0]['_attr']['name']['_value'];
                            tempFieldValue = val['value'][0]['call-template'][0]['with-param'][0]['_attr']['select']['_value'].match("='(.*)']");
                            tempFieldRule = "substring-after-comma-last-space";

                        } else if (typeof (val['value'][0]['choose']) != "undefined") {
                            //console.log("choose");
                            tempFieldValue = val['value'][0]['choose'][0]['when'][0]['_attr']['test']['_value'].match("='(.*)']");
                            tempFieldRule = "last-string-after-space";
                        }

                        if (val1['fieldName'] == val['fieldName'][0]['_text']) {

                            //console.log(tempFieldValue);
                            //console.log(tempFieldRule[1]);
                            val1['fieldValue'] = tempFieldValue[1];

                            var tempSourceVal;
                            var tempSplitToken;
                            var editSplitTempVal;
                            tempSourceVal = $scope.SampledataObj[tempFieldValue[1]];

                            var editSplitTempVal = tempSourceVal.split(',');

                            if (editSplitTempVal.length == 1) {
                                val1['splitToken'] = ' ';
                                tempSplitToken = ' ';
                                //console.log("split by comma");
                            } else {
                                val1['splitToken'] = ',';
                                tempSplitToken = ',';
                                //console.log("split by space");
                            }

                            if (tempFieldRule[1] == "substring-after") {
                                if (tempSplitToken == ',') {
                                    val1['fieldRule'] = "substring-after-comma";
                                } else {
                                    val1['fieldRule'] = "substring-after-space";
                                }

                                var splitValue = $scope.SampledataObj[tempFieldValue[1]].split(tempSplitToken);
                                var tmpsplitValue = splitValue[1].split(" ");

                                if ((splitValue[1].charAt(splitValue[1].length - 1) == ",") || (splitValue[1].charAt(0) == ",")) {
                                    val1['fieldResult'] = splitValue[1].replace(/(^,)|(,$)/g, "");

                                } else {
                                    val1['fieldResult'] = splitValue[1];
                                }


                            } else if (tempFieldRule[1] == "substring-before") {
                                if (tempSplitToken == ',') {
                                    val1['fieldRule'] = "substring-before-comma";
                                } else {
                                    val1['fieldRule'] = "substring-before-space";
                                }


                                var splitValue = $scope.SampledataObj[tempFieldValue[1]].split(tempSplitToken);
                                if ((splitValue[0].charAt(splitValue[0].length - 1) == ",") || (splitValue[0].charAt(0) == ",")) {
                                    val1['fieldResult'] = splitValue[0].replace(/(^,)|(,$)/g, "");

                                } else {
                                    val1['fieldResult'] = splitValue[0];
                                }

                            } else if (tempFieldRule == "substring-after-comma-last-space") {
                                var splitValue = $scope.SampledataObj[tempFieldValue[1]].split(',');
                                val1['fieldRule'] = "substring-after-comma-last-space";
                                var editsubStingAfterTmp = "";
                                var editTempSplitVal = splitValue[1].trim();
                                editsubStrVal = editTempSplitVal.split(' ');
                                if (editsubStrVal.length == 1) {
                                    val1['fieldResult'] = splitValue[1];
                                } else {
                                    for (var i = 0; i < editsubStrVal.length - 1; i++) {
                                        editsubStingAfterTmp += editsubStrVal[i] + " ";


                                    }
                                    val1['fieldResult'] = editsubStingAfterTmp;
                                }

                            } else if (tempFieldRule == "last-string-after-space") {
                                var splitValue = $scope.SampledataObj[tempFieldValue[1]].split(',');
                                var editsubStingAfterTmp = "";
                                val1['fieldRule'] = "last-string-after-space";
                                var editTempSplitVal = splitValue[1].trim();
                                editTempSplitVal = editTempSplitVal.split(' ');
                                if (editTempSplitVal.length == 1) {
                                    val1['fieldResult'] == "";
                                } else {
                                    val1['fieldResult'] = editTempSplitVal[editTempSplitVal.length - 1];
                                }

                            } else {

                                val1['fieldResult'] = $scope.SampledataObj[tempFieldValue[1]];

                            }
                        }
                    });

                });
                //console.log($scope.targetSourceObj);

                // $loading.finish('CreateMapping');
            } else {
                $loading.finish('UpdateMapping');
                toaster.pop("error", response.data.error.message);

            }
        });

        $scope.modalInstance = $uibModal.open({
            templateUrl: 'ImportedEditMapModalContent.html',
            controller: 'ImportedEditMapModalInstanceController',
            scope: $scope,
            size: 'lg',
        });


    };

    //Selecting Source Column Value For Preparing Sample Data Display While Mapping


    $scope.splitToken = undefined;

    $scope.SelectSourceCol = function ($index, source, target) {

        var createTempVal = $scope.SampledataObj[source];
        $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal;
        $scope.SourceColValueObj[$index]['tmpfieldResult'] = createTempVal;
    };


    $scope.ExpectedResult = function ($index, Value, source) {

        if (source != "") {
            var createTempVal = $scope.SampledataObj[source];

            $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal;
            $scope.SourceColValueObj[$index]['tmpfieldResult'] = createTempVal;
            if (Value) {

                if (Value == 'CleanSSN') { //this condition to extract only numbers.

                    if (createTempVal) {

                        var updatessnVal = createTempVal.trim();
                        updatessnVal = updatessnVal.replace(/[^0-9]/g, '');
                        $scope.SourceColValueObj[$index]['fieldResult'] = updatessnVal;
                    }
                } else if (Value == 'DateFormat(MM-DD-YYYY)') {

                    if (createTempVal) {
                        var date = new Date(createTempVal),
                            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                            day = ("0" + date.getDate()).slice(-2);
                        $scope.SourceColValueObj[$index]['fieldResult'] = [mnth, day, date.getFullYear()].join("-");

                    }

                } else if (Value == 'substring-before-comma') {

                    if (createTempVal) {
                        var createTempVal = createTempVal.split(',');
                        $scope.SourceColValueObj[$index]['splitToken'] = ',';
                        //console.log(createTempVal);
                        if ((createTempVal[0].charAt(createTempVal[0].length - 1) == ",") || (createTempVal[0].charAt(0) == ",")) {

                            $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal[0].replace(/(^,)|(,$)/g, "");

                        } else {
                            $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal[0];
                        }

                        $scope.SourceColValueObj[$index]['fieldRule'] = Value;
                    }

                } else if (Value == 'substring-after-comma') {

                    if (createTempVal) {
                        var createTempVal = createTempVal.split(',');
                        $scope.SourceColValueObj[$index]['splitToken'] = ',';
                        //console.log(createTempVal);
                        if ((createTempVal[1].charAt(createTempVal[1].length - 1) == ",") || (createTempVal[1].charAt(0) == ",")) {

                            $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal[1].replace(/(^,)|(,$)/g, "");

                        } else {
                            $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal[1];
                        }

                        $scope.SourceColValueObj[$index]['fieldRule'] = Value;

                    }

                } else if (Value == 'substring-after-comma-last-space') {

                    if (createTempVal) {
                        //console.log(createTempVal);
                        var createsubStingAfterTmp = "";
                        var createTempVal = $scope.SourceColValueObj[$index]['tmpfieldResult'].split(',');
                        $scope.SourceColValueObj[$index]['splitToken'] = ',';
                        var createsubStrTempValue = createTempVal[1].trim();

                        createsubStrTempValue = createTempVal[1].split(' ');

                        //console.log(createsubStrTempValue);
                        if ((createTempVal[1].charAt(createTempVal[1].length - 1) == ",") || (createTempVal[1].charAt(0) == ",")) {

                            if (createsubStrTempValue.length == 1) {
                                $scope.SourceColValueObj[$index]['fieldResult'] = createsubStrTempValue[0];

                            } else {
                                for (var i = 0; i < createsubStrTempValue.length - 1; i++) {
                                    //console.log(createsubStrTempValue[i]);
                                    createsubStingAfterTmp += createsubStrTempValue[i] + " ";
                                }
                                $scope.SourceColValueObj[$index]['fieldResult'] = createsubStingAfterTmp.replace(/(^,)|(,$)/g, "");

                            }

                        } else {
                            $scope.SourceColValueObj[$index]['fieldRule'] = Value;

                            if (createsubStrTempValue.length == 1) {
                                $scope.SourceColValueObj[$index]['fieldResult'] = createsubStrTempValue[0];
                            } else {
                                for (var i = 0; i < createsubStrTempValue.length - 1; i++) {

                                    //console.log(createsubStrTempValue[i]);
                                    createsubStingAfterTmp += createsubStrTempValue[i] + " ";

                                    //console.log(createsubStrTempValue[i]);
                                }
                                $scope.SourceColValueObj[$index]['fieldResult'] = createsubStingAfterTmp;

                            }

                        }

                    }

                } else if (Value == 'last-string-after-space') {

                    if (createTempVal) {
                        //console.log(createTempVal);
                        var createTempVal = $scope.SourceColValueObj[$index]['tmpfieldResult'].split(',');
                        $scope.SourceColValueObj[$index]['splitToken'] = ',';
                        createTempVal = createTempVal[1].split(' ');
                        $scope.SourceColValueObj[$index]['fieldRule'] = Value;
                        //updateTempValue[updateTempValue.length - 1];
                        //console.log(createTempVal[createTempVal.length - 1]);
                        if ((createTempVal[createTempVal.length - 1].charAt(createTempVal[createTempVal.length - 1].length - 1) == ",") || (createTempVal[createTempVal.length - 1].charAt(0) == ",")) {
                            if (createTempVal.length == 1) {
                                $scope.SourceColValueObj[$index]['fieldResult'] = "";
                            } else {
                                $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal[createTempVal.length - 1].replace(/(^,)|(,$)/g, "");

                            }

                        } else {
                            if (createTempVal.length == 1) {
                                $scope.SourceColValueObj[$index]['fieldResult'] = "";
                            } else {
                                $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal[createTempVal.length - 1];

                            }

                        }

                    }

                } else if (Value == 'substring-before-space') {
                    createTempVal = $scope.SourceColValueObj[$index]['tmpfieldResult'].split(' ');
                    $scope.splitToken = ' ';
                    if (createTempVal.length == 1) {
                        $scope.SourceColValueObj[$index]['fieldResult'] = $scope.SourceColValueObj[$index]['tmpfieldResult'];
                    } else {
                        $scope.SourceColValueObj[$index]['splitToken'] = $scope.splitToken;
                        $scope.SourceColValueObj[$index]['fieldRule'] = Value;
                        $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal[0];
                    }

                } else if (Value == 'substring-after-space') {
                    createTempVal = $scope.SourceColValueObj[$index]['tmpfieldResult'].split(' ');
                    $scope.splitToken = ' ';
                    //console.log("substring-after-space");
                    if (createTempVal.length == 1) {
                        //console.log("not splited");
                        $scope.SourceColValueObj[$index]['fieldResult'] = $scope.SourceColValueObj[$index]['tmpfieldResult'];

                    } else {
                        //console.log("splited");
                        $scope.SourceColValueObj[$index]['splitToken'] = $scope.splitToken;
                        $scope.SourceColValueObj[$index]['fieldRule'] = Value;
                        $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal[1];
                    }

                } else if (Value == 'FTEFromHours(1Week)') {

                    if (createTempVal) {
                        if (createTempVal != 0) {
                            $scope.SourceColValueObj[$index]['fieldResult'] = (createTempVal / 40).toFixed(2);
                        } else {
                            $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal;
                        }
                    }

                } else if (Value == 'FTEFromHours(2Weeks)') {

                    if (createTempVal) {
                        if (createTempVal != 0) {
                            $scope.SourceColValueObj[$index]['fieldResult'] = (createTempVal / 80).toFixed(2);
                        } else {
                            $scope.SourceColValueObj[$index]['fieldResult'] = createTempVal;
                        }
                    }

                } else if (Value == 'GetPhonePart(AreaCode)') {

                    if (createTempVal) {
                        var updateAreaCode = createTempVal.trim();
                        updateAreaCode = updateAreaCode.replace(/\D/g, '').substr(1, 3);
                        $scope.SourceColValueObj[$index]['fieldResult'] = updateAreaCode;
                    }

                } else if (Value == 'GetPhonePart(Extension)') {

                    if (createTempVal) {

                    }

                } else if (Value == 'GetPhonePart(MainNumber)') {

                    if (createTempVal) {
                        var updateMainNumber = createTempVal.trim();
                        updateMainNumber = updateMainNumber.replace(/\D/g, '').substr(4);
                        //console.log(updateMainNumber);
                        $scope.SourceColValueObj[$index]['fieldResult'] = updateMainNumber;

                    }

                }

            } else {
                //console.log("none rule");
                $scope.SourceColValueObj[$index]['fieldResult'] = $scope.SourceColValueObj[$index]['tmpfieldResult'];
            }

        }

    };
    //End Selecting Source Column Value For Preparing Sample Data Display While Mapping


    $scope.UpdateSelectSourceCol = function ($index, source, target, rule) {
        //console.log($index, source, target, rule);
        var updateTempVal = $scope.SampledataObj[source];
        //console.log($scope.targetSourceObj[$index]);
        $scope.targetSourceObj[$index]['fieldResult'] = updateTempVal;
        $scope.targetSourceObj[$index]['tmpfieldResult'] = updateTempVal;
        if (rule != '' && rule != 'none') {
            var updateTempVal = $scope.targetSourceObj[$index]['tmpfieldResult'].split(' ');
            if (updateTempVal.length == 1) {
                updateTempVal = $scope.targetSourceObj[$index]['fieldResult'].split(',');
                //$scope.$updateSplitToken = ',';
                if (rule == 'substring-before') {
                    $scope.targetSourceObj[$index]['fieldResult'] = updateTempVal[0];

                } else if (rule == 'substring-after') {
                    $scope.targetSourceObj[$index]['fieldResult'] = updateTempVal[1];
                } else {
                    $scope.targetSourceObj[$index]['fieldResult'] = $scope.targetSourceObj[$index]['tmpfieldResult'];
                }

            }
        } else {
            $scope.targetSourceObj[$index]['fieldResult'] = $scope.targetSourceObj[$index]['tmpfieldResult'];
        }

        //$scope.updateTempArr[$index]=updateTempVal;
        //console.log(updateTempVal);

    };

    $scope.UpdateExpectedResult = function ($index, Value, fieldValue) {
        var updateTempVal = $scope.SampledataObj[fieldValue];

        //console.log($index, Value, fieldValue);
        $scope.targetSourceObj[$index]['tmpfieldResult'] = updateTempVal;
        if ($scope.targetSourceObj[$index]['fieldValue']) {
            if (Value != '' && Value != 'none') {
                if (Value == 'CleanSSN') { //this condition to extract only numbers.

                    if (updateTempVal) {

                        var updatessnVal = updateTempVal.trim();
                        updatessnVal = updatessnVal.replace(/[^0-9]/g, '');
                        $scope.targetSourceObj[$index]['fieldResult'] = updatessnVal;

                    }

                } else if (Value == 'substring-before-comma') {

                    if (updateTempVal) {
                        //console.log(updateTempVal);
                        var updateTempValue = $scope.targetSourceObj[$index]['tmpfieldResult'].split(',');
                        $scope.targetSourceObj[$index]['splitToken'] = ',';
                        if ((updateTempValue[0].charAt(updateTempValue[0].length - 1) == ",") || (updateTempValue[0].charAt(0) == ",")) {

                            $scope.targetSourceObj[$index]['fieldResult'] = updateTempValue[0].replace(/(^,)|(,$)/g, "");

                        } else {
                            $scope.targetSourceObj[$index]['fieldResult'] = updateTempValue[0];
                        }

                        $scope.targetSourceObj[$index]['fieldRule'] = Value;

                    }

                } else if (Value == 'substring-after-comma') {

                    if (updateTempVal) {
                        //console.log(updateTempVal);
                        $scope.targetSourceObj[$index]['splitToken'] = ',';
                        var updateTempValue = $scope.targetSourceObj[$index]['tmpfieldResult'].split(',');
                        if ((updateTempValue[1].charAt(updateTempValue[1].length - 1) == ",") || (updateTempValue[1].charAt(0) == ",")) {

                            $scope.targetSourceObj[$index]['fieldResult'] = updateTempValue[1].replace(/(^,)|(,$)/g, "");

                        } else {
                            $scope.targetSourceObj[$index]['fieldResult'] = updateTempValue[1];
                        }

                        $scope.targetSourceObj[$index]['fieldRule'] = Value;
                    }

                } else if (Value == 'substring-after-comma-last-space') {

                    if (updateTempVal) {
                        //console.log(updateTempVal);
                        var updatesubStingAfterTmp = "";
                        var updateTempValue = $scope.targetSourceObj[$index]['tmpfieldResult'].split(',');
                        $scope.targetSourceObj[$index]['splitToken'] = ',';
                        var updatesubStrTempValue = updateTempValue[1].trim();

                        updatesubStrTempValue = updateTempValue[1].split(' ');
                        //console.log(updatesubStrTempValue.length);
                        //console.log(updatesubStrTempValue);

                        if ((updateTempValue[1].charAt(updateTempValue[1].length - 1) == ",") || (updateTempValue[1].charAt(0) == ",")) {
                            if (updatesubStrTempValue.length == 1) {
                                $scope.targetSourceObj[$index]['fieldResult'] = updateTempValue[1];

                            } else {

                                for (var i = 0; i < updatesubStrTempValue.length - 1; i++) {
                                    ////console.log(updatesubStrTempValue[i]);
                                    updatesubStingAfterTmp += updatesubStrTempValue[i] + " ";
                                }
                                $scope.targetSourceObj[$index]['fieldResult'] = updatesubStingAfterTmp.replace(/(^,)|(,$)/g, "");

                            }

                        } else {
                            $scope.targetSourceObj[$index]['fieldRule'] = Value;

                            if (updatesubStrTempValue.length == 1) {
                                $scope.targetSourceObj[$index]['fieldResult'] = updateTempValue[1];

                            } else {

                                for (var i = 0; i < updatesubStrTempValue.length - 1; i++) {
                                    ////console.log(updatesubStrTempValue[i]);

                                    updatesubStingAfterTmp += updatesubStrTempValue[i] + " ";

                                }
                                $scope.targetSourceObj[$index]['fieldResult'] = updatesubStingAfterTmp;
                            }

                        }

                    }

                } else if (Value == 'last-string-after-space') {

                    if (updateTempVal) {
                        //console.log(updateTempVal);
                        var updateTempValue = $scope.targetSourceObj[$index]['tmpfieldResult'].split(',');
                        $scope.targetSourceObj[$index]['splitToken'] = ',';
                        updateTempValue = updateTempValue[1].split(' ');
                        $scope.targetSourceObj[$index]['fieldRule'] = Value;
                        //updateTempValue[updateTempValue.length - 1];
                        //console.log(updateTempValue[updateTempValue.length - 1]);
                        if ((updateTempValue[updateTempValue.length - 1].charAt(updateTempValue[updateTempValue.length - 1].length - 1) == ",") || (updateTempValue[updateTempValue.length - 1].charAt(0) == ",")) {
                            if (updateTempValue.length == 1) {
                                $scope.targetSourceObj[$index]['fieldResult'] = "";
                            } else {
                                $scope.targetSourceObj[$index]['fieldResult'] = updateTempValue[updateTempValue.length - 1].replace(/(^,)|(,$)/g, "");
                            }

                        } else {

                            if (updateTempValue.length == 1) {
                                $scope.targetSourceObj[$index]['fieldResult'] = "";
                            } else {
                                $scope.targetSourceObj[$index]['fieldResult'] = updateTempValue[updateTempValue.length - 1];

                            }
                        }
                    }

                } else if (Value == 'DateFormat(MM-DD-YYYY)') {

                    if (updateTempVal) {
                        var date = new Date(updateTempVal),
                            mnth = ("0" + (date.getMonth() + 1)).slice(-2),
                            day = ("0" + date.getDate()).slice(-2);
                        $scope.targetSourceObj[$index]['fieldResult'] = [mnth, day, date.getFullYear()].join("-");
                    }

                } else if (Value == 'FTEFromHours(1Week)') {

                    if (updateTempVal) {
                        if (updateTempVal != 0) {
                            $scope.targetSourceObj[$index]['fieldResult'] = (updateTempVal / 40).toFixed(2);
                        } else {
                            $scope.targetSourceObj[$index]['fieldResult'] = updateTempVal;
                        }

                    }

                } else if (Value == 'FTEFromHours(2Weeks)') {

                    if (updateTempVal) {
                        if (updateTempVal != 0) {
                            $scope.targetSourceObj[$index]['fieldResult'] = (updateTempVal / 80).toFixed(2);
                        } else {
                            $scope.targetSourceObj[$index]['fieldResult'] = updateTempVal;
                        }
                    }
                } else if (Value == 'GetPhonePart(AreaCode)') {

                    if (updateTempVal) {
                        var updateAreaCode = updateTempVal.trim();
                        updateAreaCode = updateAreaCode.replace(/\D/g, '').substr(1, 3);
                        $scope.targetSourceObj[$index]['fieldResult'] = updateAreaCode;
                    }
                } else if (Value == 'GetPhonePart(Extension)') {
                    if (updateTempVal) {

                    }

                } else if (Value == 'GetPhonePart(MainNumber)') {
                    if (updateTempVal) {
                        var updateMainNumber = updateTempVal.trim();
                        updateMainNumber = updateMainNumber.replace(/\D/g, '').substr(4);
                        //console.log(updateMainNumber);
                        $scope.targetSourceObj[$index]['fieldResult'] = updateMainNumber;
                    }

                } else if (Value == 'substring-after-space') {
                    //console.log(Value);
                    updateTempVal = $scope.targetSourceObj[$index]['tmpfieldResult'].split(' ');
                    $scope.targetSourceObj[$index]['splitToken'] = ' ';
                    if (updateTempVal.length == 1) {
                        $scope.targetSourceObj[$index]['fieldResult'] = $scope.targetSourceObj[$index]['tmpfieldResult'];
                    } else {

                        $scope.targetSourceObj[$index]['fieldRule'] = Value;
                        $scope.targetSourceObj[$index]['fieldResult'] = updateTempVal[1];
                    }

                } else if (Value == 'substring-before-space') {

                    updateTempVal = $scope.targetSourceObj[$index]['tmpfieldResult'].split(' ');
                    $scope.targetSourceObj[$index]['splitToken'] = ' ';
                    if (updateTempVal.length == 1) {
                        $scope.targetSourceObj[$index]['fieldResult'] = $scope.targetSourceObj[$index]['tmpfieldResult'];
                    } else {

                        $scope.targetSourceObj[$index]['fieldRule'] = Value;
                        $scope.targetSourceObj[$index]['fieldResult'] = updateTempVal[0];
                    }
                }

            } else {

                $scope.targetSourceObj[$index]['fieldResult'] = $scope.targetSourceObj[$index]['tmpfieldResult'];
            }
        } else {
            $scope.targetSourceObj[$index]['fieldResult'] = '';
        }
    };

}]);

angular.module('cnaApp').controller('CreateMapModalInstanceController', ['$state', '$scope', 'toaster', '$loading', '$rootScope', '$uibModalInstance', 'ReportService', function CreateMapModalInstanceController($state, $scope, toaster, $loading, $rootScope, $uibModalInstance, ReportService) {

    $scope.CreateDraftXslt = function () {

        var xslt = '';
        xslt += '<?xml version="1.0" encoding="ISO-8859-1"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:foo="http://www.foo.org/" xmlns:bar="http://www.bar.org">';
        xslt += '<xsl:template name="after-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"concat($d,substring-before($s,$d))\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></xsl:template>';
        xslt += '<xsl:template name="before-last-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"substring-before($s,$d)\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise><xsl:value-of select="$s"/></xsl:otherwise></xsl:choose></xsl:template>';
        xslt += '<xsl:template name="substring-after-last">';
        xslt += '<xsl:param name="string" /><xsl:param name="delimiter" /><xsl:choose>';
        xslt += '<xsl:when test="contains($string,$delimiter)">';
        xslt += '<xsl:call-template name="substring-after-last">';
        xslt += '<xsl:with-param name="string" select=\"substring-after($string, $delimiter)\" />';
        xslt += '<xsl:with-param name="delimiter" select=\"$delimiter\" />';
        xslt += '</xsl:call-template>';
        xslt += '</xsl:when><xsl:otherwise><xsl:value-of select=\"$string\" /></xsl:otherwise>';
        xslt += '</xsl:choose></xsl:template>';
        xslt += '<xsl:template match="/report">';
        xslt += '<report>';
        xslt += '<xsl:for-each select="reportRows">';
        xslt += '<reportRows>';
        xslt += '<row><xsl:value-of select="row" /></row>';
        angular.forEach($scope.SourceColValueObj, function (val, key) {
            //console.log(val);

            if (val.fieldValue) {
                if (val['fieldRule']) {

                    /*if (val['fieldRule'] == 'middle-name') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after(substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'),\'' + ' ' + '\'))" /></value></mappedReportField>';

                        }*/
                    if (val['fieldRule'] == 'substring-after-space') {
                        xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))" /></value></mappedReportField>';

                    } else if (val['fieldRule'] == 'substring-before-space') {
                        xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))" /></value></mappedReportField>';
                    } else if (val['fieldRule'] == 'substring-before-comma') {
                        xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))" /></value></mappedReportField>';
                    } else if (val['fieldRule'] == 'substring-after-comma') {
                        xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))" /></value></mappedReportField>';
                    } else if (val['fieldRule'] == 'substring-after-comma-last-space') {
                        xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:call-template name="before-last-delim"><xsl:with-param name="s" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))"/><xsl:with-param name="d" select="\' \'"/></xsl:call-template></value></mappedReportField>';
                    } else if (val['fieldRule'] == 'last-string-after-space') {
                        xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:choose><xsl:when test="substring-before(' + "normalize-space(substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\')),\'' + ' ' + '\')"><xsl:call-template name="substring-after-last"><xsl:with-param name="string" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))" /><xsl:with-param name="delimiter" select="\' \'" /></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></value></mappedReportField>';
                    }

                } else {
                    xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value\" /></value></mappedReportField>';
                }

            }

        });

        xslt += '</reportRows>';
        xslt += '</xsl:for-each>';
        xslt += '</report>';
        xslt += '</xsl:template>';
        xslt += '</xsl:stylesheet>';
        //console.log(xslt);
        $scope.createTemplateObj = {};

        if ($scope.reportInfoObj.reportType == "PD Application") {

            $scope.createTemplateObj.reportType = $scope.reportInfoObj.reportType;
            $scope.createTemplateObj.xslt = xslt;
            $scope.createTemplateObj.status = 'Draft';

        } else {
            $scope.createTemplateObj.facilityAccountId = $rootScope.facilityAccountId;
            $scope.createTemplateObj.reportType = $scope.reportInfoObj.reportType;
            $scope.createTemplateObj.xslt = xslt;
            $scope.createTemplateObj.status = 'Draft';

        }

        //console.log($scope.createTemplateObj);
        if ($scope.templateObj) {
            //console.log("dsdfs");

            ReportService.UpdateTemplateByReportType("Templates/" + $scope.templateObj.id, $scope.templateObj, function (response) {

                if (response.status == 200) {
                    $loading.finish('UpdateMapping');
                    toaster.pop("success", "Update Mapping has been done Successfully");
                    $scope.enableEditMapBtn = true;
                    $scope.enableCreateMapBtn = false;
                    $uibModalInstance.close();

                } else {
                    $loading.finish('UpdateMapping');
                    toaster.pop("error", response.data.error.message);
                    $uibModalInstance.close();

                }
            });

        } else {

            //console.log("newxa");
            $loading.start('CreateMapping');
            ReportService.CreateTemplateByReportType("Templates", $scope.createTemplateObj, function (response) {
                //console.log(response);

                if (response.status == 200) {
                    $loading.finish('CreateMapping');

                    $uibModalInstance.close();
                    $state.go("reportDetail", {
                        reportId: $rootScope.reportId
                    }, {
                        reload: true
                    });
                } else {
                    $loading.finish('CreateMapping');
                    toaster.pop("error", response.data.error.message);
                    $uibModalInstance.close();

                }
            });

        }

    };

    $scope.GenerateXslt = function () {

        //console.log($scope.SourceColValueObj);

        var CreateMappingValidation = true;
        angular.forEach($scope.SourceColValueObj, function (value, key) {
            if ((value.required == true) && (!value.fieldValue)) {
                CreateMappingValidation = false;
            }

        });
        if (!CreateMappingValidation) {

            toaster.pop("error", "Please select all required Source column fields for Mapping");

        } else {

            var xslt = '';
            xslt += '<?xml version="1.0" encoding="ISO-8859-1"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:foo="http://www.foo.org/" xmlns:bar="http://www.bar.org">';
            xslt += '<xsl:template name="after-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"concat($d,substring-before($s,$d))\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></xsl:template>';
            xslt += '<xsl:template name="before-last-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"substring-before($s,$d)\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise><xsl:value-of select="$s"/></xsl:otherwise></xsl:choose></xsl:template>';
            xslt += '<xsl:template name="substring-after-last">';
            xslt += '<xsl:param name="string" /><xsl:param name="delimiter" /><xsl:choose>';
            xslt += '<xsl:when test="contains($string,$delimiter)">';
            xslt += '<xsl:call-template name="substring-after-last">';
            xslt += '<xsl:with-param name="string" select=\"substring-after($string, $delimiter)\" />';
            xslt += '<xsl:with-param name="delimiter" select=\"$delimiter\" />';
            xslt += '</xsl:call-template>';
            xslt += '</xsl:when><xsl:otherwise><xsl:value-of select=\"$string\" /></xsl:otherwise>';
            xslt += '</xsl:choose></xsl:template>';
            xslt += '<xsl:template match="/report">';
            xslt += '<report>';
            xslt += '<xsl:for-each select="reportRows">';
            xslt += '<reportRows>';
            xslt += '<row><xsl:value-of select="row" /></row>';
            angular.forEach($scope.SourceColValueObj, function (val, key) {
                //console.log(val);

                if (val.fieldValue) {
                    if (val['fieldRule']) {

                        /*if (val['fieldRule'] == 'middle-name') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after(substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'),\'' + ' ' + '\'))" /></value></mappedReportField>';

                            }*/
                        if (val['fieldRule'] == 'substring-after-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))" /></value></mappedReportField>';

                        } else if (val['fieldRule'] == 'substring-before-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))" /></value></mappedReportField>';
                        } else if (val['fieldRule'] == 'substring-before-comma') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))" /></value></mappedReportField>';
                        } else if (val['fieldRule'] == 'substring-after-comma') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))" /></value></mappedReportField>';
                        } else if (val['fieldRule'] == 'substring-after-comma-last-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:call-template name="before-last-delim"><xsl:with-param name="s" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))"/><xsl:with-param name="d" select="\' \'"/></xsl:call-template></value></mappedReportField>';
                        } else if (val['fieldRule'] == 'last-string-after-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:choose><xsl:when test="substring-before(' + "normalize-space(substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\')),\'' + ' ' + '\')"><xsl:call-template name="substring-after-last"><xsl:with-param name="string" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value, \'' + val['splitToken'] + '\'))" /><xsl:with-param name="delimiter" select="\' \'" /></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></value></mappedReportField>';
                        }

                    } else {
                        xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + val['fieldName'] + '</fieldName><value><xsl:value-of select=\"rawReportFields[fieldName=\'' + val['fieldValue'] + '\']/value\" /></value></mappedReportField>';
                    }
                }

            });


            xslt += '</reportRows>';
            xslt += '</xsl:for-each>';
            xslt += '</report>';
            xslt += '</xsl:template>';
            xslt += '</xsl:stylesheet>';
            //console.log(xslt);
            $scope.createTemplateObj = {};

            if ($scope.reportInfoObj.reportType == "PD Application") {

                $scope.createTemplateObj.reportType = $scope.reportInfoObj.reportType;
                $scope.createTemplateObj.xslt = xslt;
                $scope.createTemplateObj.status = 'Active';

            } else {
                //console.log($rootScope.facilityAccountId);
                $scope.createTemplateObj.facilityAccountId = $rootScope.facilityAccountId;
                $scope.createTemplateObj.reportType = $scope.reportInfoObj.reportType;
                $scope.createTemplateObj.xslt = xslt;
                $scope.createTemplateObj.status = 'Active';

            }

            //console.log($scope.createTemplateObj);

            $loading.start('CreateMapping');
            ReportService.CreateTemplateByReportType("Templates", $scope.createTemplateObj, function (response) {
                //console.log(response);

                if (response.status == 200) {

                    $scope.reportObj = {};
                    $scope.reportObj.status = 're-process';
                    $loading.finish('CreateMapping');
                    ReportService.ReportStatusUpdate("Reports/" + $scope.reportInfoObj.id, $scope.reportObj, function (response) {
                        if (response.status == 200) {
                            $scope.reportObj = {};
                            toaster.pop("success", " Create Mapping has been done Successfully");
                            $scope.enableEditMapBtn = true;
                            $scope.enableCreateMapBtn = false;

                            $state.go("reportDetail", {
                                reportId: $rootScope.reportId
                            }, {
                                reload: true
                            });

                        } else {
                            $scope.reportObj = {};
                            $uibModalInstance.close();
                            toaster.pop("error", response.data.error.message);
                        }

                    });

                } else {
                    $loading.finish('CreateMapping');
                    $uibModalInstance.close();
                    toaster.pop("error", response.data.error.message);

                }
            });

        }
    };

    $scope.CloseCreateMapModal = function () {
        $uibModalInstance.close();
    }

}]);

angular.module('cnaApp').controller('EditMapModalInstanceController', ['$scope', '$state', 'toaster', '$loading', '$rootScope', '$uibModalInstance', 'ReportService', function EditMapModalInstanceController($scope, $state, toaster, $loading, $rootScope, $uibModalInstance, ReportService) {

    $scope.UpdateXslt = function () {

        var UpdateMappingValidation = true;
        angular.forEach($scope.targetSourceObj, function (value, key) {
            if ((value.required == true) && (!value.fieldValue)) {
                UpdateMappingValidation = false;

            }

        });
        if (!UpdateMappingValidation) {

            toaster.pop("error", "Please select all required Source column fields for Mapping");

        } else {

            /* //console.log($scope.Resultdisplay)
         //console.log($scope.TargetColObj);*/
            var xslt = '';
            xslt += '<?xml version="1.0" encoding="ISO-8859-1"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:foo="http://www.foo.org/" xmlns:bar="http://www.bar.org">';
            xslt += '<xsl:template name="after-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"concat($d,substring-before($s,$d))\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></xsl:template>';
            xslt += '<xsl:template name="before-last-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"substring-before($s,$d)\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise><xsl:value-of select="$s"/></xsl:otherwise></xsl:choose></xsl:template>';
            xslt += '<xsl:template name="substring-after-last">';
            xslt += '<xsl:param name="string" /><xsl:param name="delimiter" /><xsl:choose>';
            xslt += '<xsl:when test="contains($string,$delimiter)">';
            xslt += '<xsl:call-template name="substring-after-last">';
            xslt += '<xsl:with-param name="string" select=\"substring-after($string, $delimiter)\" />';
            xslt += '<xsl:with-param name="delimiter" select=\"$delimiter\" />';
            xslt += '</xsl:call-template>';
            xslt += '</xsl:when><xsl:otherwise><xsl:value-of select=\"$string\" /></xsl:otherwise>';
            xslt += '</xsl:choose></xsl:template>';
            xslt += '<xsl:template match="/report">';
            xslt += '<report>';
            xslt += '<xsl:for-each select="reportRows">';
            xslt += '<reportRows>';
            xslt += '<row><xsl:value-of select="row" /></row>';

            for (var i = 0; i < $scope.targetSourceObj.length; i++) {
                if ($scope.targetSourceObj[i]['fieldValue']) {
                    if ($scope.targetSourceObj[i]['fieldRule']) {
                        //console.log($scope.targetSourceObj[i]['fieldRule']);
                        if ($scope.targetSourceObj[i]['fieldRule'] != 'none') {

                            /*if ($scope.targetSourceObj[i]['fieldRule'] == 'middle-name') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after(substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'),\'' + ' ' + '\'))" /></value></mappedReportField>';

                            }*/
                            if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-space') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';

                            } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-before-space') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                            } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-before-comma') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                            } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-comma') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                            } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-comma-last-space') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:call-template name="before-last-delim"><xsl:with-param name="s" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))"/><xsl:with-param name="d" select="\' \'"/></xsl:call-template></value></mappedReportField>';
                            } else if ($scope.targetSourceObj[i]['fieldRule'] == 'last-string-after-space') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:choose><xsl:when test="substring-before(' + "normalize-space(substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\')),\'' + ' ' + '\')"><xsl:call-template name="substring-after-last"><xsl:with-param name="string" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /><xsl:with-param name="delimiter" select="\' \'" /></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></value></mappedReportField>';
                            }
                        }
                    } else {
                        xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value\" /></value></mappedReportField>';
                    }
                }
            }
            xslt += '</reportRows>';
            xslt += '</xsl:for-each>';
            xslt += '</report>';
            xslt += '</xsl:template>';
            xslt += '</xsl:stylesheet>';
            //console.log(xslt);

            $scope.templateObj.xslt = xslt;
            $scope.templateObj.status = 'Active';

            $loading.start('UpdateMapping');
            ReportService.UpdateTemplateByReportType("Templates/" + $scope.templateObj.id, $scope.templateObj, function (response) {

                if (response.status == 200) {
                    $scope.reportObj = {};
                    $loading.finish('UpdateMapping');
                    $scope.reportObj.status = 're-process';
                    ReportService.ReportStatusUpdate("Reports/" + $scope.reportInfoObj.id, $scope.reportObj, function (response) {
                        if (response.status == 200) {
                        	$rootScope.controllerName = "UpdateMapping";
                            $scope.reportObj = {};
                            $uibModalInstance.close();
                            console.log($rootScope.reportId);
                            $state.go("reportDetail", {
                                reportId: $rootScope.reportId
                            }, {
                                reload: true
                            });
                            toaster.pop("success", "Save & Reprocess has been done Successfully");

                        } else {
                            $scope.reportObj = {};
                            toaster.pop("error", response.data.error.message);
                            $uibModalInstance.close();
                        }
                    });

                } else {
                    $loading.finish('UpdateMapping');
                    $uibModalInstance.close();
                    toaster.pop("error", response.data.error.message);
                }
            });
        }

    };

    /*Save as Draft the Mapping in Edit map Screen*/
    $scope.UpdateDraftXslt = function () {

        var xslt = '';
        xslt += '<?xml version="1.0" encoding="ISO-8859-1"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:foo="http://www.foo.org/" xmlns:bar="http://www.bar.org">';
        xslt += '<xsl:template name="after-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"concat($d,substring-before($s,$d))\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></xsl:template>';
        xslt += '<xsl:template name="before-last-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"substring-before($s,$d)\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise><xsl:value-of select="$s"/></xsl:otherwise></xsl:choose></xsl:template>';
        xslt += '<xsl:template name="substring-after-last">';
        xslt += '<xsl:param name="string" /><xsl:param name="delimiter" /><xsl:choose>';
        xslt += '<xsl:when test="contains($string,$delimiter)">';
        xslt += '<xsl:call-template name="substring-after-last">';
        xslt += '<xsl:with-param name="string" select=\"substring-after($string, $delimiter)\" />';
        xslt += '<xsl:with-param name="delimiter" select=\"$delimiter\" />';
        xslt += '</xsl:call-template>';
        xslt += '</xsl:when><xsl:otherwise><xsl:value-of select=\"$string\" /></xsl:otherwise>';
        xslt += '</xsl:choose></xsl:template>';
        xslt += '<xsl:template match="/report">';
        xslt += '<report>';
        xslt += '<xsl:for-each select="reportRows">';
        xslt += '<reportRows>';
        xslt += '<row><xsl:value-of select="row" /></row>';

        for (var i = 0; i < $scope.targetSourceObj.length; i++) {
            if ($scope.targetSourceObj[i]['fieldValue']) {
                if ($scope.targetSourceObj[i]['fieldRule']) {
                    //console.log($scope.targetSourceObj[i]['fieldRule']);
                    if ($scope.targetSourceObj[i]['fieldRule'] != 'none') {

                        /*if ($scope.targetSourceObj[i]['fieldRule'] == 'middle-name') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after(substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'),\'' + ' ' + '\'))" /></value></mappedReportField>';

                        }*/
                        if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';

                        } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-before-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                        } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-before-comma') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                        } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-comma') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                        } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-comma-last-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:call-template name="before-last-delim"><xsl:with-param name="s" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))"/><xsl:with-param name="d" select="\' \'"/></xsl:call-template></value></mappedReportField>';
                        } else if ($scope.targetSourceObj[i]['fieldRule'] == 'last-string-after-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:choose><xsl:when test="substring-before(' + "normalize-space(substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\')),\'' + ' ' + '\')"><xsl:call-template name="substring-after-last"><xsl:with-param name="string" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /><xsl:with-param name="delimiter" select="\' \'" /></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></value></mappedReportField>';
                        }

                    }

                } else {
                    xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value\" /></value></mappedReportField>';
                }
            }
        }
        xslt += '</reportRows>';
        xslt += '</xsl:for-each>';
        xslt += '</report>';
        xslt += '</xsl:template>';
        xslt += '</xsl:stylesheet>';
        //console.log(xslt);

        $scope.templateObj.xslt = xslt;
        $scope.templateObj.status = 'Draft';

        $loading.start('UpdateMapping');
        ReportService.UpdateTemplateByReportType("Templates/" + $scope.templateObj.id, $scope.templateObj, function (response) {

            if (response.status == 200) {
                $loading.finish('UpdateMapping');
                toaster.pop("success", "Update Mapping has been done Successfully");

            } else {
                $loading.finish('UpdateMapping');
                toaster.pop("error", response.data.error.message);

            }
        });
    };

    /*Save as Draft the Mapping in Edit map Screen*/
    $scope.CloseEditMapModal = function () {
        $uibModalInstance.close();
    }

}]);

angular.module('cnaApp').controller('ImportedEditMapModalInstanceController', ['$scope', '$state', 'toaster', '$loading', '$rootScope', '$uibModalInstance', 'ReportService', function ImportedEditMapModalInstanceController($scope, $state, toaster, $loading, $rootScope, $uibModalInstance, ReportService) {

    $scope.UpdateXslt = function () {

        var ImportMappingValidation = true;
        angular.forEach($scope.targetSourceObj, function (value, key) {
            if ((value.required == true) && (!value.fieldValue)) {
                ImportMappingValidation = false;

            }
        });
        if (!ImportMappingValidation) {

            toaster.pop("error", "Please select all required Source column fields for Mapping");

        } else {

            var xslt = '';
            xslt += '<?xml version="1.0" encoding="ISO-8859-1"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:foo="http://www.foo.org/" xmlns:bar="http://www.bar.org">';
            xslt += '<xsl:template name="after-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"concat($d,substring-before($s,$d))\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></xsl:template>';
            xslt += '<xsl:template name="before-last-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"substring-before($s,$d)\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise><xsl:value-of select="$s"/></xsl:otherwise></xsl:choose></xsl:template>';
            xslt += '<xsl:template name="substring-after-last">';
            xslt += '<xsl:param name="string" /><xsl:param name="delimiter" /><xsl:choose>';
            xslt += '<xsl:when test="contains($string,$delimiter)">';
            xslt += '<xsl:call-template name="substring-after-last">';
            xslt += '<xsl:with-param name="string" select=\"substring-after($string, $delimiter)\" />';
            xslt += '<xsl:with-param name="delimiter" select=\"$delimiter\" />';
            xslt += '</xsl:call-template>';
            xslt += '</xsl:when><xsl:otherwise><xsl:value-of select=\"$string\" /></xsl:otherwise>';
            xslt += '</xsl:choose></xsl:template>';
            xslt += '<xsl:template match="/report">';
            xslt += '<report>';
            xslt += '<xsl:for-each select="reportRows">';
            xslt += '<reportRows>';
            xslt += '<row><xsl:value-of select="row" /></row>';

            for (var i = 0; i < $scope.targetSourceObj.length; i++) {
                if ($scope.targetSourceObj[i]['fieldValue']) {
                    if ($scope.targetSourceObj[i]['fieldRule']) {
                        //console.log($scope.targetSourceObj[i]['fieldRule']);
                        if ($scope.targetSourceObj[i]['fieldRule'] != 'none') {

                            /*if ($scope.targetSourceObj[i]['fieldRule'] == 'middle-name') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after(substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'),\'' + ' ' + '\'))" /></value></mappedReportField>';

                            }*/
                            if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-space') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';

                            } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-before-space') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                            } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-before-comma') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                            } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-comma') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                            } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-comma-last-space') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:call-template name="before-last-delim"><xsl:with-param name="s" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))"/><xsl:with-param name="d" select="\' \'"/></xsl:call-template></value></mappedReportField>';
                            } else if ($scope.targetSourceObj[i]['fieldRule'] == 'last-string-after-space') {
                                xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:choose><xsl:when test="substring-before(' + "normalize-space(substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\')),\'' + ' ' + '\')"><xsl:call-template name="substring-after-last"><xsl:with-param name="string" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /><xsl:with-param name="delimiter" select="\' \'" /></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></value></mappedReportField>';
                            }

                        }

                    } else {
                        xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value\" /></value></mappedReportField>';
                    }

                }

            }
            xslt += '</reportRows>';
            xslt += '</xsl:for-each>';
            xslt += '</report>';
            xslt += '</xsl:template>';
            xslt += '</xsl:stylesheet>';
            //console.log(xslt);

            $scope.templateObj.xslt = xslt;
            $scope.templateObj.status = 'Active';

            $loading.start('UpdateMapping');
            ReportService.UpdateTemplateByReportType("Templates/" + $scope.templateObj.id, $scope.templateObj, function (response) {
                //console.log(response);
                if (response.status == 200) {
                    $scope.reportObj = {};
                    $loading.finish('UpdateMapping');
                    toaster.pop("success", "Edit Mapping has been done Successfully");
                    $uibModalInstance.close();

                } else {


                    $loading.finish('UpdateMapping');
                    $uibModalInstance.close();
                    toaster.pop("error", response.data.error.message);

                }
            });

        }
    };

    /*Save as Draft the Mapping in Edit map Screen*/
    $scope.UpdateDraftXslt = function () {

        var xslt = '';
        xslt += '<?xml version="1.0" encoding="ISO-8859-1"?><xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:foo="http://www.foo.org/" xmlns:bar="http://www.bar.org">';
        xslt += '<xsl:template name="after-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"concat($d,substring-before($s,$d))\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></xsl:template>';
        xslt += '<xsl:template name="before-last-delim"><xsl:param name="s"/><xsl:param name="d"/><xsl:choose><xsl:when test=\"contains($s,$d)\"><xsl:value-of select=\"substring-before($s,$d)\"/><xsl:call-template name="after-delim"><xsl:with-param name="s" select=\"substring-after($s,$d)\"/><xsl:with-param name="d" select="$d"/></xsl:call-template></xsl:when><xsl:otherwise><xsl:value-of select="$s"/></xsl:otherwise></xsl:choose></xsl:template>';
        xslt += '<xsl:template name="substring-after-last">';
        xslt += '<xsl:param name="string" /><xsl:param name="delimiter" /><xsl:choose>';
        xslt += '<xsl:when test="contains($string,$delimiter)">';
        xslt += '<xsl:call-template name="substring-after-last">';
        xslt += '<xsl:with-param name="string" select=\"substring-after($string, $delimiter)\" />';
        xslt += '<xsl:with-param name="delimiter" select=\"$delimiter\" />';
        xslt += '</xsl:call-template>';
        xslt += '</xsl:when><xsl:otherwise><xsl:value-of select=\"$string\" /></xsl:otherwise>';
        xslt += '</xsl:choose></xsl:template>';
        xslt += '<xsl:template match="/report">';
        xslt += '<report>';
        xslt += '<xsl:for-each select="reportRows">';
        xslt += '<reportRows>';
        xslt += '<row><xsl:value-of select="row" /></row>';

        for (var i = 0; i < $scope.targetSourceObj.length; i++) {
            if ($scope.targetSourceObj[i]['fieldValue']) {
                if ($scope.targetSourceObj[i]['fieldRule']) {
                    //console.log($scope.targetSourceObj[i]['fieldRule']);
                    if ($scope.targetSourceObj[i]['fieldRule'] != 'none') {

                        /*if ($scope.targetSourceObj[i]['fieldRule'] == 'middle-name') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after(substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'),\'' + ' ' + '\'))" /></value></mappedReportField>';

                        }*/
                        if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';

                        } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-before-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                        } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-before-comma') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-before" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                        } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-comma') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /></value></mappedReportField>';
                        } else if ($scope.targetSourceObj[i]['fieldRule'] == 'substring-after-comma-last-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:call-template name="before-last-delim"><xsl:with-param name="s" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))"/><xsl:with-param name="d" select="\' \'"/></xsl:call-template></value></mappedReportField>';
                        } else if ($scope.targetSourceObj[i]['fieldRule'] == 'last-string-after-space') {
                            xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:choose><xsl:when test="substring-before(' + "normalize-space(substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\')),\'' + ' ' + '\')"><xsl:call-template name="substring-after-last"><xsl:with-param name="string" select=\"normalize-space(' + "substring-after" + '(rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value, \'' + $scope.targetSourceObj[i]['splitToken'] + '\'))" /><xsl:with-param name="delimiter" select="\' \'" /></xsl:call-template></xsl:when><xsl:otherwise></xsl:otherwise></xsl:choose></value></mappedReportField>';
                        }

                    }

                } else {
                    xslt += '<mappedReportField><reportId><xsl:value-of select="rawReportFields/reportId" /></reportId><reportRowId><xsl:value-of select="rawReportFields/reportRowId" /></reportRowId><fieldName>' + $scope.targetSourceObj[i]['fieldName'] + '</fieldName><value><xsl:value-of select=\"rawReportFields[fieldName=\'' + $scope.targetSourceObj[i]['fieldValue'] + '\']/value\" /></value></mappedReportField>';
                }

            }

        }
        xslt += '</reportRows>';
        xslt += '</xsl:for-each>';
        xslt += '</report>';
        xslt += '</xsl:template>';
        xslt += '</xsl:stylesheet>';
        //console.log(xslt);

        $scope.templateObj.xslt = xslt;
        $scope.templateObj.status = 'Draft';

        $loading.start('UpdateMapping');
        ReportService.UpdateTemplateByReportType("Templates/" + $scope.templateObj.id, $scope.templateObj, function (response) {

            if (response.status == 200) {
                $loading.finish('UpdateMapping');
                toaster.pop("success", "Update Mapping has been done Successfully");

            } else {
                $loading.finish('UpdateMapping');
                toaster.pop("error", response.data.error.message);
            }
        });

    };

    /*Save as Draft the Mapping in Edit map Screen*/
    $scope.ImportedCloseEditMapModal = function () {
        $uibModalInstance.close();
    }

}]);


angular.module('cnaApp').controller('ActivitytabController', ['$scope', '$rootScope', 'toaster', 'ReportService', function ActivitytabController($scope, $rootScope, toaster, ReportService) {

    //$scope.CheckButton(0);
    //console.log("main tab ");
    /*API Call For Showing Report Detals */

    $scope.selectTabActivity = function (setTab) {

        //console.log($scope.seletedArray);
        $rootScope.isSelectedActivity = setTab;
        // $scope.CheckButton(setTab);
        //alert(setTab);
        if (setTab == 0) {
            $rootScope.activityTabStatus = [true, false, false];
            //console.log("main tab1 by click");

        } else if (setTab == 1) {
            $rootScope.activityTabStatus = [false, true, false];
            //console.log("main tab2 by click");

        } else if (setTab == 2) {
            $rootScope.activityTabStatus = [false, false, true];
            //console.log("main tab3 by click");
        }

    };

}]);

angular.module('cnaApp').controller('UploadedTabController', ['$scope', 'toaster', 'ReportService', '$rootScope', function UploadedTabController($scope, toaster, ReportService, $rootScope) {

    this.tab = 1;

    
    ReportService.ProcessedReportRowAllCount("&where[reportId]=" + $rootScope.reportId, function (response) {
        if (response.status == 200) {

            $scope.TotalReportCount = response.data.count;

        }
    });

    this.selectUploadedTab = function (setTab) {
        this.tab = setTab;

    };
    this.isSelected = function (checkTab) {
        return this.tab === checkTab;
    };

}]);


angular.module('cnaApp').controller('ProcessedtabController', ['$scope', '$rootScope', 'ReportService', '$loading', 'toaster', '$filter', '$uibModal', 'ReportDescriptionService', '$window', '$http', function ProcessedtabController($scope, $rootScope, ReportService, $loading, toaster, $filter, $uibModal, ReportDescriptionService, $window, $http) {

    //console.log($rootScope.reportId);
	console.log("uploaded default tab");
    $rootScope.enrichCreateBtn = true;
    $rootScope.ExceptionenrichCreateBtn = true;
    $scope.enrichReprocessBtn = false;
    $scope.enrichshowingAllSelectionoption = false;
    $scope.enrichshowingAllSelected = false;
    $scope.reportDetailWidths = [];
    $scope.newProcessedRowObject = {};

    var reportDetailInit = function(){
    	$http({
			method : 'GET',
			url : "http://0.0.0.0:3000/api/ReportTables?filter[where][username]="+$rootScope.userInfo.username,
			headers : {
				'Content-Type' : 'application/json'
			}
		}).then(function(response) {
			$scope.reportDetailWidths = response.data;
			console.log(response.data);
			if($scope.reportDetailWidths.length<=0){
				$http({
					method : 'GET',
					url : "http://0.0.0.0:3000/api/ReportTables?filter[where][username]=default",
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function(response) {
					$scope.reportDetailWidths = response.data;
					console.log(response.data);
					console.log($scope.reportDetailWidths[0]);
				}, function(err) {
					alert(angular.toJson(err));
				})
			}else{
			}
			console.log($scope.reportDetailWidths[0]);
		}, function(err) {
			alert(angular.toJson(err));
			
		})
    }
    reportDetailInit();
    $scope.gridOptionsAllProcessedRows = {
        enableGridMenu: true,
        onRegisterApi: function (gridApi) { //register grid data first within the gridOptions
            $scope.gridApi = gridApi;

        },
    };
    /*pagination for ALL tab processed*/
    $scope.AllProcessedPagination = {};

    $scope.allProcessedlimit = localStorage.getItem(localStorage.id + "processedAllPage");
    if (!$scope.allProcessedlimit) {
        $scope.allProcessedlimit = 20;
    }
    
    

    $scope.ProcessedAllPageLimitValue = localStorage.getItem(localStorage.id + "processedAllPage");

    //console.log($scope.allProcessedlimit);
    $scope.pageNumber = 0;
    $scope.AllProcessedpresentpage = 1;

    ReportService.ProcessedReportRowAllCount("&where[reportId]=" + $rootScope.reportId, function (response) {
        if (response.status == 200) {
            $scope.AllProcessedtotalcount = response.data.count;
            //console.log($scope.allProcessedlimit);
            if ($scope.AllProcessedtotalcount < $scope.allProcessedlimit) {
                $scope.AllProcessedPagination.toIndex = $scope.AllProcessedtotalcount;
            } else {
                $scope.AllProcessedPagination.toIndex = $scope.allProcessedlimit;
            }
            $scope.AllProcessedtotalpages = Math.ceil($scope.AllProcessedtotalcount / $scope.allProcessedlimit);
        }
    });
    $scope.gridOptionsAllProcessedRows.onRegisterApi = function (gridApi) {

        $scope.gridApi = gridApi;
        $scope.gridApi.colResizable.on.columnSizeChanged($scope,$scope.columnSizeChanged);
       
    }   

    $scope.AllProcessedPagination = {
        //limit:10,
        limit: $scope.allProcessedlimit,
        skip: 0,
        //pageNumber: 0,
        fromIndex: 1,
        //toIndex: $scope.limit,
        pageNumber: 0,
        loadSelCountPerPage: function (pageLimitValue) {
            localStorage.setItem(localStorage.id + "processedAllPage", pageLimitValue);
            this.pageNumber = 0;
            this.limit = pageLimitValue;
            this.skip = 0;
            $scope.AllProcessedpresentpage = 1;
            $scope.AllProcessedtotalpages = Math.ceil($scope.AllProcessedtotalcount / pageLimitValue);
            $scope.AllProcessedPagination.fromIndex = this.skip + 1;
            //$scope.AllProcessedPagination.toIndex = this.skip + Number(this.limit);
            if ($scope.AllProcessedtotalcount < pageLimitValue) {
                $scope.AllProcessedPagination.toIndex = $scope.AllProcessedtotalcount;
            } else {
                $scope.AllProcessedPagination.toIndex = this.skip + Number(this.limit);
            }
            /*Start of API*/

            $scope.ProcessedAllRows = [];
            $loading.start('ProcessAllTab');
            //http://dev.yogamihi.com/api/ReportRows?filter[where][reportId]=56dd1674f3a04e5d59c57129&access_token={{token}}&filter[include]=mappedReportFields
            ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                if (response.status == 200) {

                    angular.forEach(response.data, function (val, key) {
                        $scope.ProcessedAllRowsObj = {};

                        angular.forEach(val.mappedReportFields, function (val, key) {
                            ////console.log(val.fieldName,val.value);

                            var fieldNames;
                            var filedValues;
                            fieldNames = val.fieldName;
                            filedValues = val.value;
                            $scope.ProcessedAllRowsObj[fieldNames] = filedValues;

                        });
                        $scope.ProcessedAllRows.push($scope.ProcessedAllRowsObj);
                    });
                    //Making Json Objects for UI grid for Raw Report Rows end Here

                    $scope.gridOptionsAllProcessedRows.data = $scope.ProcessedAllRows;

                    $loading.finish('ProcessAllTab');
                } else {
                    $loading.finish('ProcessAllTab');
                    toaster.pop("error", response.data.error.message);
                }

            });
            /*End of API*/
        },
        nextPage: function () {
            if ($scope.AllProcessedpresentpage != $scope.AllProcessedtotalpages) {
                this.pageNumber++;
                $scope.AllProcessedpresentpage++;
                this.skip = this.limit * this.pageNumber;
                $scope.AllProcessedPagination.fromIndex = this.skip + 1;
                $nextMaxtoindex = Number(this.skip) + Number(this.limit);
                if ($scope.AllProcessedtotalcount < $nextMaxtoindex) {
                    $scope.AllProcessedPagination.toIndex = $scope.AllProcessedtotalcount;
                } else {
                    $scope.AllProcessedPagination.toIndex = $nextMaxtoindex;
                }
                /*Start of API*/

                $scope.ProcessedAllRows = [];
                $loading.start('ProcessAllTab');
                //http://dev.yogamihi.com/api/ReportRows?filter[where][reportId]=56dd1674f3a04e5d59c57129&access_token={{token}}&filter[include]=mappedReportFields
                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {

                        angular.forEach(response.data, function (val, key) {
                            $scope.ProcessedAllRowsObj = {};

                            angular.forEach(val.mappedReportFields, function (val, key) {
                                ////console.log(val.fieldName,val.value);

                                var fieldNames;
                                var filedValues;
                                fieldNames = val.fieldName;
                                filedValues = val.value;
                                $scope.ProcessedAllRowsObj[fieldNames] = filedValues;

                            });
                            $scope.ProcessedAllRows.push($scope.ProcessedAllRowsObj);
                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.gridOptionsAllProcessedRows.data = $scope.ProcessedAllRows;


                        $loading.finish('ProcessAllTab');
                    } else {
                        $loading.finish('ProcessAllTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });
                /*End of API*/
            }

        },

        previousPage: function () {
            if (this.pageNumber > 0) {
                this.pageNumber--;
                $scope.AllProcessedpresentpage--;
                this.skip = this.limit * this.pageNumber;
                $NexttoIndex = $scope.AllProcessedPagination.fromIndex - this.limit;
                if ($scope.AllProcessedPagination.toIndex == $scope.AllProcessedtotalcount) {
                    $scope.AllProcessedPagination.toIndex = $scope.AllProcessedPagination.fromIndex - 1;
                } else {
                    $scope.AllProcessedPagination.toIndex = $NexttoIndex;
                }
                $scope.AllProcessedPagination.fromIndex = $scope.AllProcessedPagination.fromIndex - this.limit;
                /*Start of API*/

                $scope.ProcessedAllRows = [];
                $loading.start('ProcessAllTab');
                //http://dev.yogamihi.com/api/ReportRows?filter[where][reportId]=56dd1674f3a04e5d59c57129&access_token={{token}}&filter[include]=mappedReportFields
                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {

                        angular.forEach(response.data, function (val, key) {
                            $scope.ProcessedAllRowsObj = {};

                            angular.forEach(val.mappedReportFields, function (val, key) {
                                ////console.log(val.fieldName,val.value);

                                var fieldNames;
                                var filedValues;
                                fieldNames = val.fieldName;
                                filedValues = val.value;
                                $scope.ProcessedAllRowsObj[fieldNames] = filedValues;

                            });
                            $scope.ProcessedAllRows.push($scope.ProcessedAllRowsObj);
                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.gridOptionsAllProcessedRows.data = $scope.ProcessedAllRows;


                        $loading.finish('ProcessAllTab');
                    } else {
                        $loading.finish('ProcessAllTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });
                /*End of API*/
            }
        }
    };
    	/*pagination for ALL tab processed ends here*/

    $scope.columnSizeChanged = function(colDef,deltaChange){
    	var columnDetails = angular.toJson(colDef);
    	console.log(deltaChange);
    	console.log(colDef);
    	console.log(colDef.field);
    };
    $scope.columnSizeChanged = function(colDef,deltaChange){
    	$scope.ReportWidthChanged = "changed";
    	var columnDetails = angular.toJson(colDef);
    	console.log(deltaChange);
    	console.log(colDef);
    	console.log(colDef.field);
    	var changedWidth;
    	if ($scope.processedRowObject.hasOwnProperty(colDef.field)) {
    		changedWidth = $scope.processedRowObject[colDef.field]+deltaChange;
    		$scope.processedRowObject[colDef.field] = changedWidth;
    		changedWidth = 0;
    		console.log($scope.processedRowObject[colDef.field]);
    		console.log($scope.processedRowObject);
        }else{
        	changedWidth = colDef.width+deltaChange;
        	var keyElement = colDef.field;
        	$scope.newProcessedRowObject[keyElement] = changedWidth;
        	console.log($scope.newProcessedRowObject);
        }
    };
    $scope.saveChangedReportWidth = function(){
    	console.log($scope.reportDetailWidths[0]);
    	console.log($scope.newProcessedRowObject);
    	if($scope.newProcessedRowObject.username == $rootScope.userInfo.username){
    		$http({
				method : 'PUT',
				url : "http://0.0.0.0:3000/api/ReportTables",
				data : $scope.newProcessedRowObject,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).then(function(response) {
				console.log("Data saved as existing user");
			}, function(err) {
				alert(angular.toJson(err));
			})
    	}else{
    		var prop1 = "id";
        	var prop2 = "username";
        	delete $scope.newProcessedRowObject[prop1];
    		$scope.newProcessedRowObject[prop2] = $rootScope.userInfo.username;
    		console.log($scope.newProcessedRowObject);
    		$http({
				method : 'POST',
				url : "http://0.0.0.0:3000/api/ReportTables",
				data : $scope.newProcessedRowObject,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).then(function(response) {
				console.log("Data saved as new user");
			}, function(err) {
				alert(angular.toJson(err));
			})
    	}
    }
    $scope.filterProcessedNewGrid = {
        //enableSelectAll: false,
        //multiSelect: true,
        //rowTemplate: "<div ng-dblclick=\"onDblClick(row)\" ng-repeat=\"(colRenderIndex, col) in colContainer.renderedColumns track by col.colDef.name\" class=\"ui-grid-cell\" ng-class=\"{ 'ui-grid-row-header-cell': col.isRowHeader }\" ui-grid-cell dbl-click-row></div>",
        enableGridMenu: true,
        onRegisterApi: function (gridApi) { //register grid data first within the gridOptions
            $scope.gridApi = gridApi;

        },
    };


    $scope.ProcessedNewGridselectAllValue = undefined;
    $scope.ProcessedNewGridselectAll = function () {
        $scope.gridApi.selection.selectAllRows();
        $scope.selectedProcessedNewGrid = $scope.gridApi.selection.getSelectedRows();
    	$scope.ProcessedNewGridselectAllValue = true;
        $scope.enrichshowingAllSelected = true;
        $scope.enrichshowingAllSelectionoption = false;
    };

    $scope.ProcessedNewGridclearAll = function () {
    	$scope.gridApi.selection.clearSelectedRows();
    	$scope.ProcessedNewGridselectAllValue = false;
        $scope.enrichshowingAllSelected = false;
        $scope.enrichshowingAllSelectionoption = false;
    };

    $scope.filterProcessedNewGrid.onRegisterApi = function (gridApi) {

        $scope.gridApi = gridApi;
        

        //console.log($scope.gridApi.selection.getSelectedRows());

        var val = gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.selectedProcessedNewGrid = $scope.gridApi.selection.getSelectedRows();
            console.log($scope.selectedProcessedNewGrid);
            if ($scope.selectedProcessedNewGrid != 0) {

                $scope.enrichReprocessBtn = true;


            } else {
                $scope.enrichReprocessBtn = false;

            }
            ////console.log(row.entity);

        });
        gridApi.selection.on.rowSelectionChangedBatch($scope, function () {
            $scope.selectedProcessedNewGrid = $scope.gridApi.selection.getSelectedRows();
            console.log($scope.selectedProcessedNewGrid);
            //console.log(gridApi.selection.getSelectAllState);
            if ($scope.selectedProcessedNewGrid.length == 20) {
                $scope.enrichshowingAllSelectionoption = true;
            } else {
                $scope.enrichshowingAllSelectionoption = false;
            }

            ////console.log($scope.gridApi.selection.getSelectedRows()); 
        });


         gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
      
            //$scope.toolTipOldVal = oldValue;
            //$scope.toolTipNewVal = newValue;

            $scope.ChangeUpdateCellValue = {};
            var cellFieldId = colDef['field'] + '_id';
            var fkId = rowEntity[cellFieldId];
            $scope.ChangeUpdateCellValue["value"] = newValue;
      

            $loading.start("ProcessNewTab");

            ReportService.NewTabCellEdit("ReportRows/" + rowEntity.id + "/mappedReportFields/" + fkId, $scope.ChangeUpdateCellValue, function (response) {
            
                if (response.status == 200) {
                    $scope.ChangeUpdateCellValue = {};
                    $loading.finish("ProcessNewTab");
                    toaster.pop("success", "Update  successfully");

                } else {
                    $scope.ChangeUpdateCellValue = {};
                    $loading.finish("ProcessNewTab");
                    toaster.pop("error", "Action failed !,Please try again.");
                }
            });
        });


    };
    $scope.gridOptionsNewProcessedRows = {};
    $scope.filterProcessedUpdateGrid = {
        enableSelectAll: false,
        enableGridMenu: true,
        onRegisterApi: function (gridApi) { //register grid data first within the gridOptions
            $scope.gridApi = gridApi;

        },
    };

    /* newEnrichReprocess start here */
    $scope.newEnrichReprocess = function () {

        //console.log("newEnrichReprocess");
        $scope.enrichNewObj = {};
        //$scope.enrichNewObj.status = 're-process';
        $loading.start("ProcessNewTab");
        //  http://dev.yogamihi.com/api/Reports/56f1bae61f80c3e5403d1b43?access_token={{token}}
        ReportService.ReportStatusUpdate("Reports/" + $scope.reportInfoObj.id, $scope.enrichNewObj, function (response) {
            if (response.status == 200) {
                //console.log(response);
                var loopCount = 0;
                var callCount = $scope.selectedProcessedNewGrid.length;
                angular.forEach($scope.selectedProcessedNewGrid, function (val, key) {

                    //console.log($scope.selectedProcessedNewGrid[0].id);
                    $scope.enrichRowDataObj = {};
                    $scope.enrichRowDataObj.status = 're-process';
                    ReportService.GetReportRowsById("ReportRows/" + val.id, $scope.enrichRowDataObj, function (response) {
                        if (response.status == 200) {
                            loopCount++;
                           
                            if (loopCount == callCount) {
                                $loading.finish("ProcessNewTab");
                                $scope.gridApi.selection.clearSelectedRows();
                                toaster.pop("success", "Report has been Successfully re-processed");
                            }
                            //console.log(response);
                        }
                    });
                });

            } else {
                $scope.enrichNewObj = {};
                toaster.pop("error", response.data.error.message);
            }
        });

    };
    /* newEnrichReprocess ends here */

    $scope.filterProcessedUpdateGrid.onRegisterApi = function (gridApi) {


        $scope.gridApi = gridApi;
        var val = gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.selectedProcessedNeedUpdateRow = $scope.gridApi.selection.getSelectedRows();

            $scope.modalInstance = $uibModal.open({

                templateUrl: 'NeedUpdateRowModalContent.html',
                controller: 'NeedUpdateRowModalController',
                scope: $scope,
                size: 'lg',
            });

        });

        $scope.multipleCellEdit = [];
        gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
            //console.log(rowEntity, colDef);
            $scope.toolTipOldVal = oldValue;
            $scope.toolTipNewVal = newValue;

            /* $scope.ProcessedUpdateRowsHeaderObj['cellTooltip']=function (row, col) {
                     return 'Before: ' + oldValue + "," +'After: ' + newValue;
                     }*/
            ////console.log(rowEntity);
            //var cellFieldId = colDef['field'] + '_id';
            /*//console.log(colDef['field']);
            //console.log(newValue);
            //console.log(oldValue);
            //console.log(rowEntity[cellFieldId]);*/
            /* $scope.multipleCellEditObj = {}
             $scope.multipleCellEditObj['value'] = newValue;
             $scope.multipleCellEditObj['id'] = rowEntity[cellFieldId];
             $scope.multipleCellEdit.push($scope.multipleCellEditObj);*/

            /*alert('Column: ' + colDef.field + ' ID: ' + rowEntity.id + ' Name: ' + rowEntity.name + ' Age: ' + rowEntity.age);*/
            $scope.ChangeUpdateCellValue = {};
            var cellFieldId = colDef['field'] + '_id';
            var fkId = rowEntity[cellFieldId];
            $scope.ChangeUpdateCellValue["value"] = newValue;
            ////console.log(newValue, oldValue);

            $loading.start("ProcessUpdateTab");

            ReportService.NeedUpdateTabCellEdit("ReportRows/" + rowEntity.id + "/mappedReportFields/" + fkId, $scope.ChangeUpdateCellValue, function (response) {
                //console.log(response);
                if (response.status == 200) {
                    $scope.ChangeUpdateCellValue = {};
                    $loading.finish("ProcessUpdateTab");
                    toaster.pop("success", "Update  successfully");

                } else {
                    $scope.ChangeUpdateCellValue = {};
                    $loading.finish("ProcessUpdateTab");
                    toaster.pop("error", "Action faild !,Please try again.");
                }
            });
        });

        /* gridApi.rowEdit.on.saveRow($scope, $scope.UpdateAfterRowEdit);

        gridApi.edit.on.afterCellEdit($scope, function(rowEntity, colDef, newValue, oldValue) {
            //console.log(newValue, oldValue);
        });
*/
    };
    //PUT /ReportRows/{id}/rawReportFields/{fk}

    $scope.UpdateAfterRowEdit = function (rowEntity) {
        //console.log(rowEntity);
        /* ReportService.UpdateCellEdit("ReportRows/"+rowEntity.id+"/rawReportFields/"+rowEntity.fkId, rowEntity, function(response) {
            
        });*/

    }
    $scope.gridOptionsUpdateProcessedRows = {};
    $scope.filterProcessedMatchedGrid = {
        enableSelectAll: false,
        multiSelect: false,
        enableGridMenu: true,
    };
    $scope.gridOptionsMatchedProcessedRows = {
        enableGridMenu: true,
    };

    /*grid options for processed duplicates*/
    $scope.filterProcessedDuplicatesGrid = {
        enableSelectAll: false,
        multiSelect: false,
        enableGridMenu: true,
        onRegisterApi: function (gridApi) { //register grid data first within the gridOptions
            $scope.gridApi = gridApi;
        },

    };

    $scope.filterProcessedMatchedGrid.onRegisterApi = function (gridApi) {
        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.selectedProcessedMatchedRow = $scope.gridApi.selection.getSelectedRows();

            //console.log($scope.selectedProcessedMatchedRow);
            $scope.modalInstance = $uibModal.open({

                templateUrl: 'MatchedRowModalContent.html',
                controller: 'MatchedRowModalController',
                scope: $scope,
                size: 'lg',
            });

        });

    };

    $scope.filterProcessedDuplicatesGrid.onRegisterApi = function (gridApi) {

        $scope.gridApi = gridApi;
        gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.selectedProcessedDuplicateRow = $scope.gridApi.selection.getSelectedRows();

            //console.log($scope.selectedProcessedDuplicateRow);

            //console.log($scope.ProcessedAllDuplicateRows);

            $scope.modalInstance = $uibModal.open({

                templateUrl: 'DuplicateRowModalContent.html',
                controller: 'DuplicateRowModalController',
                scope: $scope,
                size: 'lg',
            });

        });


    };

    /*grid options for processed duplicates end here*/
    $scope.gridOptionsDuplicatesProcessedRows = {
        enableGridMenu: true,

    };
    $scope.gridOptionsExceptionsProcessedRows = {

    };
    $scope.filterProcessedExceptionsGrid = {
        enableSelectAll: true,
        enableGridMenu: true,

        onRegisterApi: function (gridApi) { //register grid data first within the gridOptions
            $scope.gridApi = gridApi;

        },
    };
    $scope.filterProcessedExceptionsGrid.onRegisterApi = function (gridApi) {


        $scope.gridApi = gridApi;
        var val = gridApi.selection.on.rowSelectionChanged($scope, function (row) {
            $scope.selectedProcessedExceptionsGrid = $scope.gridApi.selection.getSelectedRows();
            //console.log($scope.selectedProcessedExceptionsGrid);

        });

        gridApi.selection.on.rowSelectionChangedBatch($scope, function () {

            console.log(gridApi.selection.getSelectAllState);
            if ($scope.gridApi.selection.getSelectedRows().length != 0) {
            } else {
            }
            ////console.log($scope.gridApi.selection.getSelectedRows()); 
        });


        $scope.ExceptionMultipleCellEdit = [];
        /*gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {
            ////console.log(rowEntity);
            var cellFieldId = colDef['field'] + '_id';
            //console.log(colDef['field']);
            //console.log(newValue);
            //console.log(oldValue);
            //console.log(rowEntity[cellFieldId]);
            $scope.ExceptionMultipleCellEditObj = {}
            $scope.ExceptionMultipleCellEditObj['value'] = newValue;
            $scope.ExceptionMultipleCellEditObj['id'] = rowEntity[cellFieldId];
            $scope.ExceptionMultipleCellEdit.push($scope.ExceptionMultipleCellEditObj);
            //console.log($scope.ExceptionMultipleCellEdit);


            //alert('Column: ' + colDef.field + ' ID: ' + rowEntity.id + ' Name: ' + rowEntity.name + ' Age: ' + rowEntity.age);
        });
*/
        //gridApi.rowEdit.on.saveRow($scope, $scope.ExceptionTabUpdateAfterRowEdit);

        gridApi.edit.on.afterCellEdit($scope, function (rowEntity, colDef, newValue, oldValue) {

            $scope.ChangeCellValue = {};
            var cellFieldId = colDef['field'] + '_id';
            var fkId = rowEntity[cellFieldId];
            $scope.ChangeCellValue["value"] = newValue;
            ////console.log(newValue, oldValue);

            $loading.start("ProcessExceptionsTab");

            ReportService.ExceptionTabCellEdit("ReportRows/" + rowEntity.id + "/mappedReportFields/" + fkId, $scope.ChangeCellValue, function (response) {
                //console.log(response);
                if (response.status == 200) {
                    $scope.ChangeCellValue = {};
                    $loading.finish("ProcessExceptionsTab");
                    toaster.pop("success", "Update  successfully");

                } else {
                    $scope.ChangeCellValue = {};
                    $loading.finish("ProcessExceptionsTab");
                    toaster.pop("error", response.data.error.message);

                }
            });

        });

    };

    $scope.ExceptionTabUpdateAfterRowEdit = function (rowEntity) {
        //console.log(rowEntity);
        /* ReportService.UpdateCellEdit("ReportRows/"+rowEntity.id+"/rawReportFields/"+rowEntity.fkId, rowEntity, function(response) {
           
        });*/

    }

    $scope.filterProcessedNonPrimaryGrid = {
        enableGridMenu: true,
    };
    $scope.gridOptionsNonPrimaryProcessedRows = {

    };

    /* API starts here all */
    $scope.ProcessedAllRowsAPI = function () {
        $scope.ProcessedAllRows = [];
        $loading.start('ProcessAllTab');
        var allprocessedlimit;
        allprocessedlimit = localStorage.getItem(localStorage.id + "processedAllPage");
        if (!allprocessedlimit) {
            allprocessedlimit = 20;
        }
        console.log($rootScope.reportId+"11"+allprocessedlimit);
        //http://dev.yogamihi.com/api/ReportRows?filter[where][reportId]=56dd1674f3a04e5d59c57129&access_token={{token}}&filter[include]=mappedReportFields
        ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[limit]=" + allprocessedlimit + "&filter[skip]=0", function (response) {
            if (response.status == 200) {
            	console.log(response.data);
            	console.log(response.data);
                angular.forEach(response.data, function (val, key) {
                    $scope.ProcessedAllRowsObj = {};

                    angular.forEach(val.mappedReportFields, function (val, key) {
                        ////console.log(val.fieldName,val.value);

                        var fieldNames;
                        var filedValues;
                        fieldNames = val.fieldName;
                        filedValues = val.value;
                        $scope.ProcessedAllRowsObj[fieldNames] = filedValues;

                    });
                    $scope.ProcessedAllRows.push($scope.ProcessedAllRowsObj);
                });
                //Making Json Objects for UI grid for Raw Report Rows end Here

                $scope.gridOptionsAllProcessedRows.data = $scope.ProcessedAllRows;
                var columnWidth = 0;
                $scope.gridOptionsAllProcessedRowsHeaderArr = $scope.ProcessedAllRows[0];
                $scope.gridOptionsAllProcessedRows.columnDefs = [];
                for (elem in $scope.gridOptionsAllProcessedRowsHeaderArr) {
                    $scope.gridOptionsAllProcessedRowsHeaderObj = {};
                    $scope.gridOptionsAllProcessedRowsHeaderObj['field'] = elem;
                    $scope.processedRowObject = $scope.reportDetailWidths[0];
                    $scope.newProcessedRowObject = $scope.processedRowObject; 
                    if ($scope.processedRowObject.hasOwnProperty(elem)) {
                		columnWidth = $scope.processedRowObject[elem];
                		console.log(columnWidth);
                    }else{
                    	columnWidth = 150;
                    }
                    $scope.gridOptionsAllProcessedRowsHeaderObj['width'] = columnWidth;
                    $scope.gridOptionsAllProcessedRowsHeaderObj['cellTooltip'] = true;
                    $scope.gridOptionsAllProcessedRowsHeaderObj['headerTooltip'] = true;
                    //$scope.NewProcessedRowsHeaderObj['cellClass'] = 'action-class';
                    $scope.gridOptionsAllProcessedRows.columnDefs.push($scope.gridOptionsAllProcessedRowsHeaderObj);

                }

                $loading.finish('ProcessAllTab');
            } else {
                $loading.finish('ProcessAllTab');
                toaster.pop("error", response.data.error.message);
            }

        });
    };
    $scope.ProcessedAllRowsAPI();
    /* API ends here all */


    /*API Call for Geeting required fiels for duplicate match columns ends here*/

    /* API starts here new */
    $scope.ProcessedNewRowsAPI = function () {
        $scope.ProcessedNewRows = [];
        $scope.ProcessedNewRowsFieldData = [];
        $loading.start('ProcessNewTab');

        var newprocessedlimit;
        newprocessedlimit = localStorage.getItem(localStorage.id + "processedNewPage");
        if (!newprocessedlimit) {
            newprocessedlimit = 20;
        }

        ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][status]=New&filter[limit]=" + newprocessedlimit + "&filter[skip]=0", function (response) {
            if (response.status == 200) {
                $scope.enrichshowingAllSelection = false;
                //console.log(response.data);

                angular.forEach(response.data, function (val, key) {
                    if (!val.exception) {

                        $scope.ProcessedNewRowsObj = {};
                        $scope.ProcessedNewRowsFieldDataObj = {};
                        $scope.ProcessedNewRowsObj.facilityAccountId = val.facilityAccountId;
                        $scope.ProcessedNewRowsObj.id = val.id;
                        $scope.ProcessedNewRowsObj.reportId = val.reportId;
                        $scope.ProcessedNewRowsObj.row = val.row;
                        $scope.ProcessedNewRowsObj.individualType = val.individualType;
                        $scope.ProcessedNewRowsObj.individualSubType = val.individualSubType;
                        $scope.ProcessedNewRowsObj.memberType = val.memberType;
                        $scope.ProcessedNewRowsObj.memberSubType = val.memberSubType;

                        $scope.ProcessedNewRowsFieldDataObj.individualType=val.individualType;
                        $scope.ProcessedNewRowsFieldDataObj.individualSubType=val.individualSubType;
                        $scope.ProcessedNewRowsFieldDataObj.memberType=val.memberType;
                        $scope.ProcessedNewRowsFieldDataObj.memberSubType=val.memberSubType

                        angular.forEach(val.mappedReportFields, function (val, key) {
                         
                            var fieldNames;
                            var filedValues;


                            fieldNames = val.fieldName;
                            filedValues = val.value;
                             var field_id = fieldNames + '_id';
                             $scope.ProcessedNewRowsObj[field_id] = val.id;

                            $scope.ProcessedNewRowsObj[fieldNames] = filedValues;


                            $scope.ProcessedNewRowsFieldDataObj[fieldNames] = filedValues;

                        });
                        $scope.ProcessedNewRows.push($scope.ProcessedNewRowsObj);

                    }
                $scope.ProcessedNewRowsFieldData.push($scope.ProcessedNewRowsFieldDataObj);
                });
                //Making Json Objects for UI grid for Raw Report Rows end Here
                //console.log($scope.ProcessedNewRows);
                $scope.gridOptionsNewProcessedRows.data = $scope.ProcessedNewRows;
                $scope.filterProcessedNewGrid.data = $scope.ProcessedNewRows;
                $scope.NewProcessedRowsHeaderArr = $scope.ProcessedNewRowsFieldData[0];
                $scope.filterProcessedNewGrid.columnDefs = [];
                for (elem in $scope.NewProcessedRowsHeaderArr) {
                    $scope.NewProcessedRowsHeaderObj = {};

                    $scope.NewProcessedRowsHeaderObj['field'] = elem;
                    $scope.NewProcessedRowsHeaderObj['width'] = 150;
                    $scope.NewProcessedRowsHeaderObj['cellTooltip'] = true;
                    $scope.NewProcessedRowsHeaderObj['headerTooltip'] = true;
                    //$scope.NewProcessedRowsHeaderObj['cellClass'] = 'action-class';

                    $scope.filterProcessedNewGrid.columnDefs.push($scope.NewProcessedRowsHeaderObj);

                }

                $loading.finish('ProcessNewTab');
            } else {
                $loading.finish('ProcessNewTab');
                toaster.pop("error", response.data.error.message);
            }

        });
    };
    /* API ends here new */


    /*Processed new tab pagination*/
    $scope.ProcessedNewPagination = {};

    $scope.newProcessedlimit = 20;

    $scope.newProcessedlimit = localStorage.getItem(localStorage.id + "processedNewPage");
    if (!$scope.newProcessedlimit) {
        $scope.newProcessedlimit = 20;
    }
    $scope.ProcessedNewPageLimitValue = localStorage.getItem(localStorage.id + "processedNewPage");

    $scope.pageNumber = 0;
    $scope.ProcessedNewpresentpage = 1;

    ReportService.ProcessedRowCount("ReportRows/filterStatus?reportId=" + $rootScope.reportId + "&status=New&count=1", function (response) {
        if (response.status == 200) {
            $scope.ProcessedNewtotalcount = response.data.count;

            if ($scope.ProcessedNewtotalcount < $scope.newProcessedlimit) {
                $scope.ProcessedNewPagination.toIndex = $scope.ProcessedNewtotalcount;
            } else {
                $scope.ProcessedNewPagination.toIndex = $scope.newProcessedlimit;
            }
            $scope.ProcessedNewtotalpages = Math.ceil($scope.ProcessedNewtotalcount / $scope.newProcessedlimit);
        }
    });
    $scope.ProcessedNewPagination = {
        //limit:10,
        limit: $scope.newProcessedlimit,
        skip: 0,
        //pageNumber: 0,
        fromIndex: 1,
        //toIndex: $scope.newProcessedlimit,
        pageNumber: 0,
        loadSelCountPerPage: function (pageLimitValue) {
            localStorage.setItem(localStorage.id + "processedNewPage", pageLimitValue);
            this.pageNumber = 0;
            this.limit = pageLimitValue;
            this.skip = 0;
            $scope.ProcessedNewpresentpage = 1;
            $scope.ProcessedNewtotalpages = Math.ceil($scope.ProcessedNewtotalcount / pageLimitValue);
            $scope.ProcessedNewPagination.fromIndex = this.skip + 1;
            //$scope.ProcessedNewPagination.toIndex = this.skip + Number(this.limit);
            if ($scope.ProcessedNewtotalcount < pageLimitValue) {
                $scope.ProcessedNewPagination.toIndex = $scope.ProcessedNewtotalcount;
            } else {
                $scope.ProcessedNewPagination.toIndex = this.skip + Number(this.limit);
            }

            /*Start of API*/
            $scope.ProcessedNewRows = [];
            $scope.ProcessedNewRowsFieldData = [];
            $loading.start('ProcessNewTab');

            ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][status]=New&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                if (response.status == 200) {
                    //console.log(response.data);

                    angular.forEach(response.data, function (val, key) {
                        if (!val.exception) {

                            $scope.ProcessedNewRowsObj = {};
                            $scope.ProcessedNewRowsFieldDataObj = {};
                            $scope.ProcessedNewRowsObj.facilityAccountId = val.facilityAccountId;
                            $scope.ProcessedNewRowsObj.id = val.id;
                            $scope.ProcessedNewRowsObj.reportId = val.reportId;
                            $scope.ProcessedNewRowsObj.row = val.row;
                            $scope.ProcessedNewRowsObj.individualType = val.individualType;
                            $scope.ProcessedNewRowsObj.individualSubType = val.individualSubType;
                            $scope.ProcessedNewRowsObj.memberType = val.memberType;
                            $scope.ProcessedNewRowsObj.memberSubType = val.memberSubType;

                            angular.forEach(val.mappedReportFields, function (val, key) {
                                ////console.log(val.id);
                                ////console.log(val.fieldName,val.value);
                                var fieldNames;
                                var filedValues;


                                fieldNames = val.fieldName;
                                filedValues = val.value;

                                $scope.ProcessedNewRowsObj[fieldNames] = filedValues;

                                //$scope.ProcessedNewRowsFieldDataObj[fieldNames] = filedValues;

                            });
                            $scope.ProcessedNewRows.push($scope.ProcessedNewRowsObj);

                        }
                        //$scope.ProcessedNewRowsFieldData.push($scope.ProcessedNewRowsFieldDataObj);
                    });
                    //Making Json Objects for UI grid for Raw Report Rows end Here
                    //console.log($scope.ProcessedNewRows);
                    $scope.gridOptionsNewProcessedRows.data = $scope.ProcessedNewRows;
                    $scope.filterProcessedNewGrid.data = $scope.ProcessedNewRows;
                    $scope.NewProcessedRowsHeaderArr = $scope.ProcessedNewRows[0];
                    $scope.filterProcessedNewGrid.columnDefs = [];
                    for (elem in $scope.NewProcessedRowsHeaderArr) {
                        $scope.NewProcessedRowsHeaderObj = {};
                        if ((elem == "facilityAccountId") || (elem == "id") || (elem == "row") || (elem == "reportId")) {
                            continue;
                        }

                        $scope.NewProcessedRowsHeaderObj['field'] = elem;
                        $scope.NewProcessedRowsHeaderObj['width'] = 150;
                        $scope.NewProcessedRowsHeaderObj['cellTooltip'] = true;
                        $scope.NewProcessedRowsHeaderObj['headerTooltip'] = true;

                        $scope.filterProcessedNewGrid.columnDefs.push($scope.NewProcessedRowsHeaderObj);
                    }

                    $loading.finish('ProcessNewTab');
                } else {
                    $loading.finish('ProcessNewTab');
                    toaster.pop("error", response.data.error.message);
                }

            });

            /*End of API*/
        },
        nextPage: function () {
            //console.log("next");
            if ($scope.ProcessedNewpresentpage != $scope.ProcessedNewtotalpages) {
                this.pageNumber++;
                $scope.ProcessedNewpresentpage++;
                this.skip = this.limit * this.pageNumber;
                $scope.ProcessedNewPagination.fromIndex = this.skip + 1;
                $nextMaxtoindex = Number(this.skip) + Number(this.limit);
                if ($scope.Reportstotalcount < $nextMaxtoindex) {
                    $scope.ProcessedNewPagination.toIndex = $scope.ProcessedNewtotalcount;
                } else {
                    $scope.ProcessedNewPagination.toIndex = $nextMaxtoindex;
                }
                /*Start of API*/
                $scope.ProcessedNewRows = [];
                $scope.ProcessedNewRowsFieldData = [];
                $loading.start('ProcessNewTab');

                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][status]=New&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {
                        //console.log(response.data);

                        angular.forEach(response.data, function (val, key) {
                            if (!val.exception) {

                                $scope.ProcessedNewRowsObj = {};
                                $scope.ProcessedNewRowsFieldDataObj = {};
                                $scope.ProcessedNewRowsObj.facilityAccountId = val.facilityAccountId;
                                $scope.ProcessedNewRowsObj.id = val.id;
                                $scope.ProcessedNewRowsObj.reportId = val.reportId;
                                $scope.ProcessedNewRowsObj.row = val.row;
                                $scope.ProcessedNewRowsObj.individualType = val.individualType;
                                $scope.ProcessedNewRowsObj.individualSubType = val.individualSubType;
                                $scope.ProcessedNewRowsObj.memberType = val.memberType;
                                $scope.ProcessedNewRowsObj.memberSubType = val.memberSubType;

                                angular.forEach(val.mappedReportFields, function (val, key) {
                                    ////console.log(val.id);
                                    ////console.log(val.fieldName,val.value);
                                    var fieldNames;
                                    var filedValues;


                                    fieldNames = val.fieldName;
                                    filedValues = val.value;

                                    $scope.ProcessedNewRowsObj[fieldNames] = filedValues;

                                    //$scope.ProcessedNewRowsFieldDataObj[fieldNames] = filedValues;

                                });
                                $scope.ProcessedNewRows.push($scope.ProcessedNewRowsObj);

                            }
                            //$scope.ProcessedNewRowsFieldData.push($scope.ProcessedNewRowsFieldDataObj);
                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here
                        //console.log($scope.ProcessedNewRows);
                        $scope.gridOptionsNewProcessedRows.data = $scope.ProcessedNewRows;
                        $scope.filterProcessedNewGrid.data = $scope.ProcessedNewRows;
                        $scope.NewProcessedRowsHeaderArr = $scope.ProcessedNewRows[0];
                        $scope.filterProcessedNewGrid.columnDefs = [];
                        for (elem in $scope.NewProcessedRowsHeaderArr) {
                            $scope.NewProcessedRowsHeaderObj = {};
                            if ((elem == "facilityAccountId") || (elem == "id") || (elem == "row") || (elem == "reportId")) {
                                continue;
                            }

                            $scope.NewProcessedRowsHeaderObj['field'] = elem;
                            $scope.NewProcessedRowsHeaderObj['width'] = 150;
                            $scope.NewProcessedRowsHeaderObj['cellTooltip'] = true;
                            $scope.NewProcessedRowsHeaderObj['headerTooltip'] = true;

                            $scope.filterProcessedNewGrid.columnDefs.push($scope.NewProcessedRowsHeaderObj);
                        }

                        $loading.finish('ProcessNewTab');
                    } else {
                        $loading.finish('ProcessNewTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });

                /*End of API*/
            }

        },

        previousPage: function () {
            if (this.pageNumber > 0) {
                this.pageNumber--;
                $scope.ProcessedNewpresentpage--;
                this.skip = this.limit * this.pageNumber;
                $NexttoIndex = $scope.ProcessedNewPagination.toIndex - this.limit;
                if ($scope.ProcessedNewPagination.toIndex == $scope.ProcessedNewtotalcount) {
                    $scope.ProcessedNewPagination.toIndex = $scope.ProcessedNewPagination.fromIndex - 1;
                } else {
                    $scope.ProcessedNewPagination.toIndex = $NexttoIndex;
                }
                $scope.ProcessedNewPagination.fromIndex = $scope.ProcessedNewPagination.fromIndex - this.limit;

                /*Start of API*/
                $scope.ProcessedNewRows = [];
                $scope.ProcessedNewRowsFieldData = [];
                $loading.start('ProcessNewTab');

                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][status]=New&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {
                        //console.log(response.data);

                        angular.forEach(response.data, function (val, key) {
                            if (!val.exception) {

                                $scope.ProcessedNewRowsObj = {};
                                $scope.ProcessedNewRowsFieldDataObj = {};
                                $scope.ProcessedNewRowsObj.facilityAccountId = val.facilityAccountId;
                                $scope.ProcessedNewRowsObj.id = val.id;
                                $scope.ProcessedNewRowsObj.reportId = val.reportId;
                                $scope.ProcessedNewRowsObj.row = val.row;
                                $scope.ProcessedNewRowsObj.individualType = val.individualType;
                                $scope.ProcessedNewRowsObj.individualSubType = val.individualSubType;
                                $scope.ProcessedNewRowsObj.memberType = val.memberType;
                                $scope.ProcessedNewRowsObj.memberSubType = val.memberSubType;

                                angular.forEach(val.mappedReportFields, function (val, key) {
                                    ////console.log(val.id);
                                    ////console.log(val.fieldName,val.value);
                                    var fieldNames;
                                    var filedValues;


                                    fieldNames = val.fieldName;
                                    filedValues = val.value;

                                    $scope.ProcessedNewRowsObj[fieldNames] = filedValues;


                                    //$scope.ProcessedNewRowsFieldDataObj[fieldNames] = filedValues;

                                });
                                $scope.ProcessedNewRows.push($scope.ProcessedNewRowsObj);

                            }
                            //$scope.ProcessedNewRowsFieldData.push($scope.ProcessedNewRowsFieldDataObj);
                        });

                        //console.log($scope.ProcessedNewRows);
                        $scope.gridOptionsNewProcessedRows.data = $scope.ProcessedNewRows;
                        $scope.filterProcessedNewGrid.data = $scope.ProcessedNewRows;
                        $scope.NewProcessedRowsHeaderArr = $scope.ProcessedNewRows[0];
                        $scope.filterProcessedNewGrid.columnDefs = [];
                        for (elem in $scope.NewProcessedRowsHeaderArr) {
                            $scope.NewProcessedRowsHeaderObj = {};
                            if ((elem == "facilityAccountId") || (elem == "id") || (elem == "row") || (elem == "reportId")) {
                                continue;
                            }

                            $scope.NewProcessedRowsHeaderObj['field'] = elem;
                            $scope.NewProcessedRowsHeaderObj['width'] = 150;
                            $scope.NewProcessedRowsHeaderObj['cellTooltip'] = true;
                            $scope.NewProcessedRowsHeaderObj['headerTooltip'] = true;

                            $scope.filterProcessedNewGrid.columnDefs.push($scope.NewProcessedRowsHeaderObj);
                        }

                        $loading.finish('ProcessNewTab');
                    } else {
                        $loading.finish('ProcessNewTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });

                /*End of API*/
            }
        }
    };
    /* API starts here new */

    /* API starts here update */
    $scope.ProcessedUpdateRowsAPI = function () {
        $scope.ProcessedUpdateRows = [];
        $scope.ProcessedUpdateRowsFieldData = [];
        $loading.start('ProcessUpdateTab');

        var updateprocessedlimit;
        updateprocessedlimit = localStorage.getItem(localStorage.id + "processedUpdatePage");
        if (!updateprocessedlimit) {
            updateprocessedlimit = 20;
        }

        ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][status]=Update", function (response) {
            //console.log(response);

            if (response.status == 200) {

                angular.forEach(response.data, function (val, key) {

                    if (!val.exception) {

                        $scope.ProcessedUpdateRowsObj = {};
                        $scope.ProcessedUpdateRowsFieldDataObj = {};
                        $scope.ProcessedUpdateRowsObj.facilityAccountId = val.facilityAccountId;
                        $scope.ProcessedUpdateRowsObj.id = val.id;
                        $scope.ProcessedUpdateRowsObj.reportId = val.reportId;
                        $scope.ProcessedUpdateRowsObj.row = val.row;
                        //console.log(val);

                        angular.forEach(val.mappedReportFields, function (val, key) {
                            //console.log(val);

                            var fieldNames;
                            var filedValues;

                            fieldNames = val.fieldName;
                            filedValues = val.value;
                            var field_id = fieldNames + '_id';
                            $scope.ProcessedUpdateRowsObj[fieldNames] = filedValues;
                            $scope.ProcessedUpdateRowsObj[field_id] = val.id;
                            $scope.ProcessedUpdateRowsFieldDataObj[fieldNames] = filedValues;

                        });
                        $scope.ProcessedUpdateRows.push($scope.ProcessedUpdateRowsObj);
                        $scope.ProcessedUpdateRowsFieldData.push($scope.ProcessedUpdateRowsFieldDataObj);
                    }

                });
                //Making Json Objects for UI grid for Raw Report Rows end Here
                //console.log($scope.ProcessedUpdateRows);
                $scope.gridOptionsUpdateProcessedRows.data = $scope.ProcessedUpdateRows;

                $scope.filterProcessedUpdateGrid.data = $scope.ProcessedUpdateRows;
                $scope.ProcessedUpdateRowsHeaderArr = $scope.ProcessedUpdateRowsFieldData[0];
                $scope.filterProcessedUpdateGrid.columnDefs = [];
                for (elem in $scope.ProcessedUpdateRowsHeaderArr) {
                    $scope.ProcessedUpdateRowsHeaderObj = {};

                    $scope.ProcessedUpdateRowsHeaderObj['field'] = elem;
                    $scope.ProcessedUpdateRowsHeaderObj['width'] = 150;
                    $scope.ProcessedUpdateRowsHeaderObj['enableCellEdit'] = true;
                    //$scope.ProcessedUpdateRowsHeaderObj['cellTooltip'] = true;
                    $scope.ProcessedUpdateRowsHeaderObj['headerTooltip'] = true;

                    /* $scope.ProcessedUpdateRowsHeaderObj['cellTooltip'] = function() {
                             return 'Before: ' + $scope.toolTipOldVal + "," + 'After: ' + $scope.toolTipNewVal;
                         }*/
                    /*else{
                          $scope.ProcessedUpdateRowsHeaderObj['cellTooltip'] = true;
                    }*/

                    $scope.filterProcessedUpdateGrid.columnDefs.push($scope.ProcessedUpdateRowsHeaderObj);

                }


                $loading.finish('ProcessUpdateTab');
            } else {
                $loading.finish('ProcessUpdateTab');
                toaster.pop("error", response.data.error.message);
            }

        });
    };
    /* API ends here update */

    /*Processed update rows pagination*/
    $scope.updateProcessedlimit = 20;

    $scope.updateProcessedlimit = localStorage.getItem(localStorage.id + "processedUpdatePage");
    if (!$scope.updateProcessedlimit) {
        $scope.updateProcessedlimit = 20;
    }
    $scope.ProcessedUpdatePageLimitValue = localStorage.getItem(localStorage.id + "processedUpdatePage");

    $scope.pageNumber = 0;
    $scope.processedupdatepresentpage = 1;


    ReportService.ProcessedRowCount("ReportRows/filterStatus?reportId=" + $rootScope.reportId + "&status=Update&count=1", function (response) {
        if (response.status == 200) {
            $scope.processedupdatetotalcount = response.data.count;
            if ($scope.processedupdatetotalcount < $scope.updateProcessedlimit) {
                $scope.processedupdatePagination.toIndex = $scope.processedupdatetotalcount;
            } else {
                $scope.processedupdatePagination.toIndex = $scope.updateProcessedlimit;
            }
            $scope.processedupdatetotalpages = Math.ceil($scope.processedupdatetotalcount / $scope.updateProcessedlimit);
        }
    });


    $scope.processedupdatePagination = {
        limit: $scope.updateProcessedlimit,
        skip: 0,
        fromIndex: 1,
        //toIndex: $scope.updateProcessedlimit,
        pageNumber: 0,
        loadSelCountPerPage: function (pageLimitValue) {
            localStorage.setItem(localStorage.id + "processedUpdatePage", pageLimitValue);
            this.pageNumber = 0;
            this.limit = pageLimitValue;
            this.skip = 0;
            $scope.processedupdatepresentpage = 1;
            $scope.processedupdatetotalpages = Math.ceil($scope.processedupdatetotalcount / pageLimitValue);
            $scope.processedupdatePagination.fromIndex = this.skip + 1;
            //$scope.processedupdatePagination.toIndex = this.skip + Number(this.limit);
            if ($scope.processedupdatetotalcount < pageLimitValue) {
                $scope.processedupdatePagination.toIndex = $scope.processedupdatetotalcount;
            } else {
                $scope.processedupdatePagination.toIndex = this.skip + Number(this.limit);
            }
            /*API Call For Raw Report Rows Detals  */
            /*API Call For Raw Report Rows Detals end here */

        },
        nextPage: function () {
            if ($scope.processedupdatepresentpage != $scope.processedupdatetotalpages) {
                $scope.processedupdatepresentpage++;
                this.pageNumber++;
                this.skip = Number(this.limit) * Number(this.pageNumber);
                $scope.processedupdatePagination.fromIndex = this.skip + 1;
                $nextMaxtoindex = this.skip + Number(this.limit);
                if ($scope.processedupdatetotalcount < $nextMaxtoindex) {
                    $scope.processedupdatePagination.toIndex = $scope.processedupdatetotalcount;
                } else {
                    $scope.processedupdatePagination.toIndex = $nextMaxtoindex;
                }
                /*API Call For Raw Report Rows Detals  */
                /*API Call For Raw Report Rows Detals end here */
            }
        },

        previousPage: function () {
            if (this.pageNumber > 0) {
                $scope.processedupdatepresentpage--;
                this.pageNumber--;
                this.skip = this.limit * this.pageNumber;
                $NexttoIndex = $scope.processedupdatePagination.toIndex - this.limit;
                if ($scope.processedupdatePagination.toIndex == $scope.processedupdatetotalcount) {
                    $scope.processedupdatePagination.toIndex = $scope.processedupdatePagination.fromIndex - 1;
                } else {
                    $scope.processedupdatePagination.toIndex = $NexttoIndex;
                }
                $scope.processedupdatePagination.fromIndex = $scope.processedupdatePagination.fromIndex - this.limit;
                /*API Call For Raw Report Rows Detals  */
                /*API Call For Raw Report Rows Detals end here */
            }
        }
    };

    /*Processed update rows pagination*/

    /* API starts here matched */
    $scope.ProcessedMatchedRowsAPI = function () {
        $scope.ProcessedMatchedRows = [];
        $scope.ProcessedMatchedFieldRows = [];
        $loading.start('ProcessMatchedTab');

        var matchedprocessedlimit;
        matchedprocessedlimit = localStorage.getItem(localStorage.id + "processedMatchedPage");
        if (!matchedprocessedlimit) {
            matchedprocessedlimit = 20;
        }

        ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][status]=Matched&filter[limit]=" + matchedprocessedlimit + "&filter[skip]=0", function (response) {
            if (response.status == 200) {
                //console.log(response);
                angular.forEach(response.data, function (val, key) {
                    if (!val.exception) {

                        $scope.ProcessedMatchedRowsObj = {};
                        $scope.ProcessedMatchedFieldRowsObj = {};
                        $scope.ProcessedMatchedRowsObj.individualId = val.individualId;

                        angular.forEach(val.mappedReportFields, function (val, key) {
                            ////console.log(val.fieldName,val.value);
                            var fieldNames;
                            var filedValues;
                            fieldNames = val.fieldName;
                            filedValues = val.value;
                            $scope.ProcessedMatchedRowsObj[fieldNames] = filedValues;
                            $scope.ProcessedMatchedFieldRowsObj[fieldNames] = filedValues;

                        });
                        $scope.ProcessedMatchedRows.push($scope.ProcessedMatchedRowsObj);
                        $scope.ProcessedMatchedFieldRows.push($scope.ProcessedMatchedFieldRowsObj);
                    }

                });
                //Making Json Objects for UI grid for Raw Report Rows end Here

                $scope.gridOptionsMatchedProcessedRows.data = $scope.ProcessedMatchedRows;
                $scope.filterProcessedMatchedGrid.data = $scope.ProcessedMatchedRows;


                $scope.ProcessedMatchedRowsHeaderArr = $scope.ProcessedMatchedFieldRows[0];
                $scope.filterProcessedMatchedGrid.columnDefs = [];
                if (response.data) {
                    if (response.data[0].action == "Import") {
                        for (elem in $scope.ProcessedMatchedRowsHeaderArr) {
                            $scope.ProcessedMatchedRowsHeaderObj = {};

                            $scope.ProcessedMatchedRowsHeaderObj['field'] = elem;
                            $scope.ProcessedMatchedRowsHeaderObj['width'] = 150;
                            $scope.ProcessedMatchedRowsHeaderObj['cellTooltip'] = true;
                            $scope.ProcessedMatchedRowsHeaderObj['headerTooltip'] = true;
                            $scope.ProcessedMatchedRowsHeaderObj['cellClass'] = 'action-class';

                            /* $scope.ProcessedUpdateRowsHeaderObj['cellTooltip'] = function() {
                                     return 'Before: ' + $scope.toolTipOldVal + "," + 'After: ' + $scope.toolTipNewVal;
                                 }*/
                            /*else{
                                  $scope.ProcessedUpdateRowsHeaderObj['cellTooltip'] = true;
                            }*/

                            $scope.filterProcessedMatchedGrid.columnDefs.push($scope.ProcessedMatchedRowsHeaderObj);

                        }

                    } else {
                        for (elem in $scope.ProcessedMatchedRowsHeaderArr) {
                            $scope.ProcessedMatchedRowsHeaderObj = {};

                            $scope.ProcessedMatchedRowsHeaderObj['field'] = elem;
                            $scope.ProcessedMatchedRowsHeaderObj['width'] = 150;
                            $scope.ProcessedMatchedRowsHeaderObj['cellTooltip'] = true;
                            $scope.ProcessedMatchedRowsHeaderObj['headerTooltip'] = true;

                            /* $scope.ProcessedUpdateRowsHeaderObj['cellTooltip'] = function() {
                                     return 'Before: ' + $scope.toolTipOldVal + "," + 'After: ' + $scope.toolTipNewVal;
                                 }*/
                            /*else{
                                  $scope.ProcessedUpdateRowsHeaderObj['cellTooltip'] = true;
                            }*/

                            $scope.filterProcessedMatchedGrid.columnDefs.push($scope.ProcessedMatchedRowsHeaderObj);

                        }

                    }
                }

                $loading.finish('ProcessMatchedTab');
            } else {
                $loading.finish('ProcessMatchedTab');
                toaster.pop("error", response.data.error.message);
            }

        });
    };
    /* API ends here matched */

    /*Processed matched rows pagination*/
    $scope.matchedProcessedlimit = 20;

    $scope.matchedProcessedlimit = localStorage.getItem(localStorage.id + "processedMatchedPage");
    if (!$scope.matchedProcessedlimit) {
        $scope.matchedProcessedlimit = 20;
    }
    $scope.ProcessedMatchedPageLimitValue = localStorage.getItem(localStorage.id + "processedMatchedPage");

    $scope.pageNumber = 0;
    $scope.processedmatchedpresentpage = 1;

    ReportService.ProcessedRowCount("ReportRows/filterStatus?reportId=" + $rootScope.reportId + "&status=Matched&count=1", function (response) {
        if (response.status == 200) {

            $scope.processedmatchedtotalcount = response.data.count;
            if ($scope.processedmatchedtotalcount < $scope.matchedProcessedlimit) {
                $scope.processedmatchedPagination.toIndex = $scope.processedmatchedtotalcount;
            } else {
                $scope.processedmatchedPagination.toIndex = $scope.matchedProcessedlimit;
            }
            $scope.processedmatchedtotalpages = Math.ceil($scope.processedmatchedtotalcount / $scope.matchedProcessedlimit);
        }
    });

    $scope.processedmatchedPagination = {
        //limit:10,
        limit: $scope.matchedProcessedlimit,
        skip: 0,
        //pageNumber: 0,
        fromIndex: 1,
        //toIndex: $scope.matchedProcessedlimit,
        pageNumber: 0,
        loadSelCountPerPage: function (pageLimitValue) {
            localStorage.setItem(localStorage.id + "processedMatchedPage", pageLimitValue);
            this.pageNumber = 0;
            this.limit = pageLimitValue;
            this.skip = 0;
            $scope.processedmatchedpresentpage = 1;
            $scope.processedmatchedtotalpages = Math.ceil($scope.processedmatchedtotalcount / pageLimitValue);
            $scope.processedmatchedPagination.fromIndex = this.skip + 1;
            //$scope.processedmatchedPagination.toIndex = this.skip + Number(this.limit);
            if ($scope.processedmatchedtotalcount < pageLimitValue) {
                $scope.processedmatchedPagination.toIndex = $scope.processedmatchedtotalcount;
            } else {
                $scope.processedmatchedPagination.toIndex = this.skip + Number(this.limit);
            }

            /*API call for processed matched records*/
            $scope.ProcessedMatchedRows = [];
            $loading.start('ProcessMatchedTab');

            ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][status]=Matched&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                if (response.status == 200) {

                    angular.forEach(response.data, function (val, key) {
                        if (!val.exception) {

                            $scope.ProcessedMatchedRowsObj = {};

                            angular.forEach(val.mappedReportFields, function (val, key) {
                                ////console.log(val.fieldName,val.value);
                                var fieldNames;
                                var filedValues;
                                fieldNames = val.fieldName;
                                filedValues = val.value;
                                $scope.ProcessedMatchedRowsObj[fieldNames] = filedValues;

                            });
                            $scope.ProcessedMatchedRows.push($scope.ProcessedMatchedRowsObj);
                        }

                    });
                    //Making Json Objects for UI grid for Raw Report Rows end Here

                    $scope.gridOptionsMatchedProcessedRows.data = $scope.ProcessedMatchedRows;
                    $scope.filterProcessedMatchedGrid.data = $scope.ProcessedMatchedRows;

                    $loading.finish('ProcessMatchedTab');
                } else {
                    $loading.finish('ProcessMatchedTab');
                    toaster.pop("error", response.data.error.message);
                }

            });

            /*API call for processed matched records ends here*/
        },
        nextPage: function () {
            if ($scope.processedmatchedpresentpage != $scope.processedmatchedtotalpages) {
                $scope.processedmatchedpresentpage++;
                this.pageNumber++;
                this.skip = Number(this.limit) * Number(this.pageNumber);
                $scope.processedmatchedPagination.fromIndex = this.skip + 1;
                $nextMaxtoindex = this.skip + Number(this.limit);
                if ($scope.processedmatchedtotalcount < $nextMaxtoindex) {
                    $scope.processedmatchedPagination.toIndex = $scope.processedmatchedtotalcount;
                } else {
                    $scope.processedmatchedPagination.toIndex = $nextMaxtoindex;
                }
                /*API call for processed matched records*/
                $scope.ProcessedMatchedRows = [];
                $loading.start('ProcessMatchedTab');

                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][status]=Matched&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {

                        angular.forEach(response.data, function (val, key) {
                            if (!val.exception) {

                                $scope.ProcessedMatchedRowsObj = {};

                                angular.forEach(val.mappedReportFields, function (val, key) {
                                    ////console.log(val.fieldName,val.value);
                                    var fieldNames;
                                    var filedValues;
                                    fieldNames = val.fieldName;
                                    filedValues = val.value;
                                    $scope.ProcessedMatchedRowsObj[fieldNames] = filedValues;

                                });
                                $scope.ProcessedMatchedRows.push($scope.ProcessedMatchedRowsObj);
                            }

                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.gridOptionsMatchedProcessedRows.data = $scope.ProcessedMatchedRows;
                        $scope.filterProcessedMatchedGrid.data = $scope.ProcessedMatchedRows;



                        $loading.finish('ProcessMatchedTab');
                    } else {
                        $loading.finish('ProcessMatchedTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });

                /*API call for processed matched records ends here*/
            }

        },

        previousPage: function () {
            if (this.pageNumber > 0) {
                $scope.processedmatchedpresentpage--;
                this.pageNumber--;
                this.skip = this.limit * this.pageNumber;

                $NexttoIndex = $scope.processedmatchedPagination.toIndex - this.limit;
                if ($scope.processedmatchedPagination.toIndex == $scope.processedmatchedtotalcount) {
                    $scope.processedmatchedPagination.toIndex = $scope.processedmatchedPagination.fromIndex - 1;
                } else {
                    $scope.processedmatchedPagination.toIndex = $NexttoIndex;
                }
                $scope.processedmatchedPagination.fromIndex = $scope.processedmatchedPagination.fromIndex - this.limit;
                /*API call for processed matched records*/
                $scope.ProcessedMatchedRows = [];
                $loading.start('ProcessMatchedTab');

                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][status]=Matched&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {

                        angular.forEach(response.data, function (val, key) {
                            if (!val.exception) {

                                $scope.ProcessedMatchedRowsObj = {};

                                angular.forEach(val.mappedReportFields, function (val, key) {
                                    ////console.log(val.fieldName,val.value);
                                    var fieldNames;
                                    var filedValues;
                                    fieldNames = val.fieldName;
                                    filedValues = val.value;
                                    $scope.ProcessedMatchedRowsObj[fieldNames] = filedValues;

                                });
                                $scope.ProcessedMatchedRows.push($scope.ProcessedMatchedRowsObj);
                            }

                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.gridOptionsMatchedProcessedRows.data = $scope.ProcessedMatchedRows;
                        $scope.filterProcessedMatchedGrid.data = $scope.ProcessedMatchedRows;


                        $loading.finish('ProcessMatchedTab');
                    } else {
                        $loading.finish('ProcessMatchedTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });

                /*API call for processed matched records ends here*/

            }
        }
    };
    /*Processed matched rows pagination*/

    /* API starts here Exceptions */
    $scope.ProcessedExceptionsRowsAPI = function () {
        $scope.ProcessedExceptionsRows = [];
        $scope.ProcessedExceptionsRowsFieldData = [];

        $loading.start('ProcessExceptionsTab');
        var exceptionprocessedlimit;
        exceptionprocessedlimit = localStorage.getItem(localStorage.id + "processedExceptionPage");
        if (!exceptionprocessedlimit) {
            exceptionprocessedlimit = 20;
        }

        ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][exception][neq]=&filter[limit]=" + exceptionprocessedlimit + "&filter[skip]=0", function (response) {
            if (response.status == 200) {
                //console.log(response.data);
                angular.forEach(response.data, function (val, key) {

                    if (val.exception) {
                        //console.log(val);
                        $scope.ProcessedExceptionsRowsObj = {};
                        $scope.ProcessedExceptionsRowsFieldObj = {};
                        $scope.ProcessedExceptionsRowsObj.facilityAccountId = val.facilityAccountId;
                        $scope.ProcessedExceptionsRowsObj.id = val.id;
                        $scope.ProcessedExceptionsRowsObj.reportId = val.reportId;
                        $scope.ProcessedExceptionsRowsObj.row = val.row;
                        $scope.ProcessedExceptionsRowsObj['exception name'] = val.exception['message'];
                        $scope.ProcessedExceptionsRowsObj.individualType = val.individualType;
                        $scope.ProcessedExceptionsRowsObj.individualSubType = val.individualSubType;
                        $scope.ProcessedExceptionsRowsObj.memberType = val.memberType;
                        $scope.ProcessedExceptionsRowsObj.memberSubType = val.memberSubType;

                        $scope.ProcessedExceptionsRowsFieldObj['exception name'] = val.exception['message']
                        $scope.ProcessedExceptionsRowsFieldObj.individualType = val.individualType;
                        $scope.ProcessedExceptionsRowsFieldObj.individualSubType = val.individualSubType;
                        $scope.ProcessedExceptionsRowsFieldObj.memberType = val.memberType;
                        $scope.ProcessedExceptionsRowsFieldObj.memberSubType = val.memberSubType;

                        angular.forEach(val.mappedReportFields, function (val, key) {
                            ////console.log(val.fieldName,val.value);
                            var fieldNames;
                            var filedValues;
                            fieldNames = val.fieldName;
                            filedValues = val.value;
                            var field_id = fieldNames + '_id';
                            $scope.ProcessedExceptionsRowsObj[fieldNames] = filedValues;
                            $scope.ProcessedExceptionsRowsObj[field_id] = val.id;

                            $scope.ProcessedExceptionsRowsFieldObj[fieldNames] = filedValues;


                        });
                        $scope.ProcessedExceptionsRows.push($scope.ProcessedExceptionsRowsObj);
                        $scope.ProcessedExceptionsRowsFieldData.push($scope.ProcessedExceptionsRowsFieldObj);
                    }

                });
                //Making Json Objects for UI grid for Raw Report Rows end Here
                //console.log($scope.ProcessedExceptionsRowsFieldData);

                $scope.gridOptionsExceptionsProcessedRows.data = $scope.ProcessedExceptionsRows;
                $scope.filterProcessedExceptionsGrid.data = $scope.ProcessedExceptionsRows;
                $scope.ProcessedExceptionsRowsHeaderArr = $scope.ProcessedExceptionsRowsFieldData[0];
                //console.log($scope.ProcessedExceptionsRowsHeaderArr);
                $scope.filterProcessedExceptionsGrid.columnDefs = [];
                if (response.data) {
                    if (response.data[0].action == "Ignore") {
                        for (elem in $scope.ProcessedExceptionsRowsHeaderArr) {
                            $scope.ProcessedExceptionsRowsHeaderObj = {};
                            //console.log(elem);
                            if ((elem == "facilityAccountId") || (elem == "id") || (elem == "row") || (elem == "reportId")) {
                                continue;
                            }
                            //console.log(elem);
                            $scope.ProcessedExceptionsRowsHeaderObj['field'] = elem;

                            $scope.ProcessedExceptionsRowsHeaderObj['width'] = 150;
                            $scope.ProcessedExceptionsRowsHeaderObj['enableCellEdit'] = true;
                            $scope.ProcessedExceptionsRowsHeaderObj['cellTooltip'] = true;
                            $scope.ProcessedExceptionsRowsHeaderObj['headerTooltip'] = true;
                            $scope.ProcessedExceptionsRowsHeaderObj['cellClass'] = 'action-class';

                            $scope.filterProcessedExceptionsGrid.columnDefs.push($scope.ProcessedExceptionsRowsHeaderObj);

                        }

                    } else {
                        for (elem in $scope.ProcessedExceptionsRowsHeaderArr) {
                            $scope.ProcessedExceptionsRowsHeaderObj = {};
                            //console.log(elem);
                            if ((elem == "facilityAccountId") || (elem == "id") || (elem == "row") || (elem == "reportId")) {
                                continue;
                            }
                            //console.log(elem);
                            $scope.ProcessedExceptionsRowsHeaderObj['field'] = elem;

                            $scope.ProcessedExceptionsRowsHeaderObj['width'] = 150;
                            $scope.ProcessedExceptionsRowsHeaderObj['enableCellEdit'] = true;
                            $scope.ProcessedExceptionsRowsHeaderObj['cellTooltip'] = true;
                            $scope.ProcessedExceptionsRowsHeaderObj['headerTooltip'] = true;


                            $scope.filterProcessedExceptionsGrid.columnDefs.push($scope.ProcessedExceptionsRowsHeaderObj);

                        }
                    }
                }

                $loading.finish('ProcessExceptionsTab');
            } else {
                $loading.finish('ProcessExceptionsTab');
                toaster.pop("error", response.data.error.message);
            }

        });
    };
    /* API ends here Exceptions */


    /*Processed exception pagination starts*/
    $scope.exceptionProcessedlimit = 20;

    $scope.exceptionProcessedlimit = localStorage.getItem(localStorage.id + "processedExceptionPage");
    if (!$scope.exceptionProcessedlimit) {
        $scope.exceptionProcessedlimit = 20;
    }
    $scope.ProcessedExceptionPageLimitValue = localStorage.getItem(localStorage.id + "processedExceptionPage");
    $scope.pageNumber = 0;
    $scope.processedExceptionpresentpage = 1;

    //ReportService.ProcessedRowCount("ReportRows/filterStatus?reportId=" + $rootScope.reportId + "&status=Exceptions&count=1", function(response) {
    ReportService.GetProcessedCount('&where={"exception":{"neq":""},"reportId":\"' + $rootScope.reportId + '\"}', function (response) {
        if (response.status == 200) {
            $scope.processedExceptiontotalcount = response.data.count;
            if ($scope.processedExceptiontotalcount < $scope.exceptionProcessedlimit) {
                $scope.processedExceptionPagination.toIndex = $scope.processedExceptiontotalcount;
            } else {
                $scope.processedExceptionPagination.toIndex = $scope.exceptionProcessedlimit;
            }
            $scope.processedExceptiontotalpages = Math.ceil($scope.processedExceptiontotalcount / $scope.exceptionProcessedlimit);
        }
    });

    $scope.processedExceptionPagination = {
        limit: $scope.exceptionProcessedlimit,
        skip: 0,
        fromIndex: 1,
        toIndex: $scope.exceptionProcessedlimit,
        pageNumber: 0,
        loadSelCountPerPage: function (pageLimitValue) {
            localStorage.setItem(localStorage.id + "processedExceptionPage", pageLimitValue);
            this.pageNumber = 0;
            this.limit = pageLimitValue;
            this.skip = 0;
            $scope.processedExceptionpresentpage = 1;
            $scope.processedExceptiontotalpages = Math.ceil($scope.processedExceptiontotalcount / pageLimitValue);
            $scope.processedExceptionPagination.fromIndex = this.skip + 1;
            //$scope.processedExceptionPagination.toIndex = this.skip + Number(this.limit);
            if ($scope.processedExceptiontotalcount < pageLimitValue) {
                $scope.processedExceptionPagination.toIndex = $scope.processedExceptiontotalcount;
            } else {
                $scope.processedExceptionPagination.toIndex = this.skip + Number(this.limit);
            }
            /*API Call For Raw Report Rows Detals  */

            $scope.ProcessedExceptionsRows = [];
            $scope.ProcessedExceptionsRowsFieldData = [];

            $loading.start('ProcessExceptionsTab');

            ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][exception][neq]=&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                if (response.status == 200) {
                    //console.log(response.data);
                    angular.forEach(response.data, function (val, key) {

                        if (val.exception) {
                            $scope.ProcessedExceptionsRowsObj = {};
                            $scope.ProcessedExceptionsRowsFieldObj = {};
                            $scope.ProcessedExceptionsRowsObj.facilityAccountId = val.facilityAccountId;
                            $scope.ProcessedExceptionsRowsObj.id = val.id;
                            $scope.ProcessedExceptionsRowsObj.reportId = val.reportId;
                            $scope.ProcessedExceptionsRowsObj.row = val.row;
                            $scope.ProcessedExceptionsRowsObj['exception name'] = val.exception['message'];
                            $scope.ProcessedExceptionsRowsObj.individualType = val.individualType;
                            $scope.ProcessedExceptionsRowsObj.individualSubType = val.individualSubType;
                            $scope.ProcessedExceptionsRowsObj.memberType = val.memberType;
                            $scope.ProcessedExceptionsRowsObj.memberSubType = val.memberSubType;

                            $scope.ProcessedExceptionsRowsFieldObj['exception name'] = val.exception['message']
                            $scope.ProcessedExceptionsRowsFieldObj.individualType = val.individualType;
                            $scope.ProcessedExceptionsRowsFieldObj.individualSubType = val.individualSubType;
                            $scope.ProcessedExceptionsRowsFieldObj.memberType = val.memberType;
                            $scope.ProcessedExceptionsRowsFieldObj.memberSubType = val.memberSubType;



                            angular.forEach(val.mappedReportFields, function (val, key) {
                                ////console.log(val.fieldName,val.value);
                                var fieldNames;
                                var filedValues;
                                fieldNames = val.fieldName;
                                filedValues = val.value;
                                var field_id = fieldNames + '_id';
                                $scope.ProcessedExceptionsRowsObj[fieldNames] = filedValues;
                                $scope.ProcessedExceptionsRowsObj[field_id] = val.id;

                                $scope.ProcessedExceptionsRowsFieldObj[fieldNames] = filedValues;

                            });
                            $scope.ProcessedExceptionsRows.push($scope.ProcessedExceptionsRowsObj);
                            $scope.ProcessedExceptionsRowsFieldData.push($scope.ProcessedExceptionsRowsFieldObj);

                        }

                    });
                    //Making Json Objects for UI grid for Raw Report Rows end Here

                    $scope.gridOptionsExceptionsProcessedRows.data = $scope.ProcessedExceptionsRows;
                    $scope.filterProcessedExceptionsGrid.data = $scope.ProcessedExceptionsRows;

                    $loading.finish('ProcessExceptionsTab');
                } else {
                    $loading.finish('ProcessExceptionsTab');
                    toaster.pop("error", response.data.error.message);
                }

            });
            /*API Call For Raw Report Rows Detals end here */
        },
        nextPage: function () {
            if ($scope.processedExceptionpresentpage != $scope.processedExceptiontotalpages) {
                $scope.processedExceptionpresentpage++;
                this.pageNumber++;
                this.skip = Number(this.limit) * Number(this.pageNumber);
                $scope.processedExceptionPagination.fromIndex = this.skip + 1;
                $nextMaxtoindex = Number(this.skip) + Number(this.limit);
                if ($scope.processedExceptiontotalcount < $nextMaxtoindex) {
                    $scope.processedExceptionPagination.toIndex = $scope.processedExceptiontotalcount;
                } else {
                    $scope.processedExceptionPagination.toIndex = $nextMaxtoindex;
                }

                /*API Call For Raw Report Rows Detals  */

                $scope.ProcessedExceptionsRows = [];
                $scope.ProcessedExceptionsRowsFieldData = [];

                $loading.start('ProcessExceptionsTab');

                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][exception][neq]=&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {
                        //console.log(response.data);
                        angular.forEach(response.data, function (val, key) {

                            if (val.exception) {
                                $scope.ProcessedExceptionsRowsObj = {};
                                $scope.ProcessedExceptionsRowsFieldObj = {};
                                $scope.ProcessedExceptionsRowsObj.facilityAccountId = val.facilityAccountId;
                                $scope.ProcessedExceptionsRowsObj.id = val.id;
                                $scope.ProcessedExceptionsRowsObj.reportId = val.reportId;
                                $scope.ProcessedExceptionsRowsObj.row = val.row;
                                $scope.ProcessedExceptionsRowsObj['exception name'] = val.exception['message'];
                                $scope.ProcessedExceptionsRowsObj.individualType = val.individualType;
                                $scope.ProcessedExceptionsRowsObj.individualSubType = val.individualSubType;
                                $scope.ProcessedExceptionsRowsObj.memberType = val.memberType;
                                $scope.ProcessedExceptionsRowsObj.memberSubType = val.memberSubType;

                                $scope.ProcessedExceptionsRowsFieldObj['exception name'] = val.exception['message']
                                $scope.ProcessedExceptionsRowsFieldObj.individualType = val.individualType;
                                $scope.ProcessedExceptionsRowsFieldObj.individualSubType = val.individualSubType;
                                $scope.ProcessedExceptionsRowsFieldObj.memberType = val.memberType;
                                $scope.ProcessedExceptionsRowsFieldObj.memberSubType = val.memberSubType;

                                angular.forEach(val.mappedReportFields, function (val, key) {
                                    ////console.log(val.fieldName,val.value);
                                    var fieldNames;
                                    var filedValues;
                                    fieldNames = val.fieldName;
                                    filedValues = val.value;
                                    var field_id = fieldNames + '_id';
                                    $scope.ProcessedExceptionsRowsObj[fieldNames] = filedValues;
                                    $scope.ProcessedExceptionsRowsObj[field_id] = val.id;

                                    $scope.ProcessedExceptionsRowsFieldObj[fieldNames] = filedValues;


                                });
                                $scope.ProcessedExceptionsRows.push($scope.ProcessedExceptionsRowsObj);
                                $scope.ProcessedExceptionsRowsFieldData.push($scope.ProcessedExceptionsRowsFieldObj);

                            }

                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.gridOptionsExceptionsProcessedRows.data = $scope.ProcessedExceptionsRows;
                        $scope.filterProcessedExceptionsGrid.data = $scope.ProcessedExceptionsRows;

                        $loading.finish('ProcessExceptionsTab');
                    } else {
                        $loading.finish('ProcessExceptionsTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });
                /*API Call For Raw Report Rows Detals end here */
            }

        },

        previousPage: function () {
            if (this.pageNumber > 0) {
                $scope.processedExceptionpresentpage--;
                this.pageNumber--;
                this.skip = this.limit * this.pageNumber;
                $NexttoIndex = $scope.processedExceptionPagination.toIndex - this.limit;

                if ($scope.processedExceptionPagination.toIndex == $scope.processedExceptiontotalcount) {
                    $scope.processedExceptionPagination.toIndex = $scope.processedExceptionPagination.fromIndex - 1;
                } else {
                    $scope.processedExceptionPagination.toIndex = $NexttoIndex;
                }
                $scope.processedExceptionPagination.fromIndex = $scope.processedExceptionPagination.fromIndex - this.limit;

                /*API Call For Raw Report Rows Detals  */

                $scope.ProcessedExceptionsRows = [];
                $scope.ProcessedExceptionsRowsFieldData = [];

                $loading.start('ProcessExceptionsTab');

                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][exception][neq]=&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {
                        //console.log(response.data);
                        angular.forEach(response.data, function (val, key) {

                            if (val.exception) {
                                $scope.ProcessedExceptionsRowsObj = {};
                                $scope.ProcessedExceptionsRowsFieldObj = {};
                                $scope.ProcessedExceptionsRowsObj.facilityAccountId = val.facilityAccountId;
                                $scope.ProcessedExceptionsRowsObj.id = val.id;
                                $scope.ProcessedExceptionsRowsObj.reportId = val.reportId;
                                $scope.ProcessedExceptionsRowsObj.row = val.row;
                                $scope.ProcessedExceptionsRowsObj['exception name'] = val.exception['message'];
                                $scope.ProcessedExceptionsRowsObj.individualType = val.individualType;
                                $scope.ProcessedExceptionsRowsObj.individualSubType = val.individualSubType;
                                $scope.ProcessedExceptionsRowsObj.memberType = val.memberType;
                                $scope.ProcessedExceptionsRowsObj.memberSubType = val.memberSubType;

                                $scope.ProcessedExceptionsRowsFieldObj['exception name'] = val.exception['message']
                                $scope.ProcessedExceptionsRowsFieldObj.individualType = val.individualType;
                                $scope.ProcessedExceptionsRowsFieldObj.individualSubType = val.individualSubType;
                                $scope.ProcessedExceptionsRowsFieldObj.memberType = val.memberType;
                                $scope.ProcessedExceptionsRowsFieldObj.memberSubType = val.memberSubType;

                                angular.forEach(val.mappedReportFields, function (val, key) {
                                    ////console.log(val.fieldName,val.value);
                                    var fieldNames;
                                    var filedValues;
                                    fieldNames = val.fieldName;
                                    filedValues = val.value;
                                    var field_id = fieldNames + '_id';
                                    $scope.ProcessedExceptionsRowsObj[fieldNames] = filedValues;
                                    $scope.ProcessedExceptionsRowsObj[field_id] = val.id;

                                    $scope.ProcessedExceptionsRowsFieldObj[fieldNames] = filedValues;
                                });
                                $scope.ProcessedExceptionsRows.push($scope.ProcessedExceptionsRowsObj);
                                $scope.ProcessedExceptionsRowsFieldData.push($scope.ProcessedExceptionsRowsFieldObj);

                            }

                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.gridOptionsExceptionsProcessedRows.data = $scope.ProcessedExceptionsRows;
                        $scope.filterProcessedExceptionsGrid.data = $scope.ProcessedExceptionsRows;

                        $loading.finish('ProcessExceptionsTab');
                    } else {
                        $loading.finish('ProcessExceptionsTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });
                /*API Call For Raw Report Rows Detals end here */

            }
        }
    };

    /*Processed exception pagination end here*/

    /* API starts here Duplicates */
    $scope.ProcessedDuplicatesRowsAPI = function () {
        $scope.ProcessedDuplicatesRows = [];
        $loading.start('ProcessDuplicatesTab');

        var duplicatesprocessedlimit;
        duplicatesprocessedlimit = localStorage.getItem(localStorage.id + "processedduplicatesPage");
        if (!duplicatesprocessedlimit) {
            duplicatesprocessedlimit = 20;
        }

        ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][status]=Duplicates&filter[limit]=" + duplicatesprocessedlimit + "&filter[skip]=0", function (response) {
            if (response.status == 200) {
                //console.log(response);

                angular.forEach(response.data, function (val, key) {

                    if (!val.exception) {

                        $scope.ProcessedDuplicatesRowsObj = {};

                        angular.forEach(val.mappedReportFields, function (val, key) {
                            ////console.log(val.fieldName,val.value);
                            var fieldNames;
                            var filedValues;
                            fieldNames = val.fieldName;
                            filedValues = val.value;
                            $scope.ProcessedDuplicatesRowsObj[fieldNames] = filedValues;

                        });
                        $scope.ProcessedDuplicatesRows.push($scope.ProcessedDuplicatesRowsObj);
                    }

                });
                //Making Json Objects for UI grid for Raw Report Rows end Here

                $scope.gridOptionsDuplicatesProcessedRows.data = $scope.ProcessedDuplicatesRows;
                $scope.filterProcessedDuplicatesGrid.data = $scope.ProcessedDuplicatesRows;
                $scope.DuplicatesProcessedRowsHeaderArr = $scope.ProcessedDuplicatesRows[0];
                if (response.data) {
                    if (response.data[0].action == "Ignore") {
                        $scope.filterProcessedDuplicatesGrid.columnDefs = [];
                        for (elem in $scope.DuplicatesProcessedRowsHeaderArr) {
                            $scope.DuplicatesProcessedRowsHeaderObj = {};

                            $scope.DuplicatesProcessedRowsHeaderObj['field'] = elem;
                            $scope.DuplicatesProcessedRowsHeaderObj['width'] = 150;
                            $scope.DuplicatesProcessedRowsHeaderObj['cellTooltip'] = true;
                            $scope.DuplicatesProcessedRowsHeaderObj['headerTooltip'] = true;
                            $scope.DuplicatesProcessedRowsHeaderObj['cellClass'] = 'action-class';

                            $scope.filterProcessedDuplicatesGrid.columnDefs.push($scope.DuplicatesProcessedRowsHeaderObj);

                        }

                    } else {
                        $scope.filterProcessedDuplicatesGrid.columnDefs = [];
                        for (elem in $scope.DuplicatesProcessedRowsHeaderArr) {
                            $scope.DuplicatesProcessedRowsHeaderObj = {};

                            $scope.DuplicatesProcessedRowsHeaderObj['field'] = elem;
                            $scope.DuplicatesProcessedRowsHeaderObj['width'] = 150;
                            $scope.DuplicatesProcessedRowsHeaderObj['cellTooltip'] = true;
                            $scope.DuplicatesProcessedRowsHeaderObj['headerTooltip'] = true;
                            $scope.filterProcessedDuplicatesGrid.columnDefs.push($scope.DuplicatesProcessedRowsHeaderObj);

                        }

                    }
                }
                $loading.finish('ProcessDuplicatesTab');
            } else {
                $loading.finish('ProcessDuplicatesTab');
                toaster.pop("error", response.data.error.message);
            }

        });
    };
    /* API ends here Duplicates */

    /*Processed duplication pagination starts*/
    $scope.duplicatesProcessedlimit = 20;

    $scope.duplicatesProcessedlimit = localStorage.getItem(localStorage.id + "processedDuplicatesPage");
    if (!$scope.duplicatesProcessedlimit) {
        $scope.duplicatesProcessedlimit = 20;
    }
    $scope.ProcessedDuplicatesPageLimitValue = localStorage.getItem(localStorage.id + "processedDuplicatesPage");

    $scope.pageNumber = 0;
    $scope.processedDuplicatepresentpage = 1;

    // ReportService.ProcessedRowCount("ReportRows/filterStatus?reportId=" + $rootScope.reportId + "&status=Duplicates&count=1", function(response) {
    ReportService.GetProcessedCount('&where={"action":{"neq":"Ignore"},"status":"Duplicates","reportId":\"' + $rootScope.reportId + '\"}', function (response) {
        if (response.status == 200) {

            $scope.processedDuplicatetotalcount = response.data.count;
            if ($scope.processedDuplicatetotalcount < $scope.duplicatesProcessedlimit) {
                $scope.processedDuplicatePagination.toIndex = $scope.processedDuplicatetotalcount;
            } else {
                $scope.processedDuplicatePagination.toIndex = $scope.duplicatesProcessedlimit;
            }
            $scope.processedDuplicatetotalpages = Math.ceil($scope.processedDuplicatetotalcount / $scope.duplicatesProcessedlimit);
        }
    });


    $scope.processedDuplicatePagination = {
        limit: $scope.duplicatesProcessedlimit,
        skip: 0,
        fromIndex: 1,
        //toIndex: $scope.duplicatesProcessedlimit,
        pageNumber: 0,
        loadSelCountPerPage: function (pageLimitValue) {
            localStorage.setItem(localStorage.id + "processedDuplicatesPage", pageLimitValue);
            this.pageNumber = 0;
            this.limit = pageLimitValue;
            this.skip = 0;
            $scope.processedDuplicatepresentpage = 1;
            $scope.processedDuplicatetotalpages = Math.ceil($scope.processedDuplicatetotalcount / pageLimitValue);
            $scope.processedDuplicatePagination.fromIndex = this.skip + 1;
            //$scope.processedDuplicatePagination.toIndex = this.skip + Number(this.limit);
            if ($scope.processedDuplicatetotalcount < pageLimitValue) {
                $scope.processedDuplicatePagination.toIndex = $scope.processedDuplicatetotalcount;
            } else {
                $scope.processedDuplicatePagination.toIndex = this.skip + Number(this.limit);
            }
            /*API Call for processed duplicates*/
            $scope.ProcessedDuplicatesRows = [];
            $loading.start('ProcessDuplicatesTab');

            ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][action][neq]=Ignore&filter[where][status]=Duplicates&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                if (response.status == 200) {

                    angular.forEach(response.data, function (val, key) {
                        if (!val.exception) {

                            $scope.ProcessedDuplicatesRowsObj = {};

                            angular.forEach(val.mappedReportFields, function (val, key) {
                                ////console.log(val.fieldName,val.value);
                                var fieldNames;
                                var filedValues;
                                fieldNames = val.fieldName;
                                filedValues = val.value;
                                $scope.ProcessedDuplicatesRowsObj[fieldNames] = filedValues;

                            });
                            $scope.ProcessedDuplicatesRows.push($scope.ProcessedDuplicatesRowsObj);
                        }

                    });
                    //Making Json Objects for UI grid for Raw Report Rows end Here

                    $scope.gridOptionsDuplicatesProcessedRows.data = $scope.ProcessedDuplicatesRows;
                    $scope.filterProcessedDuplicatesGrid.data = $scope.ProcessedDuplicatesRows;

                    $loading.finish('ProcessDuplicatesTab');
                } else {
                    $loading.finish('ProcessDuplicatesTab');
                    toaster.pop("error", response.data.error.message);
                }

            });

            /*API Call for processed duplicates ends here*/
        },
        nextPage: function () {
            if ($scope.processedDuplicatepresentpage != $scope.processedDuplicatetotalpages) {
                $scope.processedDuplicatepresentpage++;
                this.pageNumber++;
                this.skip = Number(this.limit) * Number(this.pageNumber);
                $scope.processedDuplicatePagination.fromIndex = this.skip + 1;
                $nextMaxtoindex = this.skip + Number(this.limit);
                if ($scope.processedDuplicatetotalcount < $nextMaxtoindex) {
                    $scope.processedDuplicatePagination.toIndex = $scope.processedDuplicatetotalcount;
                } else {
                    $scope.processedDuplicatePagination.toIndex = $nextMaxtoindex;
                }

                /*API Call for processed duplicates*/
                $scope.ProcessedDuplicatesRows = [];
                $loading.start('ProcessDuplicatesTab');

                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][action][neq]=Ignore&filter[where][status]=Duplicates&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {

                        angular.forEach(response.data, function (val, key) {
                            if (!val.exception) {

                                $scope.ProcessedDuplicatesRowsObj = {};

                                angular.forEach(val.mappedReportFields, function (val, key) {
                                    ////console.log(val.fieldName,val.value);
                                    var fieldNames;
                                    var filedValues;
                                    fieldNames = val.fieldName;
                                    filedValues = val.value;
                                    $scope.ProcessedDuplicatesRowsObj[fieldNames] = filedValues;

                                });
                                $scope.ProcessedDuplicatesRows.push($scope.ProcessedDuplicatesRowsObj);
                            }

                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.gridOptionsDuplicatesProcessedRows.data = $scope.ProcessedDuplicatesRows;
                        $scope.filterProcessedDuplicatesGrid.data = $scope.ProcessedDuplicatesRows;

                        $loading.finish('ProcessDuplicatesTab');
                    } else {
                        $loading.finish('ProcessDuplicatesTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });

                /*API Call for processed duplicates ends here*/
            }

        },

        previousPage: function () {
            if (this.pageNumber > 0) {
                $scope.processedDuplicatepresentpage--;
                this.pageNumber--;
                this.skip = this.limit * this.pageNumber;

                $NexttoIndex = $scope.processedDuplicatePagination.toIndex - this.limit;
                if ($scope.processedDuplicatePagination.toIndex == $scope.processedDuplicatetotalcount) {
                    $scope.processedDuplicatePagination.toIndex = $scope.processedDuplicatePagination.fromIndex - 1;
                } else {
                    $scope.processedDuplicatePagination.toIndex = $NexttoIndex;
                }
                $scope.processedDuplicatePagination.fromIndex = $scope.processedDuplicatePagination.fromIndex - this.limit;
                /*API Call for processed duplicates*/
                $scope.ProcessedDuplicatesRows = [];
                $loading.start('ProcessDuplicatesTab');

                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][action][neq]=Ignore&filter[where][status]=Duplicates&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {

                        angular.forEach(response.data, function (val, key) {
                            if (!val.exception) {

                                $scope.ProcessedDuplicatesRowsObj = {};

                                angular.forEach(val.mappedReportFields, function (val, key) {
                                    ////console.log(val.fieldName,val.value);
                                    var fieldNames;
                                    var filedValues;
                                    fieldNames = val.fieldName;
                                    filedValues = val.value;
                                    $scope.ProcessedDuplicatesRowsObj[fieldNames] = filedValues;

                                });
                                $scope.ProcessedDuplicatesRows.push($scope.ProcessedDuplicatesRowsObj);
                            }

                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.gridOptionsDuplicatesProcessedRows.data = $scope.ProcessedDuplicatesRows;
                        $scope.filterProcessedDuplicatesGrid.data = $scope.ProcessedDuplicatesRows;

                        $loading.finish('ProcessDuplicatesTab');
                    } else {
                        $loading.finish('ProcessDuplicatesTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });

                /*API Call for processed duplicates ends here*/
            }
        }
    };

    /*Processed duplication pagination ends here*/

    /* API starts here NonPrimary */
    $scope.ProcessedNonPrimaryRowsAPI = function () {
        $scope.ProcessedNonPrimaryRows = [];
        $loading.start('ProcessNonPrimaryTab');
        var nonPrimaryprocessedlimit;
        nonPrimaryprocessedlimit = localStorage.getItem(localStorage.id + "processedNonPrimaryPage");
        if (!nonPrimaryprocessedlimit) {
            nonPrimaryprocessedlimit = 20;
        }

        ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][action][neq]=Update&filter[where][nonPrimaryFacility]=true&filter[limit]=" + nonPrimaryprocessedlimit + "&filter[skip]=0", function (response) {
            if (response.status == 200) {

                angular.forEach(response.data, function (val, key) {
                    if (!val.exception) {

                        $scope.ProcessedNonPrimaryRowsObj = {};

                        angular.forEach(val.mappedReportFields, function (val, key) {
                            ////console.log(val.fieldName,val.value);
                            var fieldNames;
                            var filedValues;
                            fieldNames = val.fieldName;
                            filedValues = val.value;
                            $scope.ProcessedNonPrimaryRowsObj[fieldNames] = filedValues;

                        });
                        $scope.ProcessedNonPrimaryRows.push($scope.ProcessedNonPrimaryRowsObj);
                    }

                });
                //Making Json Objects for UI grid for Raw Report Rows end Here

                $scope.gridOptionsNonPrimaryProcessedRows.data = $scope.ProcessedNonPrimaryRows;
                $scope.filterProcessedNonPrimaryGrid.data = $scope.ProcessedNonPrimaryRows;
                $scope.ProcessedNonPrimaryGridHeaderArr = $scope.ProcessedNonPrimaryRows[0];
                $scope.filterProcessedNonPrimaryGrid.columnDefs = [];
                for (elem in $scope.ProcessedNonPrimaryGridHeaderArr) {
                    $scope.ProcessedNonPrimaryGridHeaderObj = {};

                    $scope.ProcessedNonPrimaryGridHeaderObj['field'] = elem;
                    $scope.ProcessedNonPrimaryGridHeaderObj['width'] = 150;
                    $scope.ProcessedNonPrimaryGridHeaderObj['cellTooltip'] = true;
                    $scope.ProcessedNonPrimaryGridHeaderObj['headerTooltip'] = true;
                    //$scope.NewProcessedRowsHeaderObj['cellClass'] = 'action-class';

                    $scope.filterProcessedNonPrimaryGrid.columnDefs.push($scope.ProcessedNonPrimaryGridHeaderObj);

                }

                $loading.finish('ProcessNonPrimaryTab');
            } else {
                $loading.finish('ProcessNonPrimaryTab');
                toaster.pop("error", response.data.error.message);
            }

        });
    };
    /* API ends here NonPrimary */

    /*Processed non primaryfacility pagination*/
    $scope.nonPrimaryProcessedlimit = 20;
    $scope.pageNumber = 0;
    $scope.processedNonPriFacilitypresentpage = 1;

    ReportService.GetProcessedCount('&where={"action":{"neq":"Update"},"nonPrimaryFacility":"true","stage":"Processed","reportId":\"' + $rootScope.reportId + '\"}', function (response) {
        ////console.log(response);
        if (response.status == 200) {


            $scope.processedNonPrimarytotalcount = response.data.count;
            if ($scope.processedNonPrimarytotalcount < $scope.nonPrimaryProcessedlimit) {
                $scope.processedNonPriFacilityPagination.toIndex = $scope.processedNonPrimarytotalcount;
            } else {
                $scope.processedNonPriFacilityPagination.toIndex = $scope.nonPrimaryProcessedlimit;
            }
            $scope.processedNonPriFacilitytotalpages = Math.ceil($scope.processedNonPrimarytotalcount / $scope.nonPrimaryProcessedlimit);


        }
    });


    $scope.processedNonPriFacilityPagination = {
        limit: $scope.nonPrimaryProcessedlimit,
        skip: 0,
        fromIndex: 1,
        toIndex: $scope.nonPrimaryProcessedlimit,
        pageNumber: 0,
        loadSelCountPerPage: function (pageLimitValue) {
            this.pageNumber = 0;
            this.limit = pageLimitValue;
            this.skip = 0;
            $scope.processedNonPriFacilitypresentpage = 1;
            $scope.processedNonPriFacilitytotalpages = Math.ceil($scope.processedNonPriFacilitytotalcount / pageLimitValue);
            $scope.processedNonPriFacilityPagination.fromIndex = this.skip + 1;
            //$scope.processedNonPriFacilityPagination.toIndex = this.skip + Number(this.limit);
            if ($scope.processedNonPrimarytotalcount < pageLimitValue) {
                $scope.processedNonPriFacilityPagination.toIndex = $scope.processedNonPrimarytotalcount;
            } else {
                $scope.processedNonPriFacilityPagination.toIndex = this.skip + Number(this.limit);
            }

            /*API Call For Raw Report Rows Detals  */

            ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][action][neq]=Update&filter[where][nonPrimaryFacility]=true&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                if (response.status == 200) {

                    angular.forEach(response.data, function (val, key) {
                        if (!val.exception) {

                            $scope.ProcessedNonPrimaryRowsObj = {};

                            angular.forEach(val.mappedReportFields, function (val, key) {
                                ////console.log(val.fieldName,val.value);
                                var fieldNames;
                                var filedValues;
                                fieldNames = val.fieldName;
                                filedValues = val.value;
                                $scope.ProcessedNonPrimaryRowsObj[fieldNames] = filedValues;

                            });
                            $scope.ProcessedNonPrimaryRows.push($scope.ProcessedNonPrimaryRowsObj);
                        }

                    });
                    //Making Json Objects for UI grid for Raw Report Rows end Here

                    $scope.gridOptionsNonPrimaryProcessedRows.data = $scope.ProcessedNonPrimaryRows;
                    $scope.filterProcessedNonPrimaryGrid.data = $scope.ProcessedNonPrimaryRows;

                    $loading.finish('ProcessNonPrimaryTab');
                } else {
                    $loading.finish('ProcessNonPrimaryTab');
                    toaster.pop("error", response.data.error.message);
                }

            });
            /*API Call For Raw Report Rows Detals end here */
        },
        nextPage: function () {
            if ($scope.processedNonPriFacilitypresentpage != $scope.processedNonPriFacilitytotalpages) {
                $scope.processedNonPriFacilitypresentpage++;
                this.pageNumber++;
                this.skip = Number(this.limit) * Number(this.pageNumber);
                $scope.processedNonPriFacilityPagination.fromIndex = this.skip + 1;
                //$scope.processedNonPriFacilityPagination.toIndex = this.skip + Number(this.limit);

                $nextMaxtoindex = this.skip + Number(this.limit);
                if ($scope.processedNonPrimarytotalcount < $nextMaxtoindex) {
                    $scope.processedNonPriFacilityPagination.toIndex = $scope.processedNonPrimarytotalcount;
                } else {
                    $scope.processedNonPriFacilityPagination.toIndex = $nextMaxtoindex;
                }

                /*API Call For Raw Report Rows Detals  */
                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][action][neq]=Update&filter[where][nonPrimaryFacility]=true&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {

                        angular.forEach(response.data, function (val, key) {
                            if (!val.exception) {

                                $scope.ProcessedNonPrimaryRowsObj = {};

                                angular.forEach(val.mappedReportFields, function (val, key) {
                                    ////console.log(val.fieldName,val.value);
                                    var fieldNames;
                                    var filedValues;
                                    fieldNames = val.fieldName;
                                    filedValues = val.value;
                                    $scope.ProcessedNonPrimaryRowsObj[fieldNames] = filedValues;

                                });
                                $scope.ProcessedNonPrimaryRows.push($scope.ProcessedNonPrimaryRowsObj);
                            }

                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.gridOptionsNonPrimaryProcessedRows.data = $scope.ProcessedNonPrimaryRows;
                        $scope.filterProcessedNonPrimaryGrid.data = $scope.ProcessedNonPrimaryRows;

                        $loading.finish('ProcessNonPrimaryTab');
                    } else {
                        $loading.finish('ProcessNonPrimaryTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });
                /*API Call For Raw Report Rows Detals end here */
            }

        },

        previousPage: function () {
            if (this.pageNumber > 0) {
                $scope.processedNonPriFacilitypresentpage--;
                this.pageNumber--;
                this.skip = this.limit * this.pageNumber;


                $NexttoIndex = $scope.processedNonPriFacilityPagination.toIndex - this.limit;

                if ($scope.processedNonPriFacilityPagination.toIndex == $scope.processedNonPrimarytotalcount) {
                    $scope.processedNonPriFacilityPagination.toIndex = $scope.processedNonPriFacilityPagination.fromIndex - 1;
                } else {
                    $scope.processedNonPriFacilityPagination.toIndex = $NexttoIndex;
                }
                $scope.processedNonPriFacilityPagination.fromIndex = $scope.processedNonPriFacilityPagination.fromIndex - this.limit;

                /*API Call For Raw Report Rows Detals  */
                ReportService.GetProcessedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed&filter[where][action][neq]=Update&filter[where][nonPrimaryFacility]=true&filter[limit]=" + this.limit + "&filter[skip]=" + this.skip, function (response) {
                    if (response.status == 200) {

                        angular.forEach(response.data, function (val, key) {
                            if (!val.exception) {

                                $scope.ProcessedNonPrimaryRowsObj = {};

                                angular.forEach(val.mappedReportFields, function (val, key) {
                                    ////console.log(val.fieldName,val.value);
                                    var fieldNames;
                                    var filedValues;
                                    fieldNames = val.fieldName;
                                    filedValues = val.value;
                                    $scope.ProcessedNonPrimaryRowsObj[fieldNames] = filedValues;

                                });
                                $scope.ProcessedNonPrimaryRows.push($scope.ProcessedNonPrimaryRowsObj);
                            }

                        });
                        //Making Json Objects for UI grid for Raw Report Rows end Here

                        $scope.gridOptionsNonPrimaryProcessedRows.data = $scope.ProcessedNonPrimaryRows;
                        $scope.filterProcessedNonPrimaryGrid.data = $scope.ProcessedNonPrimaryRows;

                        $loading.finish('ProcessNonPrimaryTab');
                    } else {
                        $loading.finish('ProcessNonPrimaryTab');
                        toaster.pop("error", response.data.error.message);
                    }

                });
                /*API Call For Raw Report Rows Detals end here */

            }
        }
    };

    /*Processed non primaryfacility pagination ends here*/

    //console.log("processed tab");

    /* Count for all tabs */
    ReportService.ProcessedReportRowAllCount("&where[reportId]=" + $rootScope.reportId, function (response) {
        if (response.status == 200) {

            $scope.TotalReportCount = response.data.count;

        }
    });
    ReportService.ProcessedRowCount("ReportRows/filterStatus?reportId=" + $rootScope.reportId + "&status=New&count=1", function (response) {
        if (response.status == 200) {
            $scope.newCount = response.data.count;
        }
    });
    ReportService.ProcessedRowCount("ReportRows/filterStatus?reportId=" + $rootScope.reportId + "&status=Update&count=1", function (response) {
        ////console.log(response);
        if (response.status == 200) {
            $scope.needUpdateCount = response.data.count;
        }
    });
    ReportService.ProcessedRowCount("ReportRows/filterStatus?reportId=" + $rootScope.reportId + "&status=Matched&count=1", function (response) {
        ////console.log(response);
        if (response.status == 200) {
            $scope.matchedCount = response.data.count;
        }
    });

    //&where={"action":{"neq":"Ignore"},"reportId":"56f146f01f80c3e5403d1b27","stage":"Processed","status":"New"}
    // ReportService.ProcessedRowCount("ReportRows/filterStatus?reportId=" + $rootScope.reportId + "&status=Duplicates&count=1", function(response) {
    ReportService.GetProcessedCount('&where={"status":"Duplicates","reportId":\"' + $rootScope.reportId + '\"}', function (response) {
        ////console.log(response);
        if (response.status == 200) {
            $scope.duplicatesCount = response.data.count;
        }
    });
    //ReportService.ProcessedRowCount("ReportRows/filterStatus?reportId=" + $rootScope.reportId + "&status=Exceptions&count=1", function(response) {
    ReportService.GetProcessedCount('&where={"exception":{"neq":""},"reportId":\"' + $rootScope.reportId + '\"}', function (response) {
        ////console.log(response);
        if (response.status == 200) {
            $scope.exceptionsCount = response.data.count;
        }
    });
    //ReportService.GetProcessedCount('&where={"and":[{"stage":"Processed"},{"nonPrimaryFacility":"true"},{"reportId":\"' + $rootScope.reportId + '\"}]}', function(response) {
    ReportService.GetProcessedCount('&where={"action":{"neq":"Update"},"nonPrimaryFacility":"true","stage":"Processed","reportId":\"' + $rootScope.reportId + '\"}', function (response) {
        ////console.log(response);
        if (response.status == 200) {
            $scope.nonPrimaryFacilityCount = response.data.count;
        }
    });

    /* Count for all tabs */

    /* Action button API Starts here */
    $scope.matchedImportAction = function () {
        $loading.start('ProcessMatchedTab');
        ReportService.PostProcessedImportAction('&filter={"and":[{"stage":"Processed"},{"status":"Matched"},{"reportId":\"' + $rootScope.reportId + '\"}]}', {
            "action": "Import"
        }, function (response) {
            //console.log(response);
            if (response.status == 200) {
                $loading.finish('ProcessMatchedTab');
                toaster.pop("success", "Import Action has been done Successfully");
                $scope.ProcessedMatchedRowsAPI();
            } else {
                $loading.finish('ProcessMatchedTab');
                toaster.pop("error", response.data.error.message);
            }
        });
    };
    $scope.NeedUpdateAction = function () {
        $loading.start('ProcessMatchedTab');
        ReportService.PostProcessedUpdateAction('&filter={"and":[{"stage":"Processed"},{"status":"Update"},{"reportId":\"' + $rootScope.reportId + '\"}]}', {
            "action": "Update"
        }, function (response) {
            //console.log(response);
            if (response.status == 200) {
                $loading.finish('ProcessMatchedTab');
                toaster.pop("success", "Update Action has been done Successfully");
            } else {
                $loading.finish('ProcessMatchedTab');
                toaster.pop("error", response.data.error.message);
            }
        });
    };

    $scope.clearDuplicatesAction = function () {
        $loading.start('ProcessDuplicatesTab');
        ReportService.PostProcessedClearAction('&filter={"and":[{"stage":"Processed"},{"status":"Duplicates"},{"reportId":\"' + $rootScope.reportId + '\"}]}', {
            "action": "Ignore"
        }, function (response) {
            //console.log(response);
            if (response.status == 200) {
                $loading.finish('ProcessDuplicatesTab');
                toaster.pop("success", "Clear Action has been done Successfully");
                $scope.ProcessedDuplicatesRowsAPI();

            } else {
                $loading.finish('ProcessDuplicatesTab');
                toaster.pop("error", response.data.error.message);
            }
        });
    };

    $scope.clearExceptionsAction = function () {
        $loading.start('ProcessExceptionsTab');
        ReportService.PostProcessedClearAction('&filter={"and":[{"stage":"Processed"},{"reportId":\"' + $rootScope.reportId + '\"}]}' + '&where={"exception":{"neq":""}}', {
            "action": "Ignore"
        }, function (response) {
            //console.log(response);
            if (response.status == 200) {
                $loading.finish('ProcessExceptionsTab');
                toaster.pop("success", "Clear Action has been done Successfully");
                $scope.ProcessedExceptionsRowsAPI();
            } else {
                $loading.finish('ProcessExceptionsTab');
                toaster.pop("error", response.data.error.message);
            }
        });
    };

    $scope.updateNonPrimaryAction = function () {
        $loading.start('ProcessNonPrimaryTab');
        ReportService.PostProcessedUpdateAction('&filter={"and":[{"stage":"Processed"},{"nonPrimaryFacility":"true"},{"reportId":\"' + $rootScope.reportId + '\"}]}', {
            "action": "Update"
        }, function (response) {
            //console.log(response);
            if (response.status == 200) {
                $loading.finish('ProcessNonPrimaryTab');
                toaster.pop("success", "Update Action has been done Successfully");
            } else {
                $loading.finish('ProcessNonPrimaryTab');
                toaster.pop("error", response.data.error.message);
            }
        });
    };


    /* Action button API Ends here */

    this.tab = 1;

    this.selectSubTab = function (setTab) {
        this.tab = setTab;
        if (setTab == 1) {
            //console.log("processed tab1 by click");
        } else if (setTab == 2) {
            $scope.ProcessedNewRowsAPI();
            //console.log("processed tab2 by click");
        } else if (setTab == 3) {
            $scope.ProcessedUpdateRowsAPI();
            //console.log("processed tab3 by click");
        } else if (setTab == 4) {
            $scope.ProcessedMatchedRowsAPI();
            //console.log("processed tab4 by click");
        } else if (setTab == 5) {
            $scope.ProcessedDuplicatesRowsAPI();
            //console.log("processed tab5 by click");
        } else if (setTab == 6) {
            $scope.ProcessedExceptionsRowsAPI();
            //console.log("processed tab6 by click");
        } else if (setTab == 7) {
            $scope.ProcessedNonPrimaryRowsAPI();
            //console.log("processed tab7 by click");

            /* Non primary facility */
            if ($rootScope.reportType == "Dues") {
                $scope.processRefundBtn = true;
                $scope.processBtn = false;
                ////console.log($rootScope.reportType);
            } else if ($rootScope.reportType == "Combo") {
                $scope.processRefundBtn = true;
                $scope.processBtn = false;
                ////console.log($rootScope.reportType);
            } else if ($rootScope.reportType == "Demographic") {
                $scope.processRefundBtn = false;
                $scope.processBtn = true;
                ////console.log($rootScope.reportType);
            } else {
                $scope.processRefundBtn = false;
                $scope.processBtn = false;
                ////console.log($rootScope.reportType);
            }
            /* Non primary facility */

        }

    };
    this.isSelected = function (checkTab) {
        return this.tab === checkTab;
    };

    /*Search Operation  for New TAB*/
    $scope.filterProcessedNewGrid.data = $scope.gridOptionsNewProcessedRows.data;
    $scope.refreshNewTabData = function (termObj) {

        //console.log(termObj);
        $scope.filterProcessedNewGrid.data = $scope.gridOptionsNewProcessedRows.data;

        while (termObj) {
            var oSearchArray = termObj.split(' ');
            $scope.filterProcessedNewGrid.data = $filter('filter')($scope.filterProcessedNewGrid.data, oSearchArray[0], undefined);
            oSearchArray.shift()
            termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
        }
        $scope.ProcessedNewPagination.toIndex = $scope.filterProcessedNewGrid.data.length;
    };
    /*Search Operation  for New TAB end here*/

    /*Search Operation  for Update TAB*/
    $scope.filterProcessedUpdateGrid.data = $scope.gridOptionsUpdateProcessedRows.data;
    $scope.refreshUpdateTabData = function (termObj) {

        //console.log(termObj);
        $scope.filterProcessedUpdateGrid.data = $scope.gridOptionsUpdateProcessedRows.data;

        while (termObj) {
            var oSearchArray = termObj.split(' ');
            $scope.filterProcessedUpdateGrid.data = $filter('filter')($scope.filterProcessedUpdateGrid.data, oSearchArray[0], undefined);
            oSearchArray.shift()
            termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
        }
        $scope.processedupdatePagination.toIndex = $scope.filterProcessedUpdateGrid.data.length;
    };
    /*Search Operation  for Update TAB end here*/

    /*Search Operation  for Matched TAB*/
    $scope.filterProcessedMatchedGrid.data = $scope.gridOptionsMatchedProcessedRows.data;
    $scope.refreshMatchedTabData = function (termObj) {

        //console.log(termObj);
        $scope.filterProcessedMatchedGrid.data = $scope.gridOptionsMatchedProcessedRows.data;

        while (termObj) {
            var oSearchArray = termObj.split(' ');
            $scope.filterProcessedMatchedGrid.data = $filter('filter')($scope.filterProcessedMatchedGrid.data, oSearchArray[0], undefined);
            oSearchArray.shift()
            termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
        }
        $scope.processedmatchedPagination.toIndex = $scope.filterProcessedMatchedGrid.data.length;

    };
    /*Search Operation  for Matched TAB end here*/

    /*Search Operation  for Duplicates TAB*/
    $scope.filterProcessedDuplicatesGrid.data = $scope.gridOptionsDuplicatesProcessedRows.data;
    $scope.refreshDuplicatesTabData = function (termObj) {

        //console.log(termObj);
        $scope.filterProcessedDuplicatesGrid.data = $scope.gridOptionsDuplicatesProcessedRows.data;

        while (termObj) {
            var oSearchArray = termObj.split(' ');
            $scope.filterProcessedDuplicatesGrid.data = $filter('filter')($scope.filterProcessedDuplicatesGrid.data, oSearchArray[0], undefined);
            oSearchArray.shift()
            termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
        }
        $scope.processedDuplicatePagination.toIndex = $scope.filterProcessedDuplicatesGrid.data.length;

    };
    /*Search Operation  for Duplicates TAB end here*/

    /*Search Operation  for Exceptions TAB*/
    $scope.filterProcessedExceptionsGrid.data = $scope.gridOptionsExceptionsProcessedRows.data;
    $scope.refreshExceptionsTabData = function (termObj) {

        //console.log(termObj);
        $scope.filterProcessedExceptionsGrid.data = $scope.gridOptionsExceptionsProcessedRows.data;

        while (termObj) {
            var oSearchArray = termObj.split(' ');
            $scope.filterProcessedExceptionsGrid.data = $filter('filter')($scope.filterProcessedExceptionsGrid.data, oSearchArray[0], undefined);
            oSearchArray.shift()
            termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
        }
        $scope.processedExceptionPagination.toIndex = $scope.filterProcessedExceptionsGrid.data.length;

    };
    /*Search Operation  for Exceptions TAB end here*/

    /*Search Operation  for NonPrimary TAB*/
    $scope.filterProcessedNonPrimaryGrid.data = $scope.gridOptionsNonPrimaryProcessedRows.data;
    $scope.refreshNonPrimaryTabData = function (termObj) {

        //console.log(termObj);
        $scope.filterProcessedNonPrimaryGrid.data = $scope.gridOptionsNonPrimaryProcessedRows.data;

        while (termObj) {
            var oSearchArray = termObj.split(' ');
            $scope.filterProcessedNonPrimaryGrid.data = $filter('filter')($scope.filterProcessedNonPrimaryGrid.data, oSearchArray[0], undefined);
            oSearchArray.shift()
            termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
        }
        $scope.processedNonPriFacilityPagination.toIndex = $scope.filterProcessedNonPrimaryGrid.data.length;
    };
    /*Search Operation  for NonPrimary TAB end here*/


    /*modal  popup function for enrich*/

    $scope.openEnrichModal = function () {
        //console.log("enrich");

        $scope.modalInstance = $uibModal.open({

            templateUrl: 'EnrichModalContent.html',
            controller: 'EnrichModalController',
            scope: $scope,

        });

    };

    $scope.openExceptionEnrichModal = function () {
        //console.log("enrich");

        $scope.modalInstance = $uibModal.open({

            templateUrl: 'ExceptionEnrichModalContent.html',
            controller: 'ExceptionEnrichModalController',
            scope: $scope,

        });

    };

    $scope.CreateEnrichData = function () {
        //console.log("enrich create");
        //console.log($scope.selectedProcessedNewGrid);
        $scope.EnrichPostData = {};
        var loopCount = 0;
        var CallCount = $scope.selectedProcessedNewGrid.length;

        $scope.EnrichPostData['individualType'] = $scope.selectedProcessedNewGrid[0].individualType;
        $scope.EnrichPostData['individualSubType'] = $scope.selectedProcessedNewGrid[0].individualSubType;
        $scope.EnrichPostData['memberType'] = $scope.selectedProcessedNewGrid[0].memberType;
        $scope.EnrichPostData['memberSubType'] = $scope.selectedProcessedNewGrid[0].memberSubType;
        $scope.EnrichPostData['action'] = 'Create';
        //console.log($scope.ProcessedNewGridselectAllValue);
        if ($scope.ProcessedNewGridselectAllValue) {
            $loading.start("ProcessNewTab");
            ReportService.EnrichNewRecord('&filter={"and":[{"stage":"Processed"},{"status":"New"},{"reportId":\"' + $rootScope.reportId + '\"}]}', $scope.EnrichPostData, function (response) {
                if (response.status = 200) {
                    $scope.ProcessedNewGridselectAllValue = undefined;
                    $scope.enrichshowingAllSelected = false;
                    $scope.enrichCreateBtn = true;
                    $scope.gridApi.selection.clearSelectedRows();
                    $loading.finish("ProcessNewTab");
                    toaster.pop("success", "Enrich Action has been done successfully")
                        //$window.location.reload();

                } else {
                    $scope.ProcessedNewGridselectAllValue = undefined;
                    $loading.finish("ProcessNewTab");
                    toaster.pop("error", response.data.error.message);
                }

            });

        } else {
            $loading.start("ProcessNewTab");
            angular.forEach($scope.selectedProcessedNewGrid, function (val, key) {
                ReportService.EnrichNewRecord('&filter={"and":[{"stage":"Processed"},{"status":"New"},{"id":\"' + val.id + '\"}]}', $scope.EnrichPostData, function (response) {
                    if (response.status = 200) {
                        loopCount++;
                        if (loopCount == CallCount) {
                            $scope.ProcessedNewGridselectAllValue = undefined;
                            $loading.finish("ProcessNewTab");
                            toaster.pop("success", "Enrich Action has been done successfully")
                                //$window.location.reload();

                        }

                    } else {
                        $scope.ProcessedNewGridselectAllValue = undefined;
                        $loading.finish("ProcessNewTab");
                        toaster.pop("error", response.data.error.message);
                    }

                });

            });

        }

    };

    /*modal  popup function for enrich end here*/

    $scope.CreateExceptionEnrichData = function () {

        //console.log($scope.selectedProcessedExceptionsGrid);

        $scope.EnrichPostData['individualType'] = $scope.selectedProcessedExceptionsGrid[0].individualType;
        $scope.EnrichPostData['individualSubType'] = $scope.selectedProcessedExceptionsGrid[0].individualSubType;
        $scope.EnrichPostData['memberType'] = $scope.selectedProcessedExceptionsGrid[0].memberType;
        $scope.EnrichPostData['memberSubType'] = $scope.selectedProcessedExceptionsGrid[0].memberSubType;
        $scope.EnrichPostData['socialSecurity'] = $scope.selectedProcessedExceptionsGrid[0].socialSecurity;
        $scope.EnrichPostData['rnLicenseNumber'] = $scope.selectedProcessedExceptionsGrid[0].rnLicenseNumber;
        $scope.EnrichPostData['action'] = 'Create';

    };

}]);


angular.module('cnaApp').controller('EnrichModalController', ['$scope', '$rootScope', '$uibModalInstance', function EnrichModalController($scope, $rootScope, $uibModalInstance) {
    $rootScope.enrichCreateBtn = true;
    //console.log($scope.selectedProcessedNewGrid);
    $rootScope.EnrichIndivisualTypeVal = $scope.selectedProcessedNewGrid[0].individualType;
    $rootScope.EnrichIndivisualSubTypeVal = $scope.selectedProcessedNewGrid[0].individualSubType;
    $rootScope.EnrichMemberTypeVal = $scope.selectedProcessedNewGrid[0].memberType;
    $rootScope.EnrichMemberSubTypeVal = $scope.selectedProcessedNewGrid[0].memberSubType;

    $scope.SaveEnrichData = function () {

        $scope.NewfilterProcessedNewGrid = [];
        angular.forEach($scope.selectedProcessedNewGrid, function (val, key) {
            val["individualType"] = $scope.EnrichIndivisualTypeVal;
            val["individualSubType"] = $scope.EnrichIndivisualSubTypeVal;
            val["memberType"] = $scope.EnrichMemberTypeVal;
            val["memberSubType"] = $scope.EnrichMemberSubTypeVal;
            //val["ChargePlan"]=$scope.EnrichChargePlanVal;
        });
        ////console.log($scope.NewfilterProcessedNewGrid);
        /* $scope.filterProcessedNewGrid.data=$scope.NewfilterProcessedNewGrid;*/
        $uibModalInstance.close();
        $rootScope.enrichCreateBtn = false;

    };

    $scope.CloseEnrichModal = function () {
        $uibModalInstance.close();
    };

}]);


angular.module('cnaApp').controller('ExceptionEnrichModalController', ['$scope', '$uibModalInstance', '$rootScope', function ExceptionEnrichModalController($scope, $uibModalInstance, $rootScope) {
    $rootScope.ExceptionenrichCreateBtn = true;

    //console.log($scope.selectedProcessedExceptionsGrid);
    $rootScope.ExceptionEnrichIndivisualTypeVal = $scope.selectedProcessedExceptionsGrid[0].individualType;
    $rootScope.ExceptionEnrichIndivisualSubTypeVal = $scope.selectedProcessedExceptionsGrid[0].individualSubType;
    $rootScope.ExceptionEnrichMemberTypeVal = $scope.selectedProcessedExceptionsGrid[0].memberType;
    $rootScope.ExceptionEnrichMemberSubTypeVal = $scope.selectedProcessedExceptionsGrid[0].memberSubType;
    $rootScope.ssn = $scope.selectedProcessedExceptionsGrid[0].socialSecurity;
    $rootScope.Exceptionrnlicence = $scope.selectedProcessedExceptionsGrid[0].rnLicenseNumber;
    $scope.SaveExceptionEnrichData = function () {
        angular.forEach($scope.selectedProcessedExceptionsGrid, function (val, key) {
            val["individualType"] = $scope.ExceptionEnrichIndivisualTypeVal;
            val["individualSubType"] = $scope.ExceptionEnrichIndivisualSubTypeVal;
            val["memberType"] = $scope.ExceptionEnrichMemberTypeVal;
            val["memberSubType"] = $scope.ExceptionEnrichMemberSubTypeVal;
            val["socialSecurity"] = $scope.ssn;
            val["rnLicenseNumber"] = $scope.Exceptionrnlicence;

            //val["ChargePlan"]=$scope.EnrichChargePlanVal;


        });
        $rootScope.ExceptionenrichCreateBtn = false;
        $uibModalInstance.close();
    };


    $scope.CloseExceptionEnrichModal = function () {
        $uibModalInstance.close();
    };


}]).filter('ssnFilter', function () {
    return function (value, mask) {
        var len, val;
        if (mask == null) {
            mask = false;
        }
        if (value) {
            val = value.toString().replace(/\D/g, '');
            len = val.length;
            if (len < 4) {
                return val;
            } else if (3 < len && len < 6) {
                if (mask) {
                    return '***-' + val.substr(3);
                } else {
                    return val.substr(0, 3) + '-' + val.substr(3);
                }
            } else if (len > 5) {
                if (mask) {
                    return '***-**-' + val.substr(5, 4);
                } else {
                    return val.substr(0, 3) + '-' + val.substr(3, 2) + '-' + val.substr(5, 4);
                }
            }
        }
        return value;
    };
}).filter('ssnReverse', function () {
    return function (value) {
        if (!!value) {
            return value.replace(/\D/g, '').substr(0, 9);
        }
        return value;
    };
}).directive('ssnField', function ($filter) {
    var ssnFilter, ssnReverse;
    ssnFilter = $filter('ssnFilter');
    ssnReverse = $filter('ssnReverse');
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, element, attrs, modelCtrl) {
            var formatter, mask, parser;
            mask = attrs.ssnFieldMask;
            formatter = function (value) {
                return ssnFilter(value);
            };
            parser = function (value) {
                var formatted;
                formatted = ssnReverse(value);
                element.val(ssnFilter(formatted));
                return formatted;
            };
            modelCtrl.$formatters.push(formatter);
            return modelCtrl.$parsers.unshift(parser);
        }
    };
});

/*Processed All Matched row selection modal pop controller*/
angular.module('cnaApp').controller('MatchedRowModalController', ['$scope', '$rootScope', '$uibModalInstance', '$loading', 'ReportService', function MatchedRowModalController($scope, $rootScope, $uibModalInstance, $loading, ReportService) {
    $scope.SelectedMatchedRow = {

    };
    $scope.ProcessedSelectedMatchedRowsGrid = {};
    $scope.SelectedMatchedRow.data = $scope.selectedProcessedMatchedRow;
    $scope.SelectedMatchedRowGridHeaderArr = $scope.SelectedMatchedRow.data[0];
    $scope.SelectedMatchedRow.columnDefs = [];
    for (elem in $scope.SelectedMatchedRowGridHeaderArr) {
        $scope.SelectedMatchedRowHeadertmpObj = {};
        if ((elem == "$$hashKey") || (elem == "individualId")) {
            continue;
        }
        $scope.SelectedMatchedRowHeadertmpObj['field'] = elem;

        $scope.SelectedMatchedRowHeadertmpObj['width'] = 150;
        $scope.SelectedMatchedRowHeadertmpObj['headerTooltip'] = true;
        $scope.SelectedMatchedRowHeadertmpObj['cellTooltip'] = true;

        $scope.SelectedMatchedRow.columnDefs.push($scope.SelectedMatchedRowHeadertmpObj);
    }

    $loading.start("matchedRecordModal");
    ReportService.MatchedIndividual("Individuals/" + $scope.selectedProcessedMatchedRow[0].individualId, function (response) {
        if (response.status == 200) {
            $scope.ProcessedSelectedMatchedRowsGrid.data.push(response.data);
            //console.log(response.data);
            $loading.finish("matchedRecordModal");

            $scope.ProcessedSelectedMatchedRowsGridHeaderArr = $scope.ProcessedSelectedMatchedRowsGrid.data[0];
            $scope.ProcessedSelectedMatchedRowsGrid.columnDefs = [];
            for (elem in $scope.ProcessedSelectedMatchedRowsGridHeaderArr) {
                $scope.ProcessedMatchedRowsHeadertmpObj = {};
                if (elem == "id") {
                    continue;
                }

                $scope.ProcessedMatchedRowsHeadertmpObj['field'] = elem;

                $scope.ProcessedMatchedRowsHeadertmpObj['width'] = 150;
                $scope.ProcessedMatchedRowsHeadertmpObj['headerTooltip'] = true;
                $scope.ProcessedMatchedRowsHeadertmpObj['cellTooltip'] = true;

                $scope.ProcessedSelectedMatchedRowsGrid.columnDefs.push($scope.ProcessedMatchedRowsHeadertmpObj);
            }
        }
    });

    $scope.CloseMatchedRowModal = function () {
        $scope.gridApi.selection.clearSelectedRows();
        $uibModalInstance.close();
    };

}]);

/*Processed All Matched row selection modal pop controller ends here*/


/*Processed All Needupdate row selection modal pop controller*/
angular.module('cnaApp').controller('NeedUpdateRowModalController', ['$scope', '$rootScope', '$uibModalInstance', '$loading', 'ReportService', function NeedUpdateRowModalController($scope, $rootScope, $uibModalInstance, $loading, ReportService) {
    $scope.SelectedNeedUpdateRow = {

    };
    $scope.ProcessedSelectedNeedUpdateRowsGrid = {};
    $scope.SelectedNeedUpdateRow.data = $scope.selectedProcessedNeedUpdateRow;
    $scope.SelectedNeedUpdateRow.columnDefs = [];

    /*$loading.start("needUpdateRecordModal");
     ReportService.MatchedIndividual("Individuals/" + $scope.selectedProcessedMatchedRow[0].individualId, function(response) {
         if (response.status == 200) {
             $scope.ProcessedSelectedMatchedRowsGrid.data.push(response.data);
             //console.log(response.data);
             $loading.finish("needUpdateRecordModal");

             $scope.ProcessedSelectedMatchedRowsGridHeaderArr = $scope.ProcessedSelectedMatchedRowsGrid.data[0];
             $scope.ProcessedSelectedMatchedRowsGrid.columnDefs = [];
             for (elem in $scope.ProcessedSelectedMatchedRowsGridHeaderArr) {
                 $scope.ProcessedMatchedRowsHeadertmpObj = {};

                 $scope.ProcessedMatchedRowsHeadertmpObj['field'] = elem;

                 $scope.ProcessedMatchedRowsHeadertmpObj['width'] = 150;
                 $scope.ProcessedMatchedRowsHeadertmpObj['headerTooltip'] = true;
                 $scope.ProcessedMatchedRowsHeadertmpObj['cellTooltip'] = true;

                 $scope.ProcessedSelectedMatchedRowsGrid.columnDefs.push($scope.ProcessedMatchedRowsHeadertmpObj);

             }


         }
     });
*/
    $scope.CloseNeedUpdateRowModal = function () {
        $scope.gridApi.selection.clearSelectedRows();
        $uibModalInstance.close();
    };

}]);
/*Processed All Matched row selection modal pop controller ends here*/


/*Processed All Duplicate row modal pop controller*/
angular.module('cnaApp').controller('DuplicateRowModalController', ['$scope', '$rootScope', '$uibModalInstance', '$loading', 'ReportService', 'ReportDescriptionService', function DuplicateRowModalController($scope, $rootScope, $uibModalInstance, $loading, ReportService, ReportDescriptionService) {
    $scope.DuplicateRowsArr = [];
    $scope.ProcessedSelectedDuplicateRowsGrid = {};
    $scope.SelectedDuplicateRow = {};
    $scope.SelectedDuplicateRow.data = $scope.selectedProcessedDuplicateRow;
    $loading.start("duplicateRecordModal");
    var strUrl = "";
    ReportDescriptionService.GetTargetColumn("ReportDescriptions/findOne?" + "filter[where][reportType]=" + $rootScope.reportType, function (response) {

        if (response.status == 200) {

            angular.forEach(response.data.reportFields, function (val, key) {
                if (val.required == true) {
                    //console.log(val);
                    var comparedField = val['fieldName'];
                    //console.log(comparedField);
                    //console.log($scope.selectedProcessedDuplicateRow[0][comparedField]);
                    var comparedFieldValue = $scope.selectedProcessedDuplicateRow[0][comparedField];
                    if (isNaN(comparedFieldValue)) {
                        //console.log("not number");
                        strUrl += "&filter[where]" + "[dupcheckKey." + comparedField + "]=" + comparedFieldValue;

                    } else {
                        //console.log("number");
                        strUrl += "&filter[where]" + "[dupcheckKey." + comparedField + "][like]=" + comparedFieldValue;
                    }
                }

            });
            //console.log(strUrl);

            ReportService.DuplicateRecords("filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Processed" + strUrl + "&", function (response) {
                if (response.status == 200) {
                    $loading.finish("duplicateRecordModal");
                    //console.log(response.data);
                    angular.forEach(response.data, function (val, key) {

                        $scope.DuplicateRowsObj = {};

                        angular.forEach(val.mappedReportFields, function (val1, key1) {
                            ////console.log(val.fieldName,val.value);
                            var fieldNames;
                            var filedValues;
                            fieldNames = val1.fieldName;
                            filedValues = val1.value;
                            $scope.DuplicateRowsObj[fieldNames] = filedValues;

                        });
                        $scope.DuplicateRowsArr.push($scope.DuplicateRowsObj);
                    });
                    //console.log($scope.DuplicateRowsArr);
                    $scope.ProcessedSelectedDuplicateRowsGrid.data = $scope.DuplicateRowsArr;

                }

            });

        }
    });

    $scope.CloseDuplicateRowModal = function () {
        $scope.gridApi.selection.clearSelectedRows();
        $uibModalInstance.close();
    };

}]);

/*Processed All Duplicate row modal pop controller ends here*/
angular.module('cnaApp').controller('ImportedTabController', ['$scope', '$rootScope', 'toaster', '$loading', 'ReportService', '$filter', function ImportedTabController($scope, $rootScope, toaster, $loading, ReportService, $filter) {

    this.tab = 1;
    //console.log("imported tab Default");

    /* Count for all tabs */
    ReportService.GetImportedCount('&where={"and":[{"stage":"Imported"},{"reportId":\"' + $rootScope.reportId + '\"}]}', function (response) {
        ////console.log(response);
        if (response.status == 200) {
            $scope.allImportedCount = response.data.count;
        }
    });
    /* Count for all tabs */

    $scope.gridOptionsImportedAllRow = {};
    $scope.gridOptionsImportedAllRow = {
        enableGridMenu: true,
    };

    $scope.gridOptionsImportedDuplicateRow = {};
    $scope.filterImportedDuplicateGrid = {
        enableGridMenu: true,
    };
    $scope.gridOptionsImportedExceptionRow = {};
    $scope.filterImportedExceptionGrid = {
        enableGridMenu: true,
    };


    /*API Call For all Imported stage of ALL Tab*/
    $scope.ImportedAllRowsAPI = function () {
        $scope.ImportedAllRows = [];
        $loading.start('ImportedAllTab');

        ReportService.GetImportedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Imported", function (response) {
            if (response.status == 200) {
                //console.log(response);

                angular.forEach(response.data, function (val, key) {
                    $scope.ImportedAllRowsObj = {};

                    angular.forEach(val.mappedReportFields, function (val, key) {
                        ////console.log(val.fieldName,val.value);
                        var fieldNames;
                        var filedValues;
                        fieldNames = val.fieldName;
                        filedValues = val.value;
                        $scope.ImportedAllRowsObj[fieldNames] = filedValues;

                    });
                    $scope.ImportedAllRows.push($scope.ImportedAllRowsObj);
                });
                //Making Json Objects for UI grid for Raw Report Rows end Here

                $scope.gridOptionsImportedAllRow.data = $scope.ImportedAllRows;


                $loading.finish('ImportedAllTab');
            } else {
                $loading.finish('ImportedAllTab');
                toaster.pop("error", response.data.error.message);
            }

        });
    };
    /*API Call For all Imported stage of ALL Tab end here*/

    $scope.ImportedAllRowsAPI(); //Imported ALL tab API function Call

    /*API Call For all Imported stage of Duplicate Tab */
    $scope.ImportedDuplicateRowsAPI = function () {
        $scope.ImportedDuplicateRows = [];
        $loading.start('ImportedDuplicateTab');

        ReportService.GetImportedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Imported&filter[where][status]=Duplicates", function (response) {
            if (response.status == 200) {

                angular.forEach(response.data, function (val, key) {
                    if (!val.exception) {


                        $scope.ImportedDuplicateRowsObj = {};

                        angular.forEach(val.mappedReportFields, function (val, key) {
                            ////console.log(val.fieldName,val.value);
                            var fieldNames;
                            var filedValues;
                            fieldNames = val.fieldName;
                            filedValues = val.value;
                            $scope.ImportedDuplicateRowsObj[fieldNames] = filedValues;

                        });
                        $scope.ImportedDuplicateRows.push($scope.ImportedDuplicateRowsObj);
                    }
                });


                $scope.gridOptionsImportedDuplicateRow.data = $scope.ImportedDuplicateRows;
                $scope.filterImportedDuplicateGrid.data = $scope.ImportedDuplicateRows;

                $loading.finish('ImportedDuplicateTab');
            } else {
                $loading.finish('ImportedDuplicateTab');
                toaster.pop("error", response.data.error.message);
            }

        });

    };
    /*API Call For all Imported stage of Duplicate Tab ends here*/


    /*API Call For all Imported stage of Exception Tab */
    $scope.ImportedExceptionRowsAPI = function () {
        $scope.ImportedExceptionRows = [];
        $loading.start('ImportedExceptionTab');

        ReportService.GetImportedRows("&filter[include]=mappedReportFields&filter[where][reportId]=" + $rootScope.reportId + "&filter[where][stage]=Imported&filter[where][status]=Exceptions", function (response) {
            if (response.status == 200) {

                angular.forEach(response.data, function (val, key) {
                    if (val.exception) {
                        $scope.ImportedExceptionRowsObj = {};

                        angular.forEach(val.mappedReportFields, function (val, key) {
                            ////console.log(val.fieldName,val.value);
                            var fieldNames;
                            var filedValues;
                            fieldNames = val.fieldName;
                            filedValues = val.value;
                            $scope.ImportedExceptionRowsObj[fieldNames] = filedValues;

                        });
                        $scope.ImportedExceptionRows.push($scope.ImportedExceptionRowsObj);
                    }

                });


                $scope.gridOptionsImportedExceptionRow.data = $scope.ImportedExceptionRows;
                $scope.filterImportedExceptionGrid.data = $scope.ImportedExceptionRows;

                $loading.finish('ImportedExceptionTab');
            } else {
                $loading.finish('ImportedExceptionTab');
                toaster.pop("error", response.data.error.message);
            }

        });

    };

    /*API Call For all Imported stage of Exception Tab ends here*/


    /*Search Operation  for Duplicate TAB*/
    $scope.filterImportedDuplicateGrid.data = $scope.gridOptionsImportedDuplicateRow.data;
    $scope.refreshDuplicateTabData = function (termObj) {

        //console.log(termObj);
        $scope.filterImportedDuplicateGrid.data = $scope.gridOptionsImportedDuplicateRow.data;

        while (termObj) {
            var oSearchArray = termObj.split(' ');
            $scope.filterImportedDuplicateGrid.data = $filter('filter')($scope.filterImportedDuplicateGrid.data, oSearchArray[0], undefined);
            oSearchArray.shift()
            termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
        }
    };

    /*Search Operation  for Duplicate TAB end here*/

    /*Search Operation  for Duplicate TAB*/
    $scope.filterImportedExceptionGrid.data = $scope.gridOptionsImportedExceptionRow.data;
    $scope.refreshExceptionTabData = function (termObj) {


        $scope.filterImportedExceptionGrid.data = $scope.gridOptionsImportedExceptionRow.data;

        while (termObj) {
            var oSearchArray = termObj.split(' ');
            $scope.filterImportedExceptionGrid.data = $filter('filter')($scope.filterImportedExceptionGrid.data, oSearchArray[0], undefined);
            oSearchArray.shift()
            termObj = (oSearchArray.length !== 0) ? oSearchArray.join(' ') : '';
        }
    };

    /*Search Operation  for Duplicate TAB end here*/


    /* Action button API Starts here */

    $scope.clearDuplicatesImportedAction = function () {
        $loading.start('ImportedDuplicateTab');
        ReportService.PostImportedClearAction('&filter={"and":[{"stage":"Imported"},{"status":"Duplicates"}]}', {
            "action": "Ignore"
        }, function (response) {
            //console.log(response);
            if (response.status == 200) {
                $loading.finish('ImportedDuplicateTab');
                toaster.pop("success", "Clear Action has been done Successfully");
            } else {
                $loading.finish('ImportedDuplicateTab');
                toaster.pop("error", response.data.error.message);
            }
        });

    };

    $scope.clearImportedExceptionsAction = function () {
        $loading.start('ImportedExceptionTab');
        ReportService.PostImportedClearAction('&filter={"and":[{"stage":"Imported"},{"status":"Exceptions"}]}', {
            "action": "Ignore"
        }, function (response) {
            //console.log(response);
            if (response.status == 200) {
                $loading.finish('ImportedExceptionTab');
                toaster.pop("success", "Clear Action has been done Successfully");
            } else {
                $loading.finish('ImportedExceptionTab');
                toaster.pop("error", response.data.error.message);
            }
        });

    };

    /* Action button API Ends here */

    this.selectImportTab = function (setTab) {
        this.tab = setTab;
        if (setTab == 1) {
            //console.log("imported tab1 by click");

        } else if (setTab == 2) {
            $scope.ImportedDuplicateRowsAPI();

            //console.log("imported tab2 by click");
        } else if (setTab == 3) {
            $scope.ImportedExceptionRowsAPI();

            //console.log("imported tab3 by click");
        }

    };
    this.isSelected = function (checkTab) {
        return this.tab === checkTab;
    };

}]);

angular.module('cnaApp').controller('cbaDashboardController', ['$scope', 'toaster', '$loading', 'FacilityService', function cbaDashboardController($scope, toaster, $loading, FacilityService) {

    $scope.filterCbaDashGrid = {};
    $scope.gridOptionsCbaDash = {};

    function max_date(all_dates) {
        var max_dt = all_dates[0],
            max_dtObj = new Date(all_dates[0]);
        all_dates.forEach(function (dt, index) {
            if (new Date(dt) > max_dtObj) {
                max_dt = dt;
                max_dtObj = new Date(dt);
            }
        });
        return max_dt;
    }

    $scope.cbaDashBoardAPI = function (limit, skip) {
        $loading.start('cbaDash');
        $scope.CbaDashRowdata = [];
        FacilityService.FacilityListWithCba("&filter[include]=cBARules&filter[limit]=" + limit + "&filter[skip]=" + skip, function (response) {
            //console.log(response);
            if (response.status == 200) {

                var greaterLength = 0;
                var tempLength;
                angular.forEach(response.data, function (val, key) {
                    ////console.log(val);

                    $scope.CBADashpayPeriodArr = [];
                    $scope.CbaDashRowObj = {};
                    $scope.CbaDashRowObj['id'] = val.id;
                    $scope.CbaDashRowObj['facilityAccountId'] = val.facilityAccountId;
                    $scope.CbaDashRowObj['name'] = val.name;
                    $scope.CbaDashRowObj['groupName'] = val.groupName;
                    $scope.CbaDashRowObj['consituencyId'] = val.consituencyId;
                    $scope.CbaDashRowObj['lastDemoDate'] = val.lastDemoDate;
                    $scope.CbaDashRowObj['lastDemoPayPeriod'] = val.lastDemoPayPeriod;
                    $scope.CbaDashRowObj['lastDuesDate'] = val.lastDuesDate;
                    $scope.CbaDashRowObj['lastDuesPayPeriod'] = val.lastDuesPayPeriod;

                    $scope.CbaDashRowObj['lastComboDate'] = val.lastComboDate;
                    $scope.CbaDashRowObj['lastComboPayPeriod'] = val.lastComboPayPeriod;
                    $scope.CbaDashRowObj['lastEventsDate'] = val.lastEventsDate;
                    $scope.CbaDashRowObj['lastEventsPayPeriod'] = val.lastEventsPayPeriod;
                    $scope.CbaDashRowObj['lastNewHireDate'] = val.lastNewHireDate;
                    $scope.CbaDashRowObj['lastNewHirePayPeriod'] = val.lastNewHirePayPeriod;
                    $scope.CbaDashRowObj['lastPDApplicationDate'] = val.lastPDApplicationDate;
                    $scope.CbaDashRowObj['lastPDApplicationPayPeriod'] = val.lastPDApplicationPayPeriod;
                    $scope.CbaDashRowObj['lastTerminationDate'] = val.lastTerminationDate;
                    $scope.CbaDashRowObj['lastTerminationPayPeriod'] = val.lastTerminationPayPeriod;
                    $scope.CBADashpayPeriodArr.push(val.lastDemoDate);
                    $scope.CBADashpayPeriodArr.push(val.lastDuesDate);
                    $scope.CBADashpayPeriodArr.push(val.lastComboDate);
                    $scope.CBADashpayPeriodArr.push(val.lastEventsDate);
                    $scope.CBADashpayPeriodArr.push(val.lastNewHireDate);
                    $scope.CBADashpayPeriodArr.push(val.lastPDApplicationDate);
                    $scope.CBADashpayPeriodArr.push(val.lastTerminationDate);

                    $scope.CbaDashRowObj['lastReceivedPayperiod'] = max_date($scope.CBADashpayPeriodArr);

                    $scope.cbaDashTempArr = [];
                    angular.forEach(val.cBARules, function (val1, key1) {

                        //$scope.cbaDashTempArr.push(val1.name);
                        $scope.cbaDashTempArr.push({
                            "rule": val1.name,
                            "fromDate": val1.validFrom,
                            "toDate": val1.validTo
                        });
                        // $scope.cbaDashTempArr.push({ "rule": val1.name, "toDate": val1.validTo });

                    });

                    $scope.CbaDashRowObj['billingRule'] = $scope.cbaDashTempArr;
                    if ($scope.cbaDashTempArr.length > 0) {
                        tempLength = $scope.cbaDashTempArr.length;

                    }

                    if (greaterLength < tempLength) {
                        greaterLength = tempLength;

                    }
                    $scope.CbaDashRowdata.push($scope.CbaDashRowObj);

                });

                if (greaterLength != 0) {
                    $scope.filterCbaDashGrid['expandableRowHeight'] = greaterLength * 80;
                } else {
                    $scope.filterCbaDashGrid['expandableRowHeight'] = 400;
                }

                $scope.filterCbaDashGrid.data = $scope.CbaDashRowdata;
                $scope.gridOptionsCbaDash.data = $scope.CbaDashRowdata;
                $loading.finish('cbaDash');
            }

        });

    };

    $scope.cbaDashBoardAPI(20, 0);
    $scope.filterCbaDashGrid = {

        columnDefs: [{
                field: 'consituencyId',
                displayName: 'CONSTITUENCY ID',
                enableHiding: false,
                width: 150,
                headerTooltip: true
            }, {
                field: 'name',
                displayName: 'FACILITY NAME',
                enableHiding: false,
                width: 250,
                cellTooltip: true,
                headerTooltip: true
            }, {
                field: 'groupName',
                displayName: 'GROUP NAME',
                enableHiding: false,
                width: 200,
                cellTooltip: true,
                headerTooltip: true
            }, {
                field: 'lastReceivedPayperiod',
                displayName: 'LAST RECEIVED PAY PERIOD DATE',
                cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                enableHiding: false,
                width: 240,
                headerTooltip: true
            }, {
                field: 'lastImportedPayperiod',
                displayName: 'LAST IMPORTED PAY PERIOD DATE',
                enableHiding: false,
                width: 240,
                cellTooltip: true,
                headerTooltip: true
            }, {
                field: 'lastYetImportedPayPeriod',
                displayName: 'NOT YET IMPORTED PAY PERIOD DATE',
                enableHiding: false,
                width: 280,
                cellTooltip: true,
                headerTooltip: true
            },
            /*{
                                    field: 'lastDemoDate',
                                    displayName: 'LAST DEMO DATE',
                                    cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                    enableHiding: false,
                                    width: 200,
                                    headerTooltip: true
                                }, {
                                    field: 'lastDemoPayPeriod',
                                    displayName: 'LAST DEMO PAY PERIOD',
                                    enableHiding: false,
                                    width: 200,
                                    cellTooltip: true,
                                    headerTooltip: true
                                }, {
                                    field: 'lastDuesDate',
                                    displayName: 'LAST DUE DATE',
                                    cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                    enableHiding: false,
                                    width: 200,
                                    headerTooltip: true
                                }, {
                                    field: 'lastDuesPayPeriod',
                                    displayName: 'LAST DUE PAY PERIOD',
                                    enableHiding: false,
                                    width: 200,
                                    cellTooltip: true,
                                    headerTooltip: true
                                }, {
                                    field: 'lastDemoDate',
                                    displayName: 'LAST DEMO DATE',
                                    cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                    enableHiding: false,
                                    width: 200,
                                    headerTooltip: true
                                }, {
                                    field: 'lastDemoPayPeriod',
                                    displayName: 'LAST DEMO PAY PERIOD',
                                    enableHiding: false,
                                    width: 200,
                                    cellTooltip: true,
                                    headerTooltip: true
                                }, {
                                    field: 'lastComboDate',
                                    displayName: 'LAST Combo DATE',
                                    cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                    enableHiding: false,
                                    width: 200,
                                    headerTooltip: true
                                }, {
                                    field: 'lastComboPayPeriod',
                                    displayName: 'LAST Combo PAY PERIOD',
                                    enableHiding: false,
                                    width: 200,
                                    cellTooltip: true,
                                    headerTooltip: true
                                }, {
                                    field: 'lastEventsDate',
                                    displayName: 'LAST Events DATE',
                                    cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                    enableHiding: false,
                                    width: 200,
                                    headerTooltip: true
                                }, {
                                    field: 'lastEventsPayPeriod',
                                    displayName: 'LAST Events PAY PERIOD',
                                    enableHiding: false,
                                    width: 200,
                                    cellTooltip: true,
                                    headerTooltip: true
                                }, {
                                    field: 'lastNewHireDate',
                                    displayName: 'LAST New Hire DATE',
                                    cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                    enableHiding: false,
                                    width: 200,
                                    headerTooltip: true
                                }, {
                                    field: 'lastNewHirePayPeriod',
                                    displayName: 'LAST New Hire PAY PERIOD',
                                    enableHiding: false,
                                    width: 200,
                                    cellTooltip: true,
                                    headerTooltip: true
                                }, {
                                    field: 'lastPDApplicationDate',
                                    displayName: 'LAST PD Application DATE',
                                    cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                    enableHiding: false,
                                    width: 200,
                                    headerTooltip: true
                                }, {
                                    field: 'lastPDApplicationPayPeriod',
                                    displayName: 'LAST PD Application PAY PERIOD',
                                    enableHiding: false,
                                    width: 200,
                                    cellTooltip: true,
                                    headerTooltip: true
                                }, {
                                    field: 'lastTerminationDate',
                                    displayName: 'LAST Termination DATE',
                                    cellFilter: 'date:"MM-dd-yyyy HH:mm:ss"',
                                    enableHiding: false,
                                    width: 200,
                                    headerTooltip: true
                                }, {
                                    field: 'lastTerminationPayPeriod',
                                    displayName: 'LAST Termination PAY PERIOD',
                                    enableHiding: false,
                                    width: 200,
                                    cellTooltip: true,
                                    headerTooltip: true
                                }*/
            /*, {
                                    field: 'billingRule',
                                    displayName: 'BILLING RULE',
                                    enableHiding: false,
                                    cellTooltip: true,
                                    headerTooltip: true,
                                    width: 400,

                                    cellTemplate: '<div ng-repeat="rules in row.entity.billingRule "><show-more  text="{{rules.rule}}" todate={{rules.toDate}} limit="50"> </show-more></div>'
                                },*/

        ],
        enableSelectAll: false,
        multiSelect: false,
        expandableRowTemplate: 'views/import/expandableRowTemplate.html',

        onRegisterApi: function (gridApi) { //register grid data first within the gridOptions
            $scope.gridApi = gridApi;
        }
    };

    /* pagination for facility */
    $scope.limit = 20;
    $scope.pageNumber = 0;
    $scope.CBADashpresentpage = 1;

    $scope.CBADashPagination = {};

    FacilityService.FacilityListCount("Facilities/count", function (response) {
        if (response.status == 200) {
            ////console.log(response);
            $scope.CBADashtotalcount = response.data.count;
            if ($scope.CBADashtotalcount < $scope.limit) {
                $scope.CBADashPagination.toIndex = $scope.CBADashtotalcount;
            } else {
                $scope.CBADashPagination.toIndex = $scope.limit;
            }
            $scope.CBADashtotalpages = Math.ceil($scope.CBADashtotalcount / $scope.limit);
        }

    });

    $scope.CBADashPagination = {
        //limit:10,
        limit: $scope.limit,
        skip: 0,
        //pageNumber: 0,
        fromIndex: 1,
        toIndex: $scope.limit,
        pageNumber: 0,
        loadSelCountPerPage: function (pageLimitValue) {
            this.pageNumber = 0;
            this.limit = pageLimitValue;
            this.skip = 0;
            $scope.CBADashpresentpage = 1;
            $scope.CBADashtotalpages = Math.ceil($scope.CBADashtotalcount / pageLimitValue);
            $scope.CBADashPagination.fromIndex = this.skip + 1;
            $scope.CBADashPagination.toIndex = this.skip + Number(this.limit);

            /*Start API*/
            $scope.cbaDashBoardAPI(this.limit, this.skip);

            /*End of API*/
        },
        nextPage: function () {
            if ($scope.CBADashpresentpage != $scope.CBADashtotalpages) {
                this.pageNumber++;
                $scope.CBADashpresentpage++;
                this.skip = this.limit * this.pageNumber;
                $scope.CBADashPagination.fromIndex = this.skip + 1;
                $scope.CBADashPagination.toIndex = Number(this.skip) + Number(this.limit);

                /*Start API*/
                $scope.cbaDashBoardAPI(this.limit, this.skip);

                /*End of API*/
            }

        },

        previousPage: function () {
            if (this.pageNumber > 0) {
                this.pageNumber--;
                $scope.CBADashpresentpage--;
                this.skip = this.limit * this.pageNumber;
                $scope.CBADashPagination.fromIndex = $scope.CBADashPagination.fromIndex - this.limit;
                $scope.CBADashPagination.toIndex = $scope.CBADashPagination.toIndex - this.limit;

                /*Start API*/
                $scope.cbaDashBoardAPI(this.limit, this.skip);

                /*End of API*/

            }
        }
    };
    /* pagination for facility */

}]);
